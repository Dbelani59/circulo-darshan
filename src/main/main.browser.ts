import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from '../app/app.module';
import { environment } from '../environments/environment';
import 'hammerjs';

if (environment.production) {
    enableProdMode();
}

document.addEventListener('DOMContentLoaded', () => {
    platformBrowserDynamic().bootstrapModule(AppModule);
});

if ('serviceWorker' in navigator) {
    navigator.serviceWorker
        .register('ngsw-worker.js')
        .then(() => console.log('service worker installed'))
        .catch((err) => console.error('Error', err));
}
