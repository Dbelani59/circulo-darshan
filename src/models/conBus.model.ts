import { v4 as uuid } from 'uuid';


export class ConBus {
    id: uuid;
    sysCompId: uuid;
    cardCode: string;
    cardName: string;
}
