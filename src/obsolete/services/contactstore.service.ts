import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ContactHttpService } from './contacthttp.service';

export enum relType {
  CUSTOMER  =   'CUSTOMER',
  SUPPLIER  =   'SUPPLIER',
  EMPLOYEE  =   'EMPLOYEE',
  TARGET    =   'TARGET',
  LOCATION  =   'LOCATION',
  ALL       =   'ALL'
}

@Injectable({
  providedIn: 'root'
})
export class ContactstoreService {

  // Subject of Contacts
  private _contactList = new Subject<[]>();

  // In memory data store of contacts
  private _dataStore;



  constructor(
    private contactHttpService: ContactHttpService,
  ) {
  }

  loadAll(relationShipType: relType) {
    this.contactHttpService.getContacts(relationShipType).subscribe(
      data => {
        this._dataStore = data;
        this._contactList.next(Object.assign(this._dataStore));
      }, error => console.log('Could not load contacts.'));
  }

  loadOne(id: number | string) {
   this.contactHttpService.getContact(id).subscribe(data => {
      let notFound = true;
      const localdata: any = data;
      this._dataStore.forEach((item, index) => {
        if (item.id === localdata.id) {
          this._dataStore[index] = data;
          notFound = false;
        }
      });
      if (notFound) {
        this._dataStore.todos.push(data);
      }
      this._contactList.next(Object.assign(this._dataStore));
    }, error => console.log('Could not load contact.'));
  }

  get contacts() {
    return this._contactList.asObservable();
  }

  create(contact) {

    this.contactHttpService.createContact(JSON.stringify(contact)).subscribe(
      data => {
        this._dataStore.push(data);
        this._contactList.next(Object.assign(this._dataStore));
      },
      error => console.log('Could not create contact.'));
  }

  update(contact) {
    console.log(contact);
    this.contactHttpService.updateContact(contact).subscribe(
        data => {
          this._dataStore.forEach((t, i) => {
            if (t.id === contact.id) {
              this._dataStore[i] = data;
            }
          });
          this._contactList.next(Object.assign(this._dataStore));
        }, error => console.log('Could not update contact.'));
  }

  delete(contact) {
    this.contactHttpService.deleteContact(contact).subscribe(
        response => {
          this._dataStore.forEach((t, i) => {
            if (t.id === contact.id) { this._dataStore.splice(i, 1); }
          });
          this._contactList.next(Object.assign(this._dataStore));
        }, error => console.log('Could not delete contact.'));
      }


}
