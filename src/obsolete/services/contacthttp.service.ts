import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// tslint:disable-next-line: import-blacklist
import 'rxjs/Rx';
// tslint:disable-next-line: import-blacklist
import { Observable } from 'rxjs/Observable';

import { UserSessionService } from '../../app/core/login/services/user-session.service';

@Injectable({
    providedIn: 'root'
})
export class ContactHttpService {
    constructor(
        private http: HttpClient,
        private userSessionService: UserSessionService
    ) {}

    createContact(contact) {
        return this.http.post(
            this.userSessionService.BaseAPIURL + '/con',
            contact
        );
    }

    deleteContact(contact) {
        //   console.log(contact);
        return this.http.delete(
            this.userSessionService.BaseAPIURL + '/con/' + contact.id
        );
    }

    updateContact(contact) {
        return this.http.put(
            this.userSessionService.BaseAPIURL + '/con/' + contact.id,
            contact
        );
    }

    getContacts(relationShipType) {
        const headerDict = {
            request: JSON.stringify({
                filter: {
                    relationshipType: relationShipType
                }
            })
        };

        const requestOptions = {
            headers: new HttpHeaders(headerDict)
        };

        return this.http.get(
            this.userSessionService.BaseAPIURL + '/con',
            requestOptions
        );
    }

    getContactEdit(conBusId) {
        return this.http.put(
            this.userSessionService.BaseAPIURL + '/con/' + conBusId + '/edit',
            ''
        );
    }

    // - - - -  - - -  - - - - -

    public getEmployees(): Observable<[]> {
        const url = this.userSessionService.BaseAPIURL + '/employees';
        return this.http.get<[]>(url);
    }

    getContactObs(conBusId): Observable<[]> {
        return this.http.get<[]>(
            this.userSessionService.BaseAPIURL + '/con/' + conBusId
        );
    }

    getContactsObs(): Observable<[]> {
        return this.http.get<[]>(this.userSessionService.BaseAPIURL + '/con');
    }

    getContact(conBusId) {
        return this.http.get(
            this.userSessionService.BaseAPIURL + '/con/' + conBusId
        );
    }

    removeOCRHelper(conBusId, conBusOCRHlpId) {
        return this.http.delete(
            this.userSessionService.BaseAPIURL +
                '/con/' +
                conBusId +
                '/ocr/' +
                conBusOCRHlpId
        );
    }

    addOCRHelper(conBusId, searchString) {
        const body = {
            conBusId: conBusId,
            searchType: 'added',
            searchString: searchString
        };
        return this.http.post(
            this.userSessionService.BaseAPIURL + '/con/' + conBusId + '/ocr',
            body
        );
    }

    addContactQuickAdd(contact) {
        return this.http.post(
            this.userSessionService.BaseAPIURL + '/con/quick',
            contact
        );
    }

    addContact_con_bus_ocr_help(con_bus_ocr_help, con_bus_id) {
        return this.http.post(
            this.userSessionService.BaseAPIURL +
                '/con/' +
                con_bus_id +
                '/con_bus_ocr_help',
            con_bus_ocr_help
        );
    }

    getAllOCRFields(conBusId) {
        return this.http.get(
            this.userSessionService.BaseAPIURL + '/con/' + conBusId + '/ocr'
        );
    }

    getDefaultRelationship(con_bus_id) {
        return this.http.get(
            this.userSessionService.BaseAPIURL +
                '/con/' +
                con_bus_id +
                '/rel/default'
        );
    }

    add_con_bus_ocr_help(con_bus_ocr_help) {
        const url =
            this.userSessionService.BaseAPIURL +
            '/con/' +
            con_bus_ocr_help.conBusId +
            '/ocr';
        return this.http.post(url, con_bus_ocr_help);
    }
}
