import { Injectable, Output, EventEmitter } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { ContactHttpService } from './contacthttp.service';
import { touches } from 'd3';
import { ThrowStmt } from '@angular/compiler';

export enum DocType {
    BUSINESS = 'CB',
    PERSON = 'CP',
    SITE = 'CS'
}

export enum FormState {
    ADD = 'ADD',
    EDIT = 'EDIT',
    VIEW = 'VIEW'
}

export enum RecordNavigation {
    NEXT = 'ADD',
    PREVIOUS = 'PREVIOUS',
    FIRST = 'FIRST',
    LAST = 'LAST'
}

@Injectable({
    providedIn: 'root'
})
export class ContactWorkerService {
    constructor() {}

    // Form Edit State to subscriove to lock fields
    public get formEditState() {
        return this._formEditable.asObservable();
    }

    public get helpStatus() {
        return this._showHelpSubject.asObservable();
    }

    // in memory data store of contact being edited
    public dataStore;
    public recordCount = 0;
    private _dataStoreList = [];
    private _formState: FormState;

    // Subject for Form Edit
    private _formEditable = new BehaviorSubject<Boolean>(false);
    private _showHelpSubject = new BehaviorSubject<boolean>(false);

    recordString: String;

    private _showHelp = false;

    public LoadContact(contact: any) {
        this._dataStoreList.push(contact);

        // Add first record to the datastore
        if (this._dataStoreList.length <= 1) {
            this.dataStore = contact;
        }

        this.recordCount = this._dataStoreList.length;
        this.recordString = '01 / ' + this.recordCount.toString().padStart(2, '00');
    }

    public includeDes(field, showHelp: boolean) {
        if (showHelp === true) {
            return field;
        } else {
            return '';
        }
    }

    public toggleHelp() {
        this._showHelp = !this._showHelp;
        this._showHelpSubject.next(this._showHelp);
    }

    public recordNavidate(recordNavigation: RecordNavigation, startIndex?: number) {
        let index = 0;

        if (!startIndex) {
            index = this._dataStoreList.indexOf(this.dataStore);
        } else {
            index = startIndex;
        }

        if (recordNavigation === RecordNavigation.NEXT) {
            if (index >= 0 && index < this._dataStoreList.length - 1) {
                // Move to next
                this.dataStore = this._dataStoreList[index + 1];
            } else {
                // Move to first
                this.dataStore = this._dataStoreList[0];
            }
        } else if (recordNavigation === RecordNavigation.PREVIOUS) {
            if (index >= 1 && index < this._dataStoreList.length) {
                // move to previous
                this.dataStore = this._dataStoreList[index - 1];
            } else {
                // Nove to End
                this.dataStore = this._dataStoreList[this._dataStoreList.length - 1];
            }
        } else if (recordNavigation === RecordNavigation.FIRST) {
            this.dataStore = this._dataStoreList[0];
        } else {
            this.dataStore = this._dataStoreList[this._dataStoreList.length - 1];
        }

        this.setFormSate(FormState.VIEW);
        const Newindex = this._dataStoreList.indexOf(this.dataStore) + 1;
        this.recordString = Newindex.toString().padStart(2, '00') + ' / ' + this._dataStoreList.length.toString().padStart(2, '00');
    }

    public removeRecord() {
        const index = this._dataStoreList.indexOf(this.dataStore);
        this._dataStoreList.splice(index, 1);
        this.recordNavidate(RecordNavigation.NEXT, index - 1);
    }

    public clearContacts() {
        this._dataStoreList = [];
        this.dataStore = {};
        this.recordCount = 0;
    }

    public setFormSate(formState: FormState, docType?: DocType) {
        // ADD
        // EDIT
        // VIEW
        this._formState = formState;
        if (formState === FormState.ADD) {
            if (docType === DocType.BUSINESS) {
                this.dataStore = {
                    docType: DocType.BUSINESS,
                    conBusTypBus: {},
                    conBusCons: [{}]
                };
            }
            this._formEditable.next(true);
        } else if (formState === FormState.EDIT) {
            this._formEditable.next(true);
        } else {
            this._formEditable.next(false);
        }
    }
}
