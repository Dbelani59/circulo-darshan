import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { ContactHttpService } from '../services/contacthttp.service';
import { relType } from '../services/contactstore.service';

@Injectable({
    providedIn: 'root'
})
export class ContactListResolver implements Resolve<any> {
    constructor(private tableDataService: ContactHttpService) {}

    resolve() {
        return new Promise((resolve, reject) => {
            this.tableDataService.getContacts(relType.ALL).subscribe((tableData: any) => {
                return resolve({
                    data: tableData
                });
            });
        });
    }
}
