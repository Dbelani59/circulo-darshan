export const environment = {
    production: true,
    BASE_URL: '/',
    API_URL: 'https://api.cmtech.app/api/v1',
    versionCheckURL: 'https://app.cmtech.app/version.json',
    version: '4.3.1'
};
