import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith ,  map } from 'rxjs/operators';

@Component({
  selector: 'app-search-bar-list',
  styleUrls: ['./styles/search-bar-list.scss'],
  templateUrl: './search-bar-list.component.html'
})
export class SearchBarListComponent implements OnInit {

  searchBar = new FormControl();

  @Output() SearchEmit = new EventEmitter<string>();


  emitText(value: string) {
      this.SearchEmit.next(value);
  }

  ngOnInit(): void {
  }

}
