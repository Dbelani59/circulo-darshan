import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Country } from './country.model';
import { APP_BASE_HREF } from '@angular/common';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-item-navbar-list',
  templateUrl: './item-navbar-list.component.html',
  styleUrls: ['./styles/item-navbar-list.scss']
})
export class ItemNavbarListComponent implements OnInit {

  countries = new Array<Country>();
  form: FormGroup;

  mySnackBarRef: any;

  constructor(
    public formBuilder: FormBuilder,
    private http: HttpClient,
    @Inject(APP_BASE_HREF) private baseHref: string) {


    http.get<any>(baseHref + '/assets/data/countries.json')
      .subscribe(
        data => {
          data.forEach((c) => {
            this.countries.push(new Country(c.code, c.name));
          });
        }
      );

    const country = new FormControl('', Validators.required);

    this.form = formBuilder.group({
      country
    });
  }


  @Output() ItemClick = new EventEmitter<string>();
  @Output() FilterEmit = new EventEmitter<string>();

  onClick(clickItem: string) {
    // this.showNotification('bottom', 'right', 'success', 'info-circle', 'Saved Sucessful');
    this.ItemClick.emit(clickItem);
    //  this.showNotification('bottom', 'right', '', 'info-circle')
  }


  applyFilterTable(text: string) {
    this.FilterEmit.emit(text);
  }


  ngOnInit() {
  }




}

