import { Component, OnInit, Input, OnChanges, Inject } from '@angular/core';
import { UserSessionService } from '../../core/login/services/user-session.service';
import { HttpClient } from '@angular/common/http';
import { ThrowStmt } from '@angular/compiler';
import { DeviceSessionService } from '../../tab/device-session.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-doc-view',
    templateUrl: './doc-view.component.html',
    styleUrls: ['./doc-view.component.scss', '../../styles/_module-cards.scss']
})
export class DocViewComponent implements OnInit, OnChanges {
    _docStoId = '';
    @Input() docStoId;

    docUrl = '';
    newSrc = '';

    constructor(
        private userSession: UserSessionService,
        private deviceSession: DeviceSessionService,
        private http: HttpClient,
        public dialogRef: MatDialogRef<DocViewComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        console.log(data.docStoId);
        this.docStoId = data.docStoId;
        this.getURL(this.docStoId).subscribe();
    }

    pdfDoc = false;

    getURL(id) {
        let apiUrl = '';

        apiUrl = this.userSession.BaseAPIURL + '/documents/documents/' + id;

        return this.http.get(apiUrl).map((response) => {
            let data;
            data = response;
            this.docUrl = data.data[0].url;
            this.newSrc = 'https://view.officeapps.live.com/op/embed.aspx?src=' + this.docUrl;
            console.log(this.docUrl);
            if (data.data[0].contentType === 'application/pdf') {
                this.pdfDoc = true;
            } else {
                this.pdfDoc = false;
            }
            return data;
        });
    }

    ngOnChanges() {
        this.docStoId = this.data.docStoId;
        this.getURL(this.docStoId).subscribe();
    }

    ngOnInit() {}
}
