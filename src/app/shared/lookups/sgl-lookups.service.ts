import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from '../../core/login/services/user-session.service';
import { SglLookupsHttpService } from './sgl-lookups-http.service';
import { BehaviorSubject, from } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SglLookupsService {

  constructor(
    private httpLookups: SglLookupsHttpService
  ) {
    this.loadCurrency();
    this.loadContactPurposes();
  }

  // Subject of Contacts
  private _currencySubject = new BehaviorSubject<[]>([]);
  private _currencyDataStore;

  private _contactPurposeSubject = new BehaviorSubject<[]>([]);
  private _contactPurposeDataStore;

  get currencys() {
    return this._currencySubject.asObservable();
  }

  get contactPurposes() {
    return this._contactPurposeSubject.asObservable();
  }


  filterCurrency(filters: Object) {

    const arr = this.currencys;

    const filterKeys = Object.keys(filters);

    let newData;
    newData = arr.filter(eachObj => {
      return filterKeys.every(eachKey => {
        if (!filters[eachKey].length) {
          return true; // passing an empty filter means that filter is ignored.
        }
        return filters[eachKey].includes(eachObj[eachKey]);
      });
    });

    this._currencySubject.next(newData);


  }

  private loadCurrency() {
    this.httpLookups.getCurrencys().subscribe(res => {
      this._currencyDataStore = res;
      this._currencySubject.next(this._currencyDataStore);
    });
  }


  private loadContactPurposes() {
    this.httpLookups.getContactPurposes().subscribe(res => {
      this._contactPurposeDataStore = res;
      this._contactPurposeSubject.next(this._contactPurposeDataStore);
    });
  }


}

