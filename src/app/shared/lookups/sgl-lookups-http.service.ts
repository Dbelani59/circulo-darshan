import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from '../../core/login/services/user-session.service';
import { map } from 'rxjs/operators';
// tslint:disable-next-line: import-blacklist
// import 'rxjs/add/operator/map';

export class InputStructure {
    value: string;
    label: string;
}

@Injectable({
    providedIn: 'root'
})
export class SglLookupsHttpService {
    constructor(
        private http: HttpClient,
        private userSessionService: UserSessionService
    ) {}

    getCurrencys() {
        return this.http.get(
            this.userSessionService.BaseAPIURL + '/lookup/currencys'
        );
    }

    getContactPurposes() {
        return this.http.get(
            this.userSessionService.BaseAPIURL + '/lookup/contact-purposes'
        );
    }
}
