import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatSort, MatTableDataSource, MatTable, MatPaginator } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-lookup-table',
  templateUrl: './lookup-table.component.html',
  styleUrls: ['../../data-table/data-table.component.scss']
})
export class LookupTableComponent implements OnInit {

  @Input() tableData;
  @Input() tableDataObs: Observable<[]>;
  @Input() columnHeader;
  @Input() displayedColumns;
  @Output() rowClick = new EventEmitter<object>();
  @Output() navBarClick = new EventEmitter<any[]>();



  objectKeys = Object.keys;
  dataSource;
  displayColumns;
  isLoading = true;
  loadingMessage = 'LOADING...';

  @ViewChild('sortTable1', { static: true }) sortTable1: MatSort;
  @ViewChild('paginatorTable', {static: true}) paginator: MatPaginator;

  selection ;
  selectedItems = [];

  ngOnInit() {

    this.displayColumns = this.displayedColumns;

    const index = this.displayColumns.indexOf('select', 0);
    if (index > -1) {
      this.displayColumns.splice(index, 1);
    }
    this.displayColumns.unshift('select');

    this.tableDataObs.subscribe(res => {
      this.dataSource.data = res;
      this.isLoading = false;
    });

 }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
navBarItemClick(event) {

 this.selectedItems = [];


  this.selection.selected.forEach(element => {
      this.selectedItems.push({event: event, element: element});
  });
  this.navBarClick.emit(this.selectedItems);

}

 // tslint:disable-next-line: use-life-cycle-interface
 ngAfterViewInit() {
  this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sortTable1;

  // this.isLoading = false;
 }

  getRecord(row) {
    this.rowClick.emit(row);
     }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
}
