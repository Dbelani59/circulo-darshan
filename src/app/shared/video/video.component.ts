import { Component, OnInit, ViewChild } from '@angular/core';


@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styles: []
})
export class VideoComponent implements OnInit {

  @ViewChild('video', {static: true}) matVideo;
  video: HTMLVideoElement;

  constructor() { }

  ngOnInit() {
    this.video = this.matVideo.getVideoTag();

    this.video.addEventListener('ended', () => console.log('video ended'));

  }

}
