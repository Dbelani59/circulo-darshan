import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-simple-select',
  templateUrl: './simple-select.component.html',
  styles: []
})
export class SimpleSelectComponent implements OnInit {
  icon: undefined;
  iconColor: undefined;
  title: '';
  text: undefined;
  options: false;
  input: false;
  button: undefined;
  inputData: '';

  constructor() { }

  ngOnInit() {
  }

}
