import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserSessionService } from '../../../core/login/services/user-session.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PdfViewService {
    constructor(
        private http: HttpClient,
        private userSessionService: UserSessionService
    ) {}

    getCloudfrontURL(docStoId) {
        return this.http.get(
            this.userSessionService.BaseAPIURL + '/storage/' + docStoId + '/url'
        );
    }

    deleteDoc(docStoId) {
        return this.http.put(
            this.userSessionService.BaseAPIURL +
                '/storage/' +
                docStoId +
                '/delete',
            ''
        );
    }
}
