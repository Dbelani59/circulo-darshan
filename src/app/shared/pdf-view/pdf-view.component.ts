import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { PdfViewService } from './services/pdf-view.service';
import { PDFDocumentProxy, PdfViewerComponent } from 'ng2-pdf-viewer';

@Component({
    selector: 'app-pdf-view',
    templateUrl: './pdf-view.component.html',
    styleUrls: ['./pdf-view.component.scss'],
    providers: [PdfViewService]
})
export class PdfViewComponent implements OnInit {
    constructor(private pdfView: PdfViewService) {}

    @Input() docStoId: string;
    @Input() url: string;
    @Output() messageEvent = new EventEmitter<object>();

    @ViewChild(PdfViewerComponent)
    private pdfComponent: PdfViewerComponent;

    pageVariable;
    NoOfPages = 0;
    cloudFrontUrl;
    zoom_to;
    docSto = { originalFilename: 'blank' };
    // {data: {originalFilename: ''}};

    pageNavigationNext() {
        if (this.pageVariable < this.NoOfPages) {
            this.pageVariable = this.pageVariable + 1;
            //     console.log(this.pageVariable);
        }
    }
    pageNavigationPrevious() {
        if (this.pageVariable > 1) {
            this.pageVariable = this.pageVariable - 1;
        }
    }

    pageNavigationFirst() {
        this.pageVariable = 1;
    }

    pageNavigationLast() {
        this.pageVariable = this.NoOfPages;
    }

    ngOnInit() {
        this.pageVariable = 1;
        this.zoom_to = 1;
        this.generateCloudFrontURL();
    }

    // tslint:disable-next-line: use-life-cycle-interface
    ngOnChanges() {
        this.zoom_to = 1;
        this.generateCloudFrontURL();
    }

    merge() {
        this.messageEvent.emit({ action: 'merge', docStoId: this.docStoId });
    }

    zoom_in() {
        this.zoom_to = this.zoom_to + 0.25;
    }

    zoom_out() {
        if (this.zoom_to > 1) {
            this.zoom_to = this.zoom_to - 0.25;
        }
    }
    generateCloudFrontURL() {
        if (this.url) {
            this.cloudFrontUrl = this.url;
        } else {
            if (this.docStoId) {
                this.pdfView.getCloudfrontURL(this.docStoId).subscribe((ret) => {
                    let cf;
                    cf = ret;
                    this.cloudFrontUrl = cf.url;
                    this.docSto = cf.data;
                });
            }
        }
    }

    // burstPDF() {
    //     this.docsService.burstDocs(this.docStoId).subscribe((ret) => {
    //         //    console.log(ret);
    //         this.messageEvent.emit({ action: 'burst', docStoId: this.docStoId });
    //     });
    // }

    // deletePDF() {
    //     this.docsService.deleteDoc(this.docStoId).subscribe(() => {
    //         this.messageEvent.emit({ action: 'delete', docStoId: this.docStoId });
    //     });
    // }

    afterLoadComplete(pdf: PDFDocumentProxy) {
        this.NoOfPages = pdf.numPages;
        //    console.log(this.NoOfPages) ;
    }

    pageRendered() {
        this.pdfComponent.pdfViewer.currentScaleValue = 'page-fit';
    }

    // readyForOCR() {
    //     this.docsService.readyForOCR(this.docStoId).subscribe(() => {
    //         this.messageEvent.emit({ action: 'ready', docStoId: this.docStoId });
    //     });
    // }
}
