export interface GenericStoreConstructor {
    urlWithCompany: boolean;
    loadOnStart?: boolean;
    genericUrl?: string;
    getUrl?: string;
    getAllUrl?: string;
    putUrl?: string;
    postUrl?: string;
    deleteUrl?: string;
    dbStructure?: {};
    groupField?: string;
}

export interface LoadAllInterface {
    filter?: {};
    urlParams?: {};
}
