// https://stackoverflow.com/questions/44129817/typescript-generic-service/44130739

import { GenericStoreService } from './generic-store.service';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from '../../core/login/services/user-session.service';
import { Injectable } from '@angular/core';
import { GenericHttpService } from './generic-http.service';
import { MatSnackBar } from '@angular/material';

// @Injectable({
//     providedIn: 'root'
// })
// export class LocationStoreService extends GenericStoreService {
//     constructor(http: GenericHttpService, matC: MatSnackBar) {
//         super(http, matC);
//     }
// }

// @Injectable({
//     providedIn: 'root'
// })
// export class DocumentStoreService extends GenericStoreService {
//     constructor(http: GenericHttpService, matC: MatSnackBar) {
//         super(http, matC);
//     }
// }

// @Injectable({
//     providedIn: 'root'
// })
// export class DeviceStoreService extends GenericStoreService {
//     constructor(http: GenericHttpService, matC: MatSnackBar) {
//         super(http, matC);
//     }
// }

// @Injectable({
//     providedIn: 'root'
// })
// export class DeviceRegisterStoreService extends GenericStoreService {
//     constructor(http: GenericHttpService, matC: MatSnackBar) {
//         super(http, matC);
//     }
// }

// @Injectable({
//     providedIn: 'root'
// })
// export class CommunicationsTemplateStoreService extends GenericStoreService {
//     constructor(http: GenericHttpService, matC: MatSnackBar) {
//         super(http, matC);
//     }
// }

// @Injectable({
//     providedIn: 'root'
// })
// export class CommunicationsEventStoreService extends GenericStoreService {
//     constructor(http: GenericHttpService, matC: MatSnackBar) {
//         super(http, matC);
//     }
// }
