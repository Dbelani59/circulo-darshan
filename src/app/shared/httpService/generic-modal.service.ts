import { Injectable } from '@angular/core';
import { MatSnackBar, MatDialog, MatDialogRef } from '@angular/material';

import * as uuid from 'uuid';
import { UserSessionService } from 'app/core/login/services/user-session.service';

export abstract class GenericModalService {
    constructor(private dialog: MatDialog, component: any) {
        this.instanceId = uuid.v4();
        this.component = component;
    }

    private instanceId;
    multiSelect = false;
    component: any;
    // dialogReference;
    storeService;

    dialogReference: any;
    private _table;

    configure(storeService: any, table: string) {
        this.storeService = storeService;
        this._table = table;
    }

    openModal(data) {
        if (this.multiSelect) {
            if (data.selected) {
                data.selected = !data.selected;
            } else {
                data.selected = true;
            }
        } else {
            const _ = require('lodash');
            const _data = _.cloneDeep(data); // Set main datastore

            this.dialogReference = this.dialog.open(this.component, {
                data: {
                    formState: this.storeService._formStateDisabled,
                    formData: _data
                }
            });

            this.dialogReference.afterClosed().subscribe((conBusCon) => {
                if (conBusCon) {
                    const c = this.storeService.currentRecord[this._table];
                    for (let index = 0; index < c.length; index++) {
                        const element = c[index];
                        if (element.id === conBusCon.id) {
                            c[index] = conBusCon;
                        }
                    }
                    // data = conBusCon;
                }
            });
        }
    }

    openModalAdd() {
        this.dialogReference = this.dialog.open(this.component, {
            data: {
                formState: this.storeService._formStateDisabled,
                formData: {}
            }
        });

        // this.dialogReference.afterClosed().subscribe((conBusCon) => {
        //     if (conBusCon) {
        //         conBusCon.id = this.storeService.getNewUUID();
        //         this.storeService.currentRecord[this._table].push(conBusCon);
        //     }
        // });
    }

    toggleMultiSelect() {
        if (this.multiSelect === true) {
            // Need to remove any currently selected
            const c = this.storeService.currentRecord[this._table];
            for (let index = 0; index < c.length; index++) {
                c[index].selected = false;
            }
        }
        this.multiSelect = !this.multiSelect;
        console.log('Set mutli to: ' + this.multiSelect);
    }

    public getNewUUID(): string {
        return uuid.v4();
    }
}
