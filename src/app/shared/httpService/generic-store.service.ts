import { Injectable } from '@angular/core';
import { GenericHttpService } from './generic-http.service';
import { Subject, BehaviorSubject, from, observable, Observable } from 'rxjs';
import {
    GenericStoreConstructor,
    LoadAllInterface
} from './generic-store-constructor.interface';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { MatSnackBar } from '@angular/material';
import { NotificationComponent } from '../notification/notification.component';
import * as uuid from 'uuid';
import { UserSessionService } from 'app/core/login/services/user-session.service';
const MushtacheModule = require('mustache');

export enum FormState {
    ADD = 'ADD',
    EDIT = 'EDIT',
    VIEW = 'VIEW'
}

export enum RecordNavigation {
    NEXT = 'ADD',
    PREVIOUS = 'PREVIOUS',
    FIRST = 'FIRST',
    LAST = 'LAST'
}
const _ = require('lodash');
const moment = require('moment');

export abstract class GenericStoreService {
    constructor(
        private genericStoreConstructor: GenericStoreConstructor,
        private genericHttpService: GenericHttpService,
        public snackBar: MatSnackBar,
        private userSessionService: UserSessionService
    ) {
        // console.log('called');
        this.instanceId = uuid.v4();
        this._config = _.cloneDeep(genericStoreConstructor);
        this.configure(genericStoreConstructor);
        if (this._genericURLs.loadOnStart) {
            this.loadAll({});
        }
    }

    // Observables
    public get dataListObservable() {
        return this._dataSubject.asObservable();
    }
    public get formStateDisabledObservable() {
        return this._formStateDisabledSubject.asObservable();
    }

    public get helpStatus() {
        return this._showHelpSubject.asObservable();
    }

    public get timeSinceLastReload() {
        if (this._lastReload) {
            return (Date.now() - +this._lastReload) / 1000;
        } else {
            return 0;
        }
    }

    public working: Boolean;

    public instanceId: String;

    private _lastReload: Date;
    public _loadCompleted = false;

    public _formStateDisabled: Boolean = true;

    // Subject
    private _dataSubject = new Subject<[]>(); // Main Data Store
    private _formStateDisabledSubject = new BehaviorSubject<Boolean>(true); // Form Edit Status
    private _showHelpSubject = new BehaviorSubject<boolean>(false); // Show help Subject

    // In memory data store of contacts
    private _dataStore: any[]; // Main Datastore
    public _dataStoreRecordCount = 0;
    private _dbStructure: {};

    public currentRecord: any = {};

    // Selected Records
    public _selectedRecords; // Selected records from the filter
    private _selectedRecordCount = 0;
    public selectedRecordString: string;

    // Filter Records
    public _filterRecords; // Filters applied to the data store
    private _filterRecordCount = 0;

    // Forms
    private _showHelp: boolean;
    public _formState: FormState;
    private _loadAllOptions: LoadAllInterface = {};

    private _config: GenericStoreConstructor;
    private _genericURLs: GenericStoreConstructor;

    mySnackBarRef: any;

    // Grouping
    private _groupingField: string;

    public _groupedRecords = Array();
    Observable;

    saveCurrentRecordObserable = new Observable((observer) => {
        // console.log(this.currentRecord);
        if (this.currentRecord.id) {
            this.updateRecord(this.currentRecord).subscribe((dataResp: any) => {
                observer.next(dataResp);
            });
        } else {
            this.addRecord(this.currentRecord).subscribe((dataResp: any) => {
                observer.next(dataResp);
            });
        }

        this.groupRecords();
    });

    private configure(genericStoreConstructor: GenericStoreConstructor) {
        if (!this._loadCompleted) {
            // Set to config
            this._genericURLs = _.cloneDeep(this._config);

            // Set Delete
            if (genericStoreConstructor.deleteUrl) {
                if (genericStoreConstructor.urlWithCompany === true) {
                    this._genericURLs.deleteUrl =
                        this.userSessionService.BaseAPIURL +
                        genericStoreConstructor.deleteUrl;
                } else {
                    this._genericURLs.deleteUrl =
                        this.userSessionService.BaseAPIURLWithoutSysCompId +
                        genericStoreConstructor.deleteUrl;
                }
            } else {
                if (genericStoreConstructor.urlWithCompany === true) {
                    this._genericURLs.deleteUrl =
                        this.userSessionService.BaseAPIURL +
                        genericStoreConstructor.genericUrl;
                } else {
                    this._genericURLs.deleteUrl =
                        this.userSessionService.BaseAPIURLWithoutSysCompId +
                        genericStoreConstructor.genericUrl;
                }
            }

            // Set GetAll
            if (genericStoreConstructor.getAllUrl) {
                if (genericStoreConstructor.urlWithCompany === true) {
                    this._genericURLs.getAllUrl =
                        this.userSessionService.BaseAPIURL +
                        genericStoreConstructor.getAllUrl;
                } else {
                    this._genericURLs.getAllUrl =
                        this.userSessionService.BaseAPIURLWithoutSysCompId +
                        genericStoreConstructor.getAllUrl;
                }
            } else {
                if (genericStoreConstructor.urlWithCompany === true) {
                    this._genericURLs.getAllUrl =
                        this.userSessionService.BaseAPIURL +
                        genericStoreConstructor.genericUrl;
                } else {
                    this._genericURLs.getAllUrl =
                        this.userSessionService.BaseAPIURLWithoutSysCompId +
                        genericStoreConstructor.genericUrl;
                }
            }

            // Set Get
            if (genericStoreConstructor.getUrl) {
                if (genericStoreConstructor.urlWithCompany === true) {
                    this._genericURLs.getUrl =
                        this.userSessionService.BaseAPIURL +
                        genericStoreConstructor.getUrl;
                } else {
                    this._genericURLs.getUrl =
                        this.userSessionService.BaseAPIURLWithoutSysCompId +
                        genericStoreConstructor.getUrl;
                }
            } else {
                if (genericStoreConstructor.urlWithCompany === true) {
                    this._genericURLs.getUrl =
                        this.userSessionService.BaseAPIURL +
                        genericStoreConstructor.genericUrl;
                } else {
                    this._genericURLs.getUrl =
                        this.userSessionService.BaseAPIURLWithoutSysCompId +
                        genericStoreConstructor.genericUrl;
                }
            }

            // Set Post
            if (genericStoreConstructor.postUrl) {
                if (genericStoreConstructor.urlWithCompany === true) {
                    this._genericURLs.postUrl =
                        this.userSessionService.BaseAPIURL +
                        genericStoreConstructor.postUrl;
                } else {
                    this._genericURLs.postUrl =
                        this.userSessionService.BaseAPIURLWithoutSysCompId +
                        genericStoreConstructor.postUrl;
                }
            } else {
                if (genericStoreConstructor.urlWithCompany === true) {
                    this._genericURLs.postUrl =
                        this.userSessionService.BaseAPIURL +
                        genericStoreConstructor.genericUrl;
                } else {
                    this._genericURLs.postUrl =
                        this.userSessionService.BaseAPIURLWithoutSysCompId +
                        genericStoreConstructor.genericUrl;
                }
            }
            // Set Put
            if (genericStoreConstructor.putUrl) {
                if (genericStoreConstructor.urlWithCompany === true) {
                    this._genericURLs.putUrl =
                        this.userSessionService.BaseAPIURL +
                        genericStoreConstructor.putUrl;
                } else {
                    this._genericURLs.putUrl =
                        this.userSessionService.BaseAPIURLWithoutSysCompId +
                        genericStoreConstructor.putUrl;
                }
            } else {
                if (genericStoreConstructor.urlWithCompany === true) {
                    this._genericURLs.putUrl =
                        this.userSessionService.BaseAPIURL +
                        genericStoreConstructor.genericUrl;
                } else {
                    this._genericURLs.putUrl =
                        this.userSessionService.BaseAPIURLWithoutSysCompId +
                        genericStoreConstructor.genericUrl;
                }
            }

            if (genericStoreConstructor.dbStructure) {
                this._dbStructure = genericStoreConstructor.dbStructure;
            }
            if (genericStoreConstructor.groupField) {
                this._groupingField = genericStoreConstructor.groupField;
            }
            this.setCurrentRecord({});
            this._dataStore = new Array();
            this._filterRecords = new Array();
            this._selectedRecords = new Array();
            this._selectedRecordCount = 0;
            this._filterRecordCount = 0;
            this._dataStoreRecordCount = 0;

            // Replace Templates
            if (this._loadAllOptions.urlParams) {
                this._genericURLs.deleteUrl = MushtacheModule.render(
                    this._genericURLs.deleteUrl,
                    this._loadAllOptions.urlParams
                );
                this._genericURLs.getAllUrl = MushtacheModule.render(
                    this._genericURLs.getAllUrl,
                    this._loadAllOptions.urlParams
                );
                this._genericURLs.getUrl = MushtacheModule.render(
                    this._genericURLs.getUrl,
                    this._loadAllOptions.urlParams
                );
                this._genericURLs.postUrl = MushtacheModule.render(
                    this._genericURLs.postUrl,
                    this._loadAllOptions.urlParams
                );
                this._genericURLs.putUrl = MushtacheModule.render(
                    this._genericURLs.putUrl,
                    this._loadAllOptions.urlParams
                );
            }
        }
    }
    public filterRecords(contact: any) {
        this._filterRecords.push(contact);
        this._dataSubject.next(Object.assign(this._filterRecords));
        // Add first record to the datastore
    }

    public selectRecords(contact: any) {
        this._selectedRecords = [];
        this._selectedRecords.push(contact);

        // Add first record to the datastore
        if (this._selectedRecords.length <= 1) {
            this.setCurrentRecord(contact);
        }

        this._selectedRecordCount = this._selectedRecords.length;
        this.selectedRecordString =
            '01 / ' + this._selectedRecordCount.toString().padStart(2, '00');
    }

    public setCurrentRecordById(id: string) {
        let curRec: any;

        // Find Record by ID
        this._dataStore.forEach((t, i) => {
            if (t.id === id) {
                curRec = this._dataStore[i];
            }
        });

        // Set the current record
        this.setCurrentRecord(curRec);
    }

    private setCurrentRecord(currentRow: any) {
        this.currentRecord = {};
        if (this._dbStructure) {
            this.currentRecord = JSON.parse(JSON.stringify(this._dbStructure));
        }
        if (currentRow && this._dbStructure) {
            Object.assign(this.currentRecord, currentRow);
        }
    }

    gp(xs, key) {
        return xs.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
        }, {});
    }

    private groupRecordsAdd(newRecord: any) {
        const nw = newRecord[this._groupingField];
        let matched = false;

        this._groupedRecords.forEach((t, i) => {
            if (t.name === nw) {
                this._groupedRecords[i].values.push(newRecord);
                matched = true;
            }
        });

        if (matched === false) {
            this._groupedRecords.push({
                name: newRecord[this._groupingField],
                values: [newRecord]
            });
        }
        // console.log(this._groupedRecords);
    }

    private groupRecordsDelete(record: any) {
        // Value of grouping field
        const nw = record[this._groupingField];

        this._groupedRecords.forEach((t, i) => {
            if (t.name === nw) {
                const gpRecordsValues = this._groupedRecords[i].values;
                gpRecordsValues.forEach((gpRecValue, inc) => {
                    if (gpRecValue.id === record.id) {
                        gpRecordsValues.splice(inc, 1);
                    }
                });
            }
        });

        this._groupedRecords.forEach((t, i) => {
            const gpRecordsValues = this._groupedRecords[i];
            if (gpRecordsValues.values.length === 0) {
                this._groupedRecords.splice(i, 1);
            }
        });
    }

    private groupRecords() {
        // Grouping is only ever by one level. Never two levels, and the grouping field must be on the first level
        // e.g. [{ id: 1 , gp: 'hel'} , {id: 2, gp: 'hel'}]
        // to [ {name: 'hel', values: [ {id: 1, gp: 'hel }, {id: 2, gp: 'hel } ]  } ]

        // This only applies to filter records
        // https://stackoverflow.com/questions/38575721/grouping-json-by-values
        // Check if need to apply any grouping
        if (this._groupingField) {
            // console.log(this._groupedRecords);

            const gp = this.gp(this._filterRecords, this._groupingField);
            //  console.log(gp);
            const abc = new Array();
            Object.keys(gp).forEach(function (category) {
                // console.log(this._groupingField);
                //      console.log(category);
                const cat = Object.keys(category)[0];

                abc.push({
                    name: category,
                    values: gp[category]
                });
            });

            // for (let i = 0; i < gp.length - 1; ++i) {
            //     abc.push(gp[i]);
            // }

            this._groupedRecords = abc;
            // console.log(this._groupedRecords);
        }
    }

    public _groupRecordsGetValues(groupName: string): any[] {
        let _returnValue = [];
        this._groupedRecords.forEach((element) => {
            if (element.name === groupName) {
                _returnValue = element.values;
            }
        });
        return _returnValue;
    }

    public clearFilter() {
        this._selectedRecords = this._dataStore;
        this._dataSubject.next(Object.assign(this._filterRecords));
    }

    public clearSelection() {
        this._selectedRecords = [];
        this.setCurrentRecord({});
        this._selectedRecordCount = 0;
        this.selectedRecordString = '00 / 00';
    }

    public toggleHelp() {
        this._showHelp = !this._showHelp;
        this._showHelpSubject.next(this._showHelp);
    }

    // Options level of add eg. match level 1 add
    public noUpdateAddToDataStore(currentRow: any, options?: any) {
        if (options) {
            let _matched = false;
            const fieldMatch: string = options.fieldMatch;
            const addRowName: string = options.addRowName;
            this._dataStore.forEach((t, i) => {
                if (t[fieldMatch] === currentRow[fieldMatch]) {
                    currentRow[addRowName].forEach((element) => {
                        t[addRowName].push(element);
                    });

                    _matched = true;
                }
            });
            if (!_matched) {
                this._filterRecords.push(currentRow);
                this.groupRecordsAdd(currentRow);
            }
        } else {
            this._filterRecords.push(currentRow); // This will add to the datastore as well. TODO need to check
            this.groupRecordsAdd(currentRow);
        }
    }
    public noUpdateReplaceDataStore(currentRow: any) {
        this._dataStore = currentRow;
        this._filterRecords = currentRow;
        this._selectedRecords = currentRow;
    }

    public noUpdateRemoveFromDataStore(localData: any) {
        // Remove record form the store
        this._dataStore.forEach((t, i) => {
            if (t.id === localData.id) {
                this._dataStore.splice(i, 1);
            }
        });

        // Remove record form the filter
        this._dataStore.forEach((t, i) => {
            if (t.id === localData.id) {
                this._filterRecords.splice(i, 1);
            }
        });

        // Remove record from selectedStore
        let _locationOfSelectedRecord = 1;
        this._selectedRecords.forEach((t, i) => {
            if (t.id === localData.id) {
                this._selectedRecords.splice(i, 1);
                _locationOfSelectedRecord = i;
            }
        });

        this.groupRecords();
    }

    loadAll(options: LoadAllInterface) {
        if (!this._loadCompleted) {
            if (this._loadAllOptions !== options) {
                this._loadAllOptions = options;
                this.configure(this._config);
            }

            this.genericHttpService
                .getAll(this._genericURLs.getAllUrl, options.filter)
                .subscribe(
                    (dataResp: any) => {
                        // Set the local datastore

                        this._lastReload = moment();

                        this._dataStore = _.cloneDeep(dataResp.data); // Set main datastore
                        this._dataStoreRecordCount = this._dataStore.length;
                        this._filterRecords = this._dataStore; // Set filter records

                        // Check if need to apply any grouping
                        this.groupRecords();

                        this.clearSelection();
                        this._loadCompleted = true;

                        this._dataSubject.next(
                            Object.assign(this._filterRecords)
                        );
                    },
                    (error) => console.log('Error.' + error)
                );
        } else {
            this._dataSubject.next(Object.assign(this._filterRecords));
        }
    }

    private loadOne(id: string) {
        this.genericHttpService.get(this._genericURLs.getUrl, id).subscribe(
            (dataResp: any) => {
                let notFound = true;
                const localData: any = dataResp.data[0];

                // Update Record if exists
                this._dataStore.forEach((item, index) => {
                    if (item.id === localData.id) {
                        this._dataStore[index] = localData;
                        notFound = false;
                    }
                });

                // Add Record if does not exist
                if (notFound) {
                    this._dataStore.push(localData);
                }

                // Call Next
                this._dataSubject.next(Object.assign(this._dataStore));
            },
            (error) => console.log('Error: ' + error)
        );
    }
    public saveCurrentRecord() {
        if (this.currentRecord.id) {
            this.updateRecord(
                this.currentRecord
            ).subscribe((dataResp: any) => {});
        } else {
            this.addRecord(this.currentRecord).subscribe((dataResp: any) => {});
        }

        this.groupRecords();
    }

    public deleteCurrentRecord() {
        this.deleteRecord(this.currentRecord.id);

        this.groupRecordsDelete(this.currentRecord);
    }

    private addRecord(data) {
        return new Observable((observer) => {
            this.genericHttpService
                .post(this._genericURLs.postUrl, JSON.stringify(data))
                .subscribe(
                    (dataResp: any) => {
                        const localData: any = dataResp.data[0];
                        this._dataStore.push(localData);
                        this._filterRecords.push(localData);
                        this._selectedRecords.push(localData);
                        this.setCurrentRecord(localData);
                        this._dataSubject.next(
                            Object.assign(this._filterRecords)
                        );
                        this.setFormSate(FormState.VIEW);
                        this.showNotification(
                            'bottom',
                            'right',
                            'success',
                            'info-circle',
                            'Record Added'
                        );

                        this.refreshSelectedRecordString();

                        observer.next(localData);
                    },
                    (error) => {
                        console.log('Error: ' + error);
                        observer.error(error);
                    }
                );
        });
    }

    private updateRecord(data) {
        this.working = true;
        return new Observable((observer) => {
            this.genericHttpService
                .put(this._genericURLs.putUrl, data.id, JSON.stringify(data))
                .subscribe(
                    (dataResp: any) => {
                        const localData: any = dataResp.data[0];

                        // Update record in the store
                        this._dataStore.forEach((t, i) => {
                            if (t.id === localData.id) {
                                this._dataStore[i] = localData;
                            }
                        });

                        // Update record in the filter
                        this._filterRecords.forEach((t, i) => {
                            if (t.id === localData.id) {
                                this._dataStore[i] = localData;
                            }
                        });

                        // Update record in the selection
                        this._selectedRecords.forEach((t, i) => {
                            if (t.id === localData.id) {
                                this._dataStore[i] = localData;
                            }
                        });

                        this.working = false;
                        this.showNotification(
                            'bottom',
                            'right',
                            'success',
                            'info-circle',
                            'Record Saved'
                        );

                        this.setFormSate(FormState.VIEW);

                        // Call next
                        this._dataSubject.next(Object.assign(this._dataStore));
                        observer.next(localData);
                    },
                    (error) => {
                        console.log('Error: ' + error);
                        observer.error(error);
                    }
                );
        });
    }

    private deleteRecord(id: string) {
        this.genericHttpService
            .delete(this._genericURLs.deleteUrl, id)
            .subscribe(
                (dataResp: any) => {
                    const localData: any = dataResp.data[0];

                    // Remove record form the store
                    this._dataStore.forEach((t, i) => {
                        if (t.id === localData.id) {
                            this._dataStore.splice(i, 1);
                        }
                    });

                    // Remove record form the filter
                    this._dataStore.forEach((t, i) => {
                        if (t.id === localData.id) {
                            this._filterRecords.splice(i, 1);
                        }
                    });

                    // Remove record from selectedStore
                    let _locationOfSelectedRecord = 1;
                    this._selectedRecords.forEach((t, i) => {
                        if (t.id === localData.id) {
                            this._selectedRecords.splice(i, 1);
                            _locationOfSelectedRecord = i;
                        }
                    });

                    this.showNotification(
                        'bottom',
                        'right',
                        'success',
                        'info-circle',
                        'Record Delete'
                    );

                    // Move to  newrecord in selection
                    this.navigateSelectedRecords(
                        RecordNavigation.NEXT,
                        _locationOfSelectedRecord - 1
                    );

                    // Call Next
                    this._dataSubject.next(Object.assign(this._dataStore));

                    this.refreshSelectedRecordString();
                },
                (error) => console.log('Error: ' + error)
            );
    }

    public navigateSelectedRecords(
        recordNavigation: RecordNavigation,
        startIndex?: number
    ) {
        let index = 0;

        if (!startIndex) {
            this._selectedRecords.forEach((t, i) => {
                if (t.id === this.currentRecord.id) {
                    this._dataStore.splice(i, 1);
                    index = i;
                }
            });
        } else {
            index = startIndex;
        }

        if (recordNavigation === RecordNavigation.NEXT) {
            if (index >= 0 && index < this._selectedRecords.length - 1) {
                // Move to next
                this.setCurrentRecord(this._selectedRecords[index + 1]);
            } else {
                // Move to first
                this.setCurrentRecord(this._selectedRecords[0]);
            }
        } else if (recordNavigation === RecordNavigation.PREVIOUS) {
            if (index >= 1 && index < this._selectedRecords.length) {
                // move to previous
                this.setCurrentRecord(this._selectedRecords[index - 1]);
            } else {
                // Nove to End
                this.setCurrentRecord(
                    this._selectedRecords[this._selectedRecords.length - 1]
                );
            }
        } else if (recordNavigation === RecordNavigation.FIRST) {
            this.setCurrentRecord(this._selectedRecords[0]);
        } else {
            this.setCurrentRecord(
                this._selectedRecords[this._selectedRecords.length - 1]
            );
        }

        this.setFormSate(FormState.VIEW);
        this.refreshSelectedRecordString();
    }

    private refreshSelectedRecordString() {
        let NewIndex = 0;
        this._selectedRecords.forEach((t, i) => {
            if (t.id === this.currentRecord.id) {
                this._dataStore.splice(i, 1);
                NewIndex = i + 1;
            }
        });

        this.selectedRecordString =
            NewIndex.toString().padStart(2, '00') +
            ' / ' +
            this._selectedRecords.length.toString().padStart(2, '00');
    }

    public setFormSate(formState: FormState) {
        // ADD
        // EDIT
        // VIEW
        this._formState = formState;
        if (formState === FormState.ADD) {
            this.setCurrentRecord({});
            this._formStateDisabled = false;
            this._formStateDisabledSubject.next(false);
        } else if (formState === FormState.EDIT) {
            this._formStateDisabled = false;
            this._formStateDisabledSubject.next(false);
        } else {
            this._formStateDisabled = true;
            this._formStateDisabledSubject.next(true);
        }
    }

    itemClick(itemClick): boolean {
        if (itemClick === 'ADD') {
            this.setFormSate(FormState.ADD);
            return true;
        }

        if (itemClick === 'EDIT') {
            this.setFormSate(FormState.EDIT);
            return true;
        }

        if (itemClick === 'SAVE') {
            this.saveCurrentRecord();
            return true;
        }

        if (itemClick === 'DELETE') {
            this.deleteCurrentRecord();
            return true;
        }

        if (itemClick === 'NEXT') {
            this.navigateSelectedRecords(RecordNavigation.NEXT);
            return true;
        }

        if (itemClick === 'PREVIOUS') {
            this.navigateSelectedRecords(RecordNavigation.PREVIOUS);
            return true;
        }

        if (itemClick === 'FIRST') {
            this.navigateSelectedRecords(RecordNavigation.FIRST);
            return true;
        }

        if (itemClick === 'LAST') {
            this.navigateSelectedRecords(RecordNavigation.LAST);
            return true;
        }

        if (itemClick === 'HELP') {
            this.toggleHelp();
            return true;
        }
        return false;
    }

    showNotification(vpos, hpos, type, icon = '', message): void {
        // for more info about Angular Material snackBar check: https://material.angular.io/components/snack-bar/overview
        this.mySnackBarRef = this.snackBar.openFromComponent(
            NotificationComponent,
            {
                data: {
                    message: message,
                    icon,
                    type,
                    dismissible: true
                    // you can add everything you want here
                },
                duration: 3000,
                horizontalPosition: hpos, // 'start' | 'center' | 'end' | 'left' | 'right'
                verticalPosition: vpos, // 'top' | 'bottom'
                panelClass: ['notification-wrapper']
            }
        );

        // this is to be able to close it from the NotificationComponent
        this.mySnackBarRef.instance.snackBarRef = this.mySnackBarRef;
    }

    public getNewUUID(): string {
        return uuid.v4();
    }
}
