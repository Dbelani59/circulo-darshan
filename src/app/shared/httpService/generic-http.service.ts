import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from '../../core/login/services/user-session.service';

@Injectable({
    providedIn: 'root'
})
export class GenericHttpService {
    constructor(
        private http: HttpClient,
        private userSessionService: UserSessionService
    ) {}

    post(url, postData) {
        return this.http.post(url, postData);
    }

    delete(url, id) {
        return this.http.delete(url + '/' + id);
    }

    put(url, id, putData) {
        return this.http.put(url + '/' + id, putData);
    }

    getAll(url, filter?) {
        return this.http.get(url);
    }

    get(url, id) {
        return this.http.get(url + '/' + id);
    }

    setEdit(url, id) {
        return this.http.put(url + '/' + id, '');
    }
}
