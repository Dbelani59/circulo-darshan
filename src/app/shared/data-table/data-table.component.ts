import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core';
import { MatSort, MatTableDataSource, MatTable, MatPaginator } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

//  const get = require('keypather/get');

const objectPath = require('object-path');

export interface RowData {
    picture: string;
    name: string;
    country: string;
    interests: string;
    subscribed: string;
    age: Number;
    status: string;
}

@Component({
    selector: 'app-data-table',
    templateUrl: './data-table.component.html',
    styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit, OnDestroy {
    constructor(public translate: TranslateService) {
        this.dataSource = new MatTableDataSource();
    }
    @Input() tableData;
    @Input() tableDataObs: Observable<[]>;
    @Input() columnHeader;
    @Input() displayedColumns;
    @Output() rowClick = new EventEmitter<object>();
    @Output() navBarClick = new EventEmitter<any[]>();

    objectKeys = Object.keys;
    dataSource;
    displayColumns;
    isLoading = true;
    loadingMessage = 'LOADING...';

    @ViewChild('sortTable1', { static: true }) sortTable1: MatSort;
    @ViewChild('paginatorTable', { static: true }) paginator: MatPaginator;

    selection = new SelectionModel<RowData>(true, []);
    selectedItems = [];

    private tableDataSubsription;

    ngOnInit() {
        this.displayColumns = this.displayedColumns;

        const index = this.displayColumns.indexOf('select', 0);
        if (index > -1) {
            this.displayColumns.splice(index, 1);
        }
        this.displayColumns.unshift('select');

        if (this.tableDataObs) {
            this.tableDataSubsription = this.tableDataObs.subscribe((res) => {
                this.dataSource.data = res;
                this.isLoading = false;
            });
        } else {
            this.dataSource.data = this.tableData;
            this.isLoading = false;
        }
        console.log('data');
        console.log(this.dataSource);
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
    navBarItemClick(event) {
        this.selectedItems = [];

        if (event === 'EDIT' || event === 'DELETE') {
            this.selection.selected.forEach((element) => {
                this.selectedItems.push({ event: event, element: element });
            });
            this.navBarClick.emit(this.selectedItems);
        }

        if (event === 'ADD') {
            this.selectedItems.push({ event: event, element: {} });
            this.navBarClick.emit(this.selectedItems);
        }

        if (event === 'REFRESH') {
            this.selectedItems.push({ event: event, element: {} });
            this.navBarClick.emit(this.selectedItems);
        }
    }

    getNest(theObject, path) {
        return objectPath.get(theObject, path);
    }

    getNested(theObject, path, separator) {
        try {
            separator = separator || '.';

            return path
                .replace('[', separator)
                .replace(']', '')
                .split(separator)
                .reduce(function (obj, property) {
                    return obj[property];
                }, theObject);
        } catch (err) {
            return undefined;
        }
    }

    // tslint:disable-next-line: use-life-cycle-interface
    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sortTable1;
        //  console.log('oth');
        //  console.log(this.dataSource);

        // this.isLoading = false;
    }

    ngOnDestroy() {
        this.tableDataSubsription.unsubscribe();
    }

    getRecord(row) {
        this.rowClick.emit(row);
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected() ? this.selection.clear() : this.dataSource.data.forEach((row) => this.selection.select(row));
    }
}
