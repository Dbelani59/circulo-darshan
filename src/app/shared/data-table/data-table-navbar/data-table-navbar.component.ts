import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { NotificationComponent } from '../../notification/notification.component';

@Component({
  selector: 'app-data-table-navbar',
  templateUrl: './data-table-navbar.component.html',
  styleUrls: ['./data-table-navbar.component.scss']
})
export class DataTableNavbarComponent implements OnInit {

  mySnackBarRef: any;

  constructor(
    ) {
  }


  @Output() itemClick = new EventEmitter<string>();
  @Output() FilterEmit = new EventEmitter<string>();

  onClick(clickItem: string) {
    // this.showNotification('bottom', 'right', 'success', 'info-circle', 'Saved Sucessful');
    this.itemClick.emit(clickItem);
    //  this.showNotification('bottom', 'right', '', 'info-circle')
  }

  applyFilterTable(text: string) {
    this.FilterEmit.emit(text);
  }

  ngOnInit() {
  }

}

