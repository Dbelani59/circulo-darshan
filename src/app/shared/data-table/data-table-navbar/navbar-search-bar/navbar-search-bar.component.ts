import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-navbar-search-bar',
  templateUrl: './navbar-search-bar.component.html',
  styleUrls: ['navbar-search-bar.component.scss']
})
export class NavbarSearchBarComponent implements OnInit {
  searchBar = new FormControl();

  @Output() SearchEmit = new EventEmitter<string>();


  emitText(value: string) {
      this.SearchEmit.next(value);
  }

  ngOnInit(): void {
  }

}
