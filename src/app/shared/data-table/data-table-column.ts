export interface DataTableColumn {
    colName: string;
    HeaderName: string;
    format: string;
}
