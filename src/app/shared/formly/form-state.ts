export interface FormState {
    formState: string;
    delete: boolean;
    save: boolean;
}

export enum formStates {
    ADD = 'ADD',
    EDIT = 'EDIT',
    VIEW = 'VIEW'
}
