import { Component, ViewChild } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

@Component({
    selector: 'app-form-datepicker-type',
    template: ` <div class="form-group" [class.has-error]="showError">
        <label *ngIf="to.label && to.hideLabel !== true" [attr.for]="id">
            {{ to.label }}
            <span *ngIf="to.required && to.hideRequiredMarker !== true">*</span>
        </label>
        <input
            type="text"
            class="form-control calendar"
            placement="bottom"
            bsDatepicker
            [formControl]="formControl"
            [formlyAttributes]="field"
            #dobDate="bsDatepicker"
            [bsConfig]="bsConfig"
            placeholder="YYYY-MM-DD"
            [class.is-invalid]="showError"
        />
    </div>`
})
export class DatepickerTypeComponent extends FieldType {
    // Optional: only if you want to rely on `MatInput` implementation
    bsConfig: Partial<BsDatepickerConfig> = {
        dateInputFormat: 'YYYY-MM-DD',
        showWeekNumbers: false,
        containerClass: 'theme-dark-blue'
    };
}
