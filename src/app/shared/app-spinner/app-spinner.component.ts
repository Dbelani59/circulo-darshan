import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-app-spinner',
  templateUrl: './app-spinner.component.html',
  styleUrls: ['./app-spinner.component.scss']
})
export class AppSpinnerComponent implements OnInit {

  constructor() { }

  @Input() message = '';

  ngOnInit() {
  }

}
