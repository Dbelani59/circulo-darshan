import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
@Injectable()
export class AuthService {
  constructor(public jwtHelper: JwtHelperService) {}
  // ...
  public isAuthenticated(): boolean {
    const token = sessionStorage.getItem('currentUser');
    // Check whether the token is expired and return
    // true or false

    return !this.jwtHelper.isTokenExpired(token);
  }

  getToken() {
    const currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    const token = currentUser.token; // your token
    return token;
  }

}
