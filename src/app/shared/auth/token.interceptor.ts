import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { UserSessionService } from 'app/core/login/services/user-session.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(private userSessionService: UserSessionService) {}
    else;
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        //  console.log(request);
        request = request.clone({
            setHeaders: {
                'x-access-token': this.getToken(),
                'Content-Type': 'application/json'
            }
        });
        return next.handle(request);
    }

    getToken() {
        return this.userSessionService.getToken();

        // if (localStorage['appType'] != null) {
        //     const app = localStorage.getItem('app').toUpperCase();
        //     if (app === 'TAB') {
        //         return this.getDeviceToken();
        //     } else {
        //         return this.getUserToken();
        //     }
        // } else {
        //     return this.getUserToken();
        // }
    }

    // getUserToken() {
    //     if (sessionStorage['currentUser'] != null) {
    //         return sessionStorage.getItem('currentUser');
    //     }

    //     if (localStorage['currentUser'] != null) {
    //         return localStorage.getItem('currentUser');
    //     }

    //     return '';
    // }

    // getDeviceToken() {
    //     // always local storage as persist
    //     if (localStorage['deviceToken'] != null) {
    //         return localStorage.getItem('deviceToken');
    //     } else {
    //         return '';
    //     }
    // }
}
