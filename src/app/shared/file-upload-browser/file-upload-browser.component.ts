import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NotificationComponent } from '../notification/notification.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ContactHttpService } from '../../../obsolete/services/contacthttp.service';
import { ConstantPool } from '@angular/compiler';

@Component({
    selector: 'app-file-upload-browser',
    templateUrl: './file-upload-browser.component.html',
    styleUrls: ['./file-upload-browser.component.scss']
})
export class FileUploadBrowserComponent implements OnInit {
    constructor(
        public snackBar: MatSnackBar,
        private contactHttpService: ContactHttpService
    ) {}

    @Input() displayDocList = false;
    @Output() fileListUploaded: EventEmitter<
        Array<object>
    > = new EventEmitter();
    @Output() fileUploadData;
    loading = false;
    fileUploadCount = 0;
    fileToUploadCount = 0;
    mySnackBarRef: any;

    placeholder = 'https://via.placeholder.com/190x190';

    uploaders = {
        avatar: {
            progress: undefined,
            url: undefined
        },
        files: {
            list: [],
            invalidList: []
        },
        imports: {
            importItems: [],
            invalidimportItems: []
        },

        image: {
            progress: undefined,
            url: undefined
        }
    };

    ngOnInit() {}

    onMultipleChange(event: any, uploader: string): void {
        this.onDropzoneMultipleChange(event.target.files, uploader);
    }

    fileUploadComplete() {
        this.fileUploadCount++;
        if (this.fileUploadCount >= this.fileToUploadCount) {
            this.loading = false;
            this.toast();
            this.fileUploadCount = 0;
            this.fileToUploadCount = 0;
        }
    }

    onDropzoneMultipleChange(fileList: Array<File>, uploader: string): void {
        this.fileToUploadCount = fileList.length;
        this.loading = true;
        for (const file of fileList) {
            const l = this.uploaders[uploader].list.push(file);
            this.read(file, this.uploaders[uploader].list[l - 1]);
        }
    }

    read(file: File, store: any): void {
        const fileReader = new FileReader();
        fileReader.readAsText(file, 'utf8');
        const csvjson = require('csvjson');

        fileReader.onload = function () {
            const options = {
                delimiter: ',' // optional
                //   quote     : '"' // optional
            };

            const csv = csvjson.toSchemaObject(fileReader.result, options);

            store.items = csv;
        };
    }

    loopAndUpdateDate(obj, parentKey: String) {
        Object.keys(obj).forEach((key) => {
            // console.log(`parentKey: ${parentKey}, key: ${key}, value: ${obj[key]}`);
            let newParentKey: String;
            if (parentKey === '') {
                newParentKey = key;
            } else {
                newParentKey = parentKey + '.' + key;
            }

            if (key.toUpperCase().indexOf('DATE', 0) > -1) {
                const newDate = new Date(obj[key]);
                obj[key] = newDate;
            }

            if (typeof obj[key] === 'object') {
                this.loopAndUpdateDate(obj[key], newParentKey);
            }
        });
    }

    sendEmail(item) {
        return new Promise(async (resolve, reject) => {
            // this is a mock email send logic

            //  console.log(item);

            this.loopAndUpdateDate(item, '');
            this.contactHttpService.createContact(item).subscribe((res) => {
                resolve(`Email Sent to ${item}`);
            });
        });
    }

    async sendData() {
        const file = this.uploaders.files.list[0].items;

        const status = await Promise.all(
            file.map((email) => this.sendEmail(email))
        );

        //   console.log('Status =>', status);
    }

    sendDataOld() {
        for (const file of this.uploaders.files.list) {
            for (const item of file.items) {
                this.loopAndUpdateDate(item, '');

                this.contactHttpService.createContact(item).subscribe((res) => {
                    //    console.log(res);
                });
            }
        }
    }

    toast(): void {
        // const dialogRef = this.dialog.open(AlertComponent, {
        //   data: {
        //     icon: 'check-circle',
        //     iconColor: 'success',
        //     title: 'File Upload',
        //     text: this.fileUploadCount + ' files uploaded.',
        //     time: 5000
        //   }
        // });

        this.showNotification(
            'bottom',
            'right',
            'success',
            'info-circle',
            this.fileUploadCount + ' files uploaded successfully!'
        );
    }
    showNotification(vpos, hpos, type, icon = '', message): void {
        // for more info about Angular Material snackBar check: https://material.angular.io/components/snack-bar/overview
        this.mySnackBarRef = this.snackBar.openFromComponent(
            NotificationComponent,
            {
                data: {
                    message: message,
                    icon,
                    type,
                    dismissible: true
                    // you can add everything you want here
                },
                duration: 5000,
                horizontalPosition: hpos, // 'start' | 'center' | 'end' | 'left' | 'right'
                verticalPosition: vpos, // 'top' | 'bottom'
                panelClass: ['notification-wrapper']
            }
        );

        // this is to be able to close it from the NotificationComponent
        this.mySnackBarRef.instance.snackBarRef = this.mySnackBarRef;
    }
}
