import { Injectable } from '@angular/core';
import {
    HttpHeaders,
    HttpRequest,
    HttpClientModule,
    HttpClient
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserSessionService } from '../../../core/login/services/user-session.service';

export interface Config {
    componentType: string;
    show: Boolean;
}

@Injectable()
export class FileUploadService {
    constructor(
        private http: HttpClient,
        private userSessionService: UserSessionService
    ) {}

    getpresignedurls(filename: String, docSource: String, sysCompId: String) {
        const getheaders = new HttpHeaders().set('Accept', 'application/json');
        const body = {
            sysCompId: sysCompId,
            filename: filename,
            docSource: docSource
        };
        return this.http.post(
            this.userSessionService.BaseAPIURL + '/documents/documents',
            body,
            { headers: getheaders }
        );
    }

    uploadfileAWSS3(fileuploadurl, contenttype, file): Observable<any> {
        // this will be used to upload all csv files to AWS S3
        const headers = new HttpHeaders({ 'Content-Type': contenttype });
        const req = new HttpRequest('PUT', fileuploadurl, file, {
            headers: headers,
            reportProgress: true // This is required for track upload process
        });
        return this.http.request(req);
    }

    markUploadComplete(docStoId: String) {
        const getheaders = new HttpHeaders().set('Accept', 'application/json');
        const body = {
            sysCompId: this.userSessionService.user.sysCompId
        };
        return this.http.put(
            this.userSessionService.BaseAPIURL +
                '/documents/documents/' +
                docStoId +
                '/uploadComplete',
            body,
            {
                headers: getheaders
            }
        );
    }
}
