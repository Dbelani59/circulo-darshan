import {
    Component,
    OnInit,
    ViewEncapsulation,
    SystemJsNgModuleLoaderConfig,
    Input,
    Output,
    EventEmitter
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import { HttpHeaders, HttpRequest, HttpEventType } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { FileUploadService } from './services/file-upload.service';
import { UserSessionService } from '../../core/login/services/user-session.service';
import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y';
import { NotificationComponent } from '../notification/notification.component';

@Component({
    selector: 'app-file-upload',
    templateUrl: './file-upload.component.html',
    styleUrls: ['./file-upload.scss']
})
export class FileUploadComponent {
    constructor(
        public dialog: MatDialog,
        private http: HttpClient,
        private fileUploadService: FileUploadService,
        private userSessionService: UserSessionService,
        public snackBar: MatSnackBar
    ) {}

    @Input() displayDocList = true;
    @Output() fileListUploaded: EventEmitter<
        Array<object>
    > = new EventEmitter();
    loading = false;
    fileUploadCount = 0;
    fileToUploadCount = 0;
    mySnackBarRef: any;

    placeholder = 'https://via.placeholder.com/190x190';

    uploaders = {
        avatar: {
            progress: undefined,
            url: undefined
        },
        files: {
            list: [],
            invalidList: []
        },
        image: {
            progress: undefined,
            url: undefined
        }
    };

    onMultipleChange(event: any, uploader: string): void {
        this.onDropzoneMultipleChange(event.target.files, uploader);
    }

    fileUploadComplete() {
        this.fileUploadCount++;
        if (this.fileUploadCount >= this.fileToUploadCount) {
            this.loading = false;
            this.toast();

            this.fileListUploaded.emit(this.uploaders.files.list);

            this.fileUploadCount = 0;
            this.fileToUploadCount = 0;
            this.uploaders.files.list = [];
        }
    }

    onDropzoneMultipleChange(fileList: Array<File>, uploader: string): void {
        this.fileToUploadCount = fileList.length;
        this.loading = true;
        for (const file of fileList) {
            const l = this.uploaders[uploader].list.push(file);
            this.read(file, this.uploaders[uploader].list[l - 1]);
        }
    }

    read(file: File, store: any): void {
        console.log(store);
        console.log(file);
        this.fileUploadService
            .getpresignedurls(
                file.name,
                'UPLOAD',
                this.userSessionService.user.sysCompId
            )
            .subscribe((res: any) => {
                let data;
                data = res.data[0];
                store.loaded = 0;
                store.id = data.id;
                this.fileUploadService
                    .uploadfileAWSS3(data.url, data.contentType, file)
                    .subscribe((event) => {
                        console.log('event');
                        console.log(event);
                        if (event.type === HttpEventType.UploadProgress) {
                            const percentDone = Math.round(
                                (100 * event.loaded) / event.total
                            );
                            store.total = Math.round(event.total / 1024);
                            store.loaded = Math.round(event.loaded / 1024);
                            store.progress = Math.round(
                                (100 * event.loaded) / event.total
                            );
                        }
                        if (event.type === HttpEventType.Response) {
                            this.fileUploadService
                                .markUploadComplete(data.id)
                                .subscribe();
                            this.fileUploadComplete();
                        }
                    });
            });
    }

    toast(): void {
        // const dialogRef = this.dialog.open(AlertComponent, {
        //   data: {
        //     icon: 'check-circle',
        //     iconColor: 'success',
        //     title: 'File Upload',
        //     text: this.fileUploadCount + ' files uploaded.',
        //     time: 5000
        //   }
        // });

        this.showNotification(
            'bottom',
            'right',
            'success',
            'info-circle',
            this.fileUploadCount + ' files uploaded successfully!'
        );
    }
    showNotification(vpos, hpos, type, icon = '', message): void {
        // for more info about Angular Material snackBar check: https://material.angular.io/components/snack-bar/overview
        this.mySnackBarRef = this.snackBar.openFromComponent(
            NotificationComponent,
            {
                data: {
                    message: message,
                    icon,
                    type,
                    dismissible: true
                    // you can add everything you want here
                },
                duration: 5000,
                horizontalPosition: hpos, // 'start' | 'center' | 'end' | 'left' | 'right'
                verticalPosition: vpos, // 'top' | 'bottom'
                panelClass: ['notification-wrapper']
            }
        );

        // this is to be able to close it from the NotificationComponent
        this.mySnackBarRef.instance.snackBarRef = this.mySnackBarRef;
    }
}
