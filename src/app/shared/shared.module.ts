import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { PdfViewerModule } from 'ng2-pdf-viewer';
import { TranslateModule } from '@ngx-translate/core';

// Material modules
import {
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDialogModule,
    MatExpansionModule,
    MatMenuModule,
    MatProgressBarModule,
    MatRadioModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatTabsModule,
    MatDatepickerModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatNativeDateModule,
    MatInputModule,
    MatSelectModule,
    MatGridListModule,
    MatGridTile,
    MatFormFieldModule,
    MatButtonModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatIconModule,
    MatToolbarModule,
    MatRippleModule
} from '@angular/material';

// Material Search Module
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

import { PreloadImageComponent } from './preload-image/preload-image.component';
import { AlertComponent } from './alert/alert.component';
import { NotificationComponent } from './notification/notification.component';
import { FileUploaderDirective } from './file-uploader/file-uploader.directive';
import { PdfViewComponent } from './pdf-view/pdf-view.component';
import { DataTableComponent } from './data-table/data-table.component';
import { DataTableNavbarComponent } from './data-table/data-table-navbar/data-table-navbar.component';
import { NavbarSearchBarComponent } from './data-table/data-table-navbar/navbar-search-bar/navbar-search-bar.component';
import { AppSpinnerComponent } from './app-spinner/app-spinner.component';
import { AppSpinnerWrapperComponent } from './app-spinner-wrapper/app-spinner-wrapper.component';
import { VideoComponent } from './video/video.component';

// import { AuthService } from './auth/auth.service';

// For Video player
import { MatVideoModule } from 'mat-video';

import { SimpleSelectComponent } from './simple-select/simple-select.component';
import { LookupTableComponent } from './lookups/lookup-table/lookup-table.component';
import { FormlySelectComponent } from './formly/formly-select/formly-select.component';

import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyHorizontalWrapper } from './formly/wrappers/horizontal-wrapper';
import { DatepickerTypeComponent } from './formly/formly-date/formly-date';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { DocViewComponent } from './doc-view/doc-view.component';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { DeviceSessionService } from '../tab/device-session.service';

import { ItemNavbarComponent } from './item-navbar/item-navbar.component';
import { ItemNavbarListComponent } from './item-navbar-list/item-navbar-list.component';
import { SearchBarListComponent } from './item-navbar-list/search-bar-list/search-bar-list.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { FileUploadBrowserComponent } from './file-upload-browser/file-upload-browser.component';
import { FileUploadService } from './file-upload/services/file-upload.service';
import { LongPress } from './directives/long-press';
// Import Quill Editor
// https://www.freakyjolly.com/angular-rich-text-editor-using-ngx-quill-tutorial/#.Xv9gvyhKiUk
import { QuillModule } from 'ngx-quill';
import { FaceRecogComponent } from './face-recog/face-recog.component';
import { FaceCaptureComponent } from './face/face-capture/face-capture.component';

@NgModule({
    declarations: [
        // Shared components
        PreloadImageComponent,
        AlertComponent,
        NotificationComponent,
        FileUploaderDirective,
        PdfViewComponent,
        DataTableComponent,
        DataTableNavbarComponent,
        NavbarSearchBarComponent,
        AppSpinnerComponent,
        AppSpinnerWrapperComponent,
        VideoComponent,
        SimpleSelectComponent,
        LookupTableComponent,
        FormlySelectComponent,
        FormlyHorizontalWrapper,
        DatepickerTypeComponent,
        DocViewComponent,
        ItemNavbarComponent,
        ItemNavbarListComponent,
        SearchBarListComponent,
        FileUploadComponent,
        FileUploadBrowserComponent,
        LongPress,
        FaceRecogComponent,
        FaceCaptureComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule,
        // Material modules
        MatAutocompleteModule,
        MatCheckboxModule,
        MatChipsModule,
        MatExpansionModule,
        MatMenuModule,
        MatProgressBarModule,
        MatSidenavModule,
        MatTabsModule,
        MatDialogModule,
        MatAutocompleteModule,
        MatSnackBarModule,
        MatButtonToggleModule,
        MatRadioModule,
        MatDatepickerModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatNativeDateModule,
        MatInputModule,
        MatSelectModule,
        MatGridListModule,
        MatFormFieldModule,
        MatButtonModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatCardModule,
        PdfViewerModule,
        TranslateModule,
        NgxMatSelectSearchModule,
        MatVideoModule,
        MatIconModule,
        MatToolbarModule,
        MatRippleModule,
        BsDatepickerModule.forRoot(),
        FormlyModule.forRoot({
            wrappers: [
                {
                    name: 'form-field-horizontal',
                    component: FormlyHorizontalWrapper
                }
            ],
            validationMessages: [
                { name: 'required', message: 'This field is required' }
            ],
            types: [
                // {name: 'select', component: FormlySelectComponent},
                {
                    name: 'datepicker',
                    component: DatepickerTypeComponent,
                    defaultOptions: {
                        templateOptions: {
                            datepickerOptions: {}
                        }
                    }
                }
            ]
        }),
        FormlyBootstrapModule,
        NgxDocViewerModule,
        QuillModule.forRoot()
    ],
    entryComponents: [AlertComponent, NotificationComponent],
    providers: [DeviceSessionService, FileUploadService],
    exports: [
        // Shared components
        PreloadImageComponent,
        AlertComponent,
        NotificationComponent,
        FileUploaderDirective,
        AppSpinnerComponent,
        //  AuthService,
        // Re-export these modules to prevent repeated imports (see: https://angular.io/guide/ngmodule#re-exporting-other-modules)
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule,
        DataTableComponent,
        // Material modules
        MatAutocompleteModule,
        MatCheckboxModule,
        MatChipsModule,
        MatExpansionModule,
        MatMenuModule,
        MatProgressBarModule,
        MatSidenavModule,
        MatTabsModule,
        MatDialogModule,
        MatSnackBarModule,
        MatButtonToggleModule,
        MatRadioModule,
        MatDatepickerModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatNativeDateModule,
        MatInputModule,
        MatSelectModule,
        MatGridListModule,
        MatFormFieldModule,
        MatButtonModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatCardModule,
        MatIconModule,
        MatToolbarModule,
        TranslateModule,
        PdfViewComponent,
        VideoComponent,
        NgxMatSelectSearchModule,
        FormlyModule,
        FormlyBootstrapModule,
        MatRippleModule,
        BsDatepickerModule,
        DocViewComponent,
        ItemNavbarComponent,
        ItemNavbarListComponent,
        FileUploadComponent,
        FileUploadBrowserComponent,
        NgxDocViewerModule,
        LongPress,
        QuillModule,
        FaceRecogComponent,
        FaceCaptureComponent
    ]
})
export class SharedModule {}
