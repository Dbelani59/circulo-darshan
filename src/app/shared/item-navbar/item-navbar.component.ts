import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { NotificationComponent } from '../notification/notification.component';

@Component({
    selector: 'app-item-navbar',
    templateUrl: './item-navbar.component.html',
    styleUrls: ['./styles/item-navbar.scss']
})
export class ItemNavbarComponent implements OnInit {
    constructor(public snackBar: MatSnackBar) {}

    @Output() itemClick = new EventEmitter<string>();
    @Input() text: String;
    @Input() edit: boolean;
    @Input() recordNav: String;

    ngOnInit() {}

    onClick(clickItem: string) {
        this.itemClick.emit(clickItem);
    }
}
