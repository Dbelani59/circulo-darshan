import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-app-spinner-wrapper',
  templateUrl: './app-spinner-wrapper.component.html',
  styleUrls: ['./app-spinner-wrapper.component.scss']
})
export class AppSpinnerWrapperComponent implements OnInit {

@Input() showSpinner: boolean;
@Input() message: string;


  constructor() { }

  ngOnInit() {
  }

}
