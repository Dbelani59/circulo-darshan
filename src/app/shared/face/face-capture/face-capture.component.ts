import {
    Component,
    OnInit,
    ViewChild,
    ElementRef,
    AfterViewInit
} from '@angular/core';
import {
    TinyFaceDetectorOptions,
    detectSingleFace,
    nets,
    matchDimensions,
    resizeResults,
    draw
} from 'face-api.js';
import * as faceapi from 'face-api.js';
import { DrawFaceLandmarksOptions } from 'face-api.js/build/commonjs/draw';

@Component({
    selector: 'app-face-capture',
    templateUrl: './face-capture.component.html',
    styleUrls: ['./face-capture.component.scss']
})
export class FaceCaptureComponent implements OnInit, AfterViewInit {
    @ViewChild('inputVideo') videoEl: ElementRef;
    @ViewChild('overlay') canvas: ElementRef;
    @ViewChild('othe') othe: ElementRef;

    constructor() {}

    forwardTimes = [];

    selectedFaceDetector = 'tiny_face_detector';

    ngOnInit() {}

    ngAfterViewInit() {
        this.run();
    }

    updateTimeStats(timeInMs) {
        this.forwardTimes = [timeInMs].concat(this.forwardTimes).slice(0, 30);
        const avgTimeInMs =
            this.forwardTimes.reduce((total, t) => total + t) /
            this.forwardTimes.length;
        //   console.log(avgTimeInMs);
    }

    getCurrentFaceDetectionNet() {
        if (this.selectedFaceDetector === 'tiny_face_detector') {
            return nets.tinyFaceDetector;
        }
    }

    isFaceDetectionModelLoaded() {
        return !!this.getCurrentFaceDetectionNet().params;
    }

    async onPlay() {
        if (
            this.videoEl.nativeElement.paused ||
            this.videoEl.nativeElement.ended ||
            !this.isFaceDetectionModelLoaded()
        ) {
            return setTimeout(() => this.onPlay());
        }

        const options = new TinyFaceDetectorOptions({
            inputSize: 512,
            scoreThreshold: 0.7
        });

        const ts = Date.now();

        const result = await faceapi
            .detectSingleFace(this.videoEl.nativeElement, options)
            .withFaceLandmarks(true)
            .withFaceDescriptor();

        //   console.log(result);
        this.updateTimeStats(Date.now() - ts);

        if (result) {
            const faceMatcher = new faceapi.FaceMatcher(result);
        }

        if (result) {
            console.log(result);

            const dims = matchDimensions(
                this.canvas.nativeElement,
                this.videoEl.nativeElement,
                true
            );

            dims.height = this.videoEl.nativeElement.offsetHeight;
            dims.width = this.videoEl.nativeElement.offsetWidth;
            this.canvas.nativeElement.height = this.videoEl.nativeElement.offsetHeight;
            this.canvas.nativeElement.width = this.videoEl.nativeElement.offsetWidth;

            const resizedResult = faceapi.resizeResults(result, dims);

            draw.drawDetections(this.canvas.nativeElement, resizedResult);

            if (result.detection.classScore > 0.99) {
                //  const bestMatch = faceMatcher.findBestMatch(result.descriptor);

                const canvasA = document.createElement('canvas');

                // scale the canvas accordingly
                canvasA.width = this.videoEl.nativeElement.offsetWidth;
                canvasA.height = this.videoEl.nativeElement.offsetHeight;
                // draw the video at that frame
                canvasA
                    .getContext('2d')
                    .drawImage(
                        this.videoEl.nativeElement,
                        resizedResult.detection.box.x,
                        resizedResult.detection.box.y - 30,
                        resizedResult.detection.box.width,
                        resizedResult.detection.box.height,
                        0,
                        0,
                        resizedResult.detection.box.width,
                        resizedResult.detection.box.height + 30
                    );

                // convert it to a usable data URL
                const dataURL = canvasA.toDataURL();

                // this.othe.nativeElement.src = dataURL;
            }
        }

        setTimeout(() => this.onPlay());
    }

    async run() {
        // load face detection model
        //   await changeFaceDetector(TINY_FACE_DETECTOR);
        //  changeInputSize(128);

        await faceapi.nets.tinyFaceDetector.loadFromUri('assets/imgs/weights');

        await faceapi.nets.faceLandmark68TinyNet.loadFromUri(
            'assets/imgs/weights'
        );
        await faceapi.nets.faceRecognitionNet.loadFromUri(
            'assets/imgs/weights'
        );

        // try to access users webcam and stream the images
        // to the video element
        const stream = await navigator.mediaDevices.getUserMedia({ video: {} });

        this.videoEl.nativeElement.srcObject = stream;

        this.videoEl.nativeElement.addEventListener('loadedmetadata', () => {
            this.onPlay();
        });

        this.videoEl.nativeElement.onloadedmetadata = () => {
            this.onPlay();
        };
    }
}
