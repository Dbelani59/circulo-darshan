import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { TransferHttpCacheModule } from '@nguniversal/common';

import {
    APP_BASE_HREF,
    LocationStrategy,
    HashLocationStrategy
} from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from './../environments/environment';

import { rootRoutes } from './app.routes';

import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MAT_DATE_LOCALE, MatTabsModule } from '@angular/material';
import { TabModule } from './tab/tab.module';
// import { AuthGuardService as AuthGuard } from './accounting/auth/auth-guard.service';
// import { AuthService  } from './accounting/auth/auth.service';

import { MatIconModule } from '@angular/material/icon'; // <----- Here
// import { AppRoutingModule } from './accounting/app.routing';
import { DashboardModule } from './dashboard/dashboard.module';
import { UserSessionService } from './core/login/services/user-session.service';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './../app/shared/auth/token.interceptor';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ServiceWorkerModule } from '@angular/service-worker';
import { FormlyFieldTabs } from './features/contact/contact-cont/contact-cont-item-modal/tabs.type';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';

// Import Quill Editor
// https://www.freakyjolly.com/angular-rich-text-editor-using-ngx-quill-tutorial/#.Xv9gvyhKiUk
// import { QuillModule } from 'ngx-quill';

// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
    return new TranslateHttpLoader(httpClient);
}

// export function getBaseLocation() {
//     const app = localStorage.getItem('app');
//     return `${environment.BASE_URL}` + '/' + app;
// }

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule.withServerTransition({ appId: 'my-app' }),
        HttpClientModule,
        RouterModule.forRoot(rootRoutes, {
            enableTracing: false, // For debugging
            preloadingStrategy: PreloadAllModules,
            initialNavigation: 'enabled',
            useHash: false
        }),
        // RouterModule.forRoot(rootRoutes, {
        //    // enableTracing: false, // For debugging
        //    useHash: true

        // }),
        //   AppRoutingModule, //TW Added to get routing working
        CoreModule,
        SharedModule,
        DashboardModule,
        BrowserAnimationsModule,
        TransferHttpCacheModule,
        TabModule,
        MatIconModule,
        MatTabsModule,
        FormlyBootstrapModule,
        FormlyModule.forRoot({
            validationMessages: [
                { name: 'required', message: 'This field is required' }
            ],
            types: [{ name: 'tabs', component: FormlyFieldTabs }]
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        // ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
        ServiceWorkerModule.register('ngsw-worker.js')
        //   QuillModule.forRoot()
    ],
    bootstrap: [AppComponent],
    providers: [
        {
            provide: APP_BASE_HREF,
            // useFactory: getBaseLocation
            //  useValue: `${environment.BASE_URL}`
            useValue: '/'
        },
        {
            provide: MAT_DATE_LOCALE,
            useValue: 'en-GB'
        },
        UserSessionService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        }
        //  {provide: LocationStrategy, useClass: HashLocationStrategy  },
    ]
})
export class AppModule {}
