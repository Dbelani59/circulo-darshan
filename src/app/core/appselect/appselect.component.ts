import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-appselect',
  templateUrl: './appselect.component.html',
  styleUrls: ['./appselect.component.scss']
})
export class AppselectComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  onClick(itemSelected: string) {


    if (itemSelected === 'API') {
      window.location.href = 'https://api.cmtech.app/';
    } else {
      localStorage.setItem('app', itemSelected.toLocaleLowerCase());
      this.router.navigate(['/' + itemSelected.toLocaleLowerCase()]);
    }



  }

}
