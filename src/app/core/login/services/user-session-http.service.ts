import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class UserSessionHttpService {
    constructor(private http: HttpClient) {}

    registerDevice(BaseAPIURLWithoutSysCompId, body) {
        return this.http.post(BaseAPIURLWithoutSysCompId + '/device', body);
    }

    getTokenFromServer(BaseAPIURLWithoutSysCompId, username, password) {
        console.log(BaseAPIURLWithoutSysCompId);
        return this.http.put(BaseAPIURLWithoutSysCompId + '/users/login', {
            username: username,
            password: password
        });
    }

    getTokenFromDeviceRegistration(BaseAPIURLWithoutSysCompId, body) {
        console.log(BaseAPIURLWithoutSysCompId);
        return this.http.put(
            BaseAPIURLWithoutSysCompId + '/device/register',
            body
        );
    }

    getUserFromToken(BaseAPIURLWithoutSysCompId): Promise<any> {
        // Other methods using map would not work as used in app.component. Think is has to do with how / when the dependencies are loaded

        return new Promise((resolve, reject) => {
            try {
                this.http
                    .put(BaseAPIURLWithoutSysCompId + '/users/token', {})
                    .subscribe(
                        (data) => {
                            resolve(data);
                        },
                        (err) => reject(err)
                    );
            } catch (err) {
                console.log(err);
                resolve(err);
            }
        });
    }
}
