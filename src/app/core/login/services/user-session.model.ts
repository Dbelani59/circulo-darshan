export class UserSessionModel {
    public friendlyName: string;
    public apiKey: string;

    public sysUsrId: string;
    public sysUsrTenId: string;
    public conBusId: string;
    public conBusName: string;
    public conBusCardCode: string;

    public sysCompId: string;
    public conCompName: string;

    public language: string;
    public app: string;
    public appType: string;

    public loggedIn: boolean;

    public sysUsrSesId: string;

    constructor() {
        this.loggedIn = false;
        this.language = 'eng';
        this.appType = 'BO';
        this.app = 'OFFICE';
    }
}
