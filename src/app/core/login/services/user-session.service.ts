import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { UserSessionModel } from './user-session.model';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from '../../../../environments/environment';
import { UserSessionHttpService } from './user-session-http.service';
import * as uuid from 'uuid';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
const jwtHelper = new JwtHelperService();

@Injectable()
export class UserSessionService {
    userSessionEmit = new Subject<UserSessionModel>();

    public user = new UserSessionModel();
    public BaseAPIURLWithoutSysCompId;
    public BaseAPIURL;
    private time = 0;
    private interval;
    public registrationCode: string;
    // public _appTab: boolean;
    // public _app: string;
    private _deviceRegistrationRunning = false;

    constructor(private userSessionHttp: UserSessionHttpService) {
        // Get base url
        this.BaseAPIURLWithoutSysCompId = environment.API_URL;

        // this.setApp();
    }

    public setApp(_app: string = '') {
        this.user = new UserSessionModel();

        const appTypes = ['TAB', 'BO'];
        const apps = [
            'OFFICE',
            'CLIENT',
            'EMPLOYEE',
            'CUSTOMER',
            'SUPPLIER',
            'TAB'
        ]; // This is populated from the user account

        let appType = localStorage.getItem('appType');
        let app = localStorage.getItem('app');

        if (app) {
            app = app.toUpperCase();
        }
        if (appType) {
            appType = appType.toUpperCase();
        }

        if (_app) {
            _app = _app.toUpperCase();
        }

        if (_app) {
            // Set New App
            this.user.app = _app;
            if (_app === 'TAB') {
                localStorage.setItem('appType', 'TAB');
                this.user.appType = 'TAB';
            } else {
                localStorage.setItem('appType', 'BO');
                this.user.appType = 'BO';
            }
        } else if (appTypes.includes(appType)) {
            // Set to a
            this.user.appType = appType;
            if (appType === 'TAB') {
                this.user.app = 'TAB';
            }
        } else {
            // If nothing provided
            localStorage.setItem('appType', 'BO'); // DEFAULT
            this.user.appType = 'BO';
            this.user.app = 'OFFICE';
        }

        if (this.getToken()) {
            this.setupSession();
        } else {
            // If not login
            if (
                this.user.appType === 'TAB' &&
                this._deviceRegistrationRunning === false
            ) {
                this.DeviceCheckForTokenFromRegistration();
            }

            this.userSessionEmit.next(this.user);
        }
    }

    private setupSession(sysCompId: String = '') {
        const auth = this.isAuthenticated();

        try {
            if (sysCompId !== this.user.sysCompId) {
                // Attempt key exchange - This is to avoid logging in each time
            }

            this.userSessionHttp
                .getUserFromToken(this.BaseAPIURLWithoutSysCompId)
                .then(
                    async (data) => {
                        let _sysUsr: any;
                        _sysUsr = data.data[0];

                        this.user.friendlyName = _sysUsr.conBusName;
                        this.user.apiKey = this.getToken();

                        this.user.sysUsrId = _sysUsr.id;
                        this.user.sysUsrTenId = _sysUsr.sysUsrTenId;
                        this.user.conBusId = _sysUsr.conBusId;
                        this.user.conBusName = _sysUsr.conBusName;
                        this.user.conBusCardCode = _sysUsr.conBusCardCode;

                        this.user.sysCompId = _sysUsr.defaultSysCompId;
                        this.user.conCompName = _sysUsr.conCompName;

                        this.user.language = _sysUsr.language;

                        // Temp for now until provided from the database
                        if (this.user.appType === 'BO') {
                            this.user.app = 'OFFICE';
                        }

                        this.user.loggedIn = true;

                        this.user.sysUsrSesId = _sysUsr.sysUsrSesId;

                        this.BaseAPIURL =
                            this.BaseAPIURLWithoutSysCompId +
                            '/' +
                            this.user.sysCompId;

                        this.userSessionEmit.next(this.user);
                    },
                    (error) => {
                        if (
                            this.user.appType === 'TAB' &&
                            this._deviceRegistrationRunning === false
                        ) {
                            this.DeviceCheckForTokenFromRegistration();
                        }
                        console.log('failed login from token');
                        this.user.loggedIn = false;

                        this.userSessionEmit.next(this.user);
                    }
                );
        } catch (err) {
            console.log(err);
        }
    }

    login(username, password, persist: boolean) {
        return new Promise((resolve, reject) => {
            this.userSessionHttp
                .getTokenFromServer(
                    this.BaseAPIURLWithoutSysCompId,
                    username,
                    password
                )
                .subscribe(
                    (res: any) => {
                        const _sysUsr = res.data[0];

                        if (persist === true) {
                            localStorage.setItem('currentUser', _sysUsr.token); // Will look at how we can do this more securely
                        } else {
                            sessionStorage.setItem(
                                'currentUser',
                                _sysUsr.token
                            ); // Will look at how we can do this more securely
                            localStorage.removeItem('currentUser');
                        }
                        this.setupSession();
                        resolve(this.user);
                    },
                    (error) => {
                        console.log(error);
                        reject(error);
                    }
                );
        });
    }

    private isAuthenticated() {
        try {
            const token = this.getToken();
            return !jwtHelper.isTokenExpired(token);
        } catch (e) {
            return false;
        }
    }

    private DeviceCheckForTokenFromRegistration() {
        this.interval = setInterval(() => {
            this.time++;

            this._deviceRegistrationRunning = true;
            let registrationCode;
            registrationCode = localStorage.getItem('registrationCode');

            const body = {
                registrationCode: registrationCode,
                latitude: 0,
                longitude: 0,
                userAgent: '',
                connection: '',
                info: localStorage,
                externalIpAddress: ''
            };

            this.userSessionHttp
                .getTokenFromDeviceRegistration(
                    this.BaseAPIURLWithoutSysCompId,
                    body
                )
                .subscribe((res: any) => {
                    const devReq = res.data[0];
                    if (devReq.token) {
                        // Set Token
                        localStorage.setItem('deviceToken', devReq.token);
                        localStorage.removeItem('registrationCode');

                        // Setup Session
                        this.setupSession();

                        // End Device Registration
                        this._deviceRegistrationRunning = false;

                        // End loop
                        clearInterval(this.interval);
                    } else {
                        // Update Registration Code
                        this.registrationCode = devReq.registrationCode;
                        console.log(this.registrationCode);
                        localStorage.setItem(
                            'registrationCode',
                            devReq.registrationCode
                        );
                    }
                });
        }, 10000);
    }

    public getToken() {
        if (this.user.appType === 'TAB') {
            if (localStorage['deviceToken'] != null) {
                return localStorage.getItem('deviceToken');
            } else {
                return '';
            }
        } else {
            if (sessionStorage['currentUser'] != null) {
                return sessionStorage.getItem('currentUser');
            } else if (localStorage['currentUser'] != null) {
                return localStorage.getItem('currentUser');
            } else {
                return '';
            }
        }
    }
}
