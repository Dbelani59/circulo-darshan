import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable()
export class LoginService {
    constructor(private http: HttpClient) {}

    login(username, password) {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this.http.post(
            environment.API_URL + '/users/login',
            { username: username, password: password },
            { headers: headers }
        );
    }
}
