import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from './user-session.service';

@Injectable({
    providedIn: 'root'
})
export class TrackingService {
    constructor(private http: HttpClient, private userSessionService: UserSessionService) {
        if (navigator.geolocation) {
            this.trackingAvailable = true;
        } else {
            this.trackingAvailable = false;
        }

        this.userAgent = navigator.userAgent;
        this.connection = navigator['connection'];

        this.trackMe();
    }

    public userAgent;
    public connection;
    public latitude;
    public longitude;
    public position;
    public trackingAvailable = false;
    public isTracking = false;

    public startTracking() {
        if (this.trackingAvailable === true) {
            this.trackMe();
            return true;
        } else {
            return false;
        }
    }

    private findMe() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                // console.log(position);
            });
        } else {
            alert('Geolocation is not supported by this browser.');
        }
    }

    private SendHeartbeat() {
        this.http
            .post(this.userSessionService.BaseAPIURL + '/devices/' + this.userSessionService.user.conBusId + '/heartbeat', {
                latitude: this.latitude,
                longitude: this.longitude
            })
            .subscribe();
    }

    private trackMe() {
        if (navigator.geolocation) {
            this.isTracking = true;
            navigator.geolocation.watchPosition((position) => {
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;
                this.position = position;
                //  console.log(this.position);
                //  this.SendHeartbeat();
            });
        } else {
            this.isTracking = false;
        }
    }
}
