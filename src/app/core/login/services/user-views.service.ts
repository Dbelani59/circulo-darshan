import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserViewsService {

  constructor() { }



contractList(docType) {


  if (docType === 'P') {
    return ['docType', 'cardCode', 'cardName', 'validFrom'];
  } else if (docType === 'E') {
    return ['conBusTypPer.payEmp.payNo', 'conBusTypPer.firstname', 'conBusTypPer.lastname', 'conBusTypPer.jobtitle'];
  } else {
    return ['docType', 'cardCode', 'cardName', 'validFrom'];
 }
}


}
