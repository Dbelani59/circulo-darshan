import { Component, OnInit } from '@angular/core';
import { CompanySelectService } from './services/company-select.service';


@Component({
  selector: 'app-company-select',
  templateUrl: './company-select.component.html',
  styleUrls: ['./company-select.component.scss'],
  providers: [CompanySelectService]
})
export class CompanySelectComponent implements OnInit {

  constructor(
      private companySelectService: CompanySelectService
  ) { }

  show = false;
  data;

  get stateName() {
    return this.show ? 'show' : 'hide';
  }
  ngOnInit() {

    this.companySelectService.getCompanys().subscribe(
      res => {
        this.data = res;
      }
    );
  }

}
