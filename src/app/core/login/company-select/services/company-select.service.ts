import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from '../../services/user-session.service';

@Injectable()
// {
//   providedIn: 'root'
// }
export class CompanySelectService {
    constructor(
        private http: HttpClient,
        private userSessionService: UserSessionService
    ) {}

    getCompanys() {
        return this.http.get(
            this.userSessionService.BaseAPIURLWithoutSysCompId + '/company'
        );
    }
}
