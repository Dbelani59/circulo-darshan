/* prettier-ignore-start */
/* prettier-ignore */
import { Component, OnInit } from '@angular/core';
// import { LoginModalComponent } from '../../../../ignore/aaa/utils/pages/modals/templates/login/login.component';
/* prettier-ignore */
import { MatDialog } from '@angular/material';

import { LoginService } from './services/login.service';
import { AlertComponent } from '../../shared/alert/alert.component';
import { UserSessionService } from '../login/services/user-session.service';

import {
    trigger,
    state,
    style,
    animate,
    transition
} from '@angular/animations';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { environment } from 'environments/environment';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss', './login.component2.scss'],
    providers: [LoginService],
    animations: [
        trigger('popOverState', [
            state(
                'show',
                style({
                    opacity: 1
                })
            ),
            state(
                'hide',
                style({
                    opacity: 0
                })
            ),
            transition('show => hide', animate('600ms ease-out')),
            transition('hide => show', animate('2000ms ease-in'))
        ])
    ]
})
export class LoginComponent implements OnInit {
    constructor(
        public dialog: MatDialog,
        private loginService: LoginService,
        private router: Router,
        private userSessionService: UserSessionService
    ) {
        this.show = true;
        this.version = environment.version;
    }

    get stateName() {
        return this.show ? 'show' : 'hide';
    }

    selectedOption: string;
    show = false;
    sessionSubs: Subscription;
    loginEmit: String = 'no';
    username;
    password;
    persistLogin = false;
    errorMessage: String = '';
    version;

    ngOnInit() {}

    toggle() {
        this.show = !this.show;
    }

    // tslint:disable-next-line: use-life-cycle-interface
    ngAfterViewInit() {
        // this.toggle();
    }

    // loginModal(): void {
    //     const dialogRef = this.dialog.open(LoginModalComponent);
    //     dialogRef.afterClosed().subscribe(result => {
    //         this.selectedOption = result;
    //     });
    // }

    login() {
        try {
            this.userSessionService
                .login(
                    this.username.toLowerCase(),
                    this.password,
                    this.persistLogin
                )
                .then((res) => {
                    this.router.navigate(['/office']);
                })
                .catch((err) => {
                    const error = err;
                    console.log(err);
                    this.errorMessage = error.error.error.errorFriendly;
                    //  console.log(error.error.message);
                    this.loginError();
                });
        } catch (err) {
            console.log(err);
        }
    }

    loginError(): void {
        const dialogRef = this.dialog.open(AlertComponent, {
            data: {
                icon: 'exclamation-circle',
                iconColor: 'failure',
                title: 'Login Error',
                text: '|   ' + this.errorMessage + '   |',
                time: 2000
            }
        });
    }

    navTab() {
        this.userSessionService.setApp('TAB');

        this.router.navigate(['./tab']);
    }
}
/* prettier-ignore-end */
