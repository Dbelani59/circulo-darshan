import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-side-menu-content-supplier',
  templateUrl: './side-menu-content-supplier.component.html',
  styles: []
})
export class SideMenuContentSupplierComponent implements OnInit {

  constructor(
    public translate: TranslateService
  ) { }

  ngOnInit() {
  }

}
