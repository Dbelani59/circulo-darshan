import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { version } from 'process';
import { UserSessionService } from 'app/core/login/services/user-session.service';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';

@Component({
    selector: 'app-side-menu-content-office',
    templateUrl: './side-menu-content-office.component.html',
    styleUrls: ['../styles/side-menu-content.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SideMenuContentOfficeComponent implements OnInit {
    version;

    constructor(
        public translate: TranslateService,
        private userSessionService: UserSessionService,
        private router: Router
    ) {
        // const feature = require('./version.json');
        // this.version = feature.version;
        this.version = environment.version;
    }

    ngOnInit() {}

    selectTablet() {
        this.userSessionService.setApp('TAB');
        this.router.navigate(['tablet']);
    }
}
