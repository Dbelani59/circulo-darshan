import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-side-menu-content-employee',
  templateUrl: './side-menu-content-employee.component.html',
  styles: []
})
export class SideMenuContentEmployeeComponent implements OnInit {

  constructor(
    public translate: TranslateService
  ) { }

  ngOnInit() {
  }

}
