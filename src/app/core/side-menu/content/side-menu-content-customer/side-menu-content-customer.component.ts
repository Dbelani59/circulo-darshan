import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-side-menu-content-customer',
  templateUrl: './side-menu-content-customer.component.html',
  styles: []
})
export class SideMenuContentCustomerComponent implements OnInit {

  constructor(
    public translate: TranslateService
  ) { }

  ngOnInit() {
  }

}
