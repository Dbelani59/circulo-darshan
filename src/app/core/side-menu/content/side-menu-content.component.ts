import { Component, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { UserSessionService } from 'app/core/login/services/user-session.service';
import { UserSessionModel } from 'app/core/login/services/user-session.model';

@Component({
    selector: 'app-side-menu-content',
    styleUrls: ['./styles/side-menu-content.scss'],
    templateUrl: './side-menu-content.component.html',
    encapsulation: ViewEncapsulation.None
})
export class SideMenuContentComponent {
    app;

    constructor(public translate: TranslateService, public router: Router, public userSessionService: UserSessionService) {
        this.app = userSessionService.user.app;

        this.userSessionService.userSessionEmit.subscribe((res: UserSessionModel) => {
            this.app = res.app;
        });
    }
    OnInit() {}
}
