import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { MatSnackBar, MatDialog } from '@angular/material';
import { NotificationComponent, AlertComponent } from '../shared';
import { interval } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UpdateService {
    mySnackBarRef: any;

    constructor(private swUpdate: SwUpdate, private snackBar: MatSnackBar, private dialog: MatDialog) {
        this.swUpdate.available.subscribe((evt) => {
            // an update is available. Show notification about to update
            console.log('current version is', evt.current);
            console.log('available version is', evt.available);
            this.showUpdateAlert();
        });

        // Check if software update is enabled
        if (!this.swUpdate.isEnabled) {
            console.log('Software update is not enabled..  🙁');
        } else {
            console.log('Software Update Service Running...');
        }

        // Set interval to check for updates
        const everyMinutes$ = interval(60 * 1000);
        everyMinutes$.subscribe(() => {
            try {
                console.log('Checking for updates..');
                console.log(swUpdate);
                swUpdate.checkForUpdate();
            } catch (err) {
                console.log(err);
            }
        });
    }

    // showNotification(vpos, hpos, type, icon = '', message): void {
    //     // for more info about Angular Material snackBar check: https://material.angular.io/components/snack-bar/overview
    //     this.mySnackBarRef = this.snackBar.openFromComponent(NotificationComponent, {
    //         data: {
    //             message: message,
    //             icon,
    //             type,
    //             dismissible: true
    //             // you can add everything you want here
    //         },
    //         duration: 50000,
    //         horizontalPosition: hpos, // 'start' | 'center' | 'end' | 'left' | 'right'
    //         verticalPosition: vpos, // 'top' | 'bottom'
    //         panelClass: ['notification-wrapper']
    //     });

    //     // this is to be able to close it from the NotificationComponent
    //     this.mySnackBarRef.instance.snackBarRef = this.mySnackBarRef;
    // }

    showUpdateAlert(): void {
        const dialogRef = this.dialog.open(AlertComponent, {
            data: {
                icon: 'exclamation-circle',
                iconColor: 'failure',
                title: 'Software Update Available!',
                text: 'Confirm update to new version?',
                options: true
            }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                window.location.reload();
            } else {
                console.log(result);
            }
        });
    }
}
