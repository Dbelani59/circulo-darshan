import { Component, OnInit } from '@angular/core';
import { CommunicationsTemplateStoreService } from '../../services/comunicationStoreService';
import { MatDialog, MatTabChangeEvent } from '@angular/material';
import { CommunicationsTemplateItemAttachmentsEditComponent } from './communications-template-item-attachments/communications-template-item-attachments-edit/communications-template-item-attachments-edit.component';

@Component({
    selector: 'app-communications-template-item',
    templateUrl: './communications-template-item.component.html',
    styleUrls: [
        './communications-template-item.component.scss',
        '../../../../styles/_module-cards.scss'
    ]
})
export class CommunicationsTemplateItemComponent implements OnInit {
    tabIndex: number;
    deviceAddDialogRef: any;
    multiSelect = false;
    constructor(
        public communicationsTemplateStoreService: CommunicationsTemplateStoreService,
        private dialog: MatDialog
    ) {}

    ngOnInit() {}

    itemClick(itemClick) {
        const serviceResult = this.communicationsTemplateStoreService.itemClick(
            itemClick
        );
        if (serviceResult === false) {
            if (this.tabIndex === 1) {
                if (itemClick === 'ITEM_ADD') {
                    this.openAttachmentAddModal();
                }
            }
        }
    }

    openAttachmentAddModal() {
        this.deviceAddDialogRef = this.dialog.open(
            CommunicationsTemplateItemAttachmentsEditComponent,
            {
                data: {
                    formState: !this.communicationsTemplateStoreService
                        ._formStateDisabled,
                    formData: {}
                }
            }
        );

        this.deviceAddDialogRef.afterClosed().subscribe((conBusCon) => {
            if (conBusCon) {
                conBusCon.id = this.communicationsTemplateStoreService.getNewUUID();
                this.communicationsTemplateStoreService.currentRecord.conBusCons.push(
                    conBusCon
                );
            }
        });
    }

    tabChanged(tabChangeEvent: MatTabChangeEvent): void {
        console.log('tabChangeEvent => ', tabChangeEvent);
        console.log('index => ', tabChangeEvent.index);
        this.tabIndex = tabChangeEvent.index;
    }

    onClickContactMethod(data) {
        if (this.multiSelect) {
            if (data.selected) {
                data.selected = !data.selected;
            } else {
                data.selected = true;
            }
        } else {
            const _ = require('lodash');
            const _data = _.cloneDeep(data); // Set main datastore

            this.deviceAddDialogRef = this.dialog.open(
                CommunicationsTemplateItemAttachmentsEditComponent,
                {
                    data: {
                        formState: this.communicationsTemplateStoreService
                            ._formStateDisabled,
                        formData: _data
                    },
                    minWidth: '100vw',
                    height: '100vh'
                }
            );

            this.deviceAddDialogRef.afterClosed().subscribe((conBusCon) => {
                if (conBusCon) {
                    const c = this.communicationsTemplateStoreService
                        .currentRecord.comTemAtts;
                    for (let index = 0; index < c.length; index++) {
                        const element = c[index];
                        if (element.id === conBusCon.id) {
                            c[index] = conBusCon;
                        }
                    }

                    console.log(
                        this.communicationsTemplateStoreService.currentRecord
                            .comTemAtts
                    );
                    // data = conBusCon;
                }
            });
        }
    }
}
