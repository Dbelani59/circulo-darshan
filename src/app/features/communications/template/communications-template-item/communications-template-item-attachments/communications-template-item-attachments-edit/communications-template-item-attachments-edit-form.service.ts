import { Injectable } from '@angular/core';
import { SglLookupsService } from 'app/shared/lookups/sgl-lookups.service';
import { TranslateService } from '@ngx-translate/core';
import { FormlyFieldConfig } from '@ngx-formly/core';

@Injectable({
    providedIn: 'root'
})
export class CommunicationsTemplateItemAttachmentsEditFormService {
    constructor(
        private translate: TranslateService,
        private sglLookupService: SglLookupsService
    ) {}

    fields: FormlyFieldConfig[] = [
        {
            key: 'name',
            type: 'input', // input type
            className: 'col2',
            defaultValue: '',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.template.attachment.name.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.template.attachment.name.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.template.attachment.name.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },

        {
            key: 'attachmentName',
            type: 'input', // input type
            className: 'col4',
            defaultValue: '',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.template.attachment.attachmentName.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.template.attachment.attachmentName.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.template.attachment.attachmentName.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'letterHead',
            type: 'checkbox', // input type
            className: 'col2',
            defaultValue: 'true',
            templateOptions: {
                required: false,
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.template.attachment.letterHead.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.template.attachment.letterHead.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.template.attachment.letterHead..description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        }
    ];
}
