import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CommunicationsTemplateItemAttachmentsEditFormService } from './communications-template-item-attachments-edit-form.service';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-communications-template-item-attachments-edit',
    templateUrl:
        './communications-template-item-attachments-edit.component.html',
    styleUrls: [
        './communications-template-item-attachments-edit.component.scss',
        '../../../../../../styles/_module-cards.scss'
    ]
})
export class CommunicationsTemplateItemAttachmentsEditComponent
    implements OnInit {
    constructor(
        private formFields: CommunicationsTemplateItemAttachmentsEditFormService,
        public dialogRef: MatDialogRef<
            CommunicationsTemplateItemAttachmentsEditComponent
        >,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {}

    options: FormlyFormOptions = {
        formState: {
            disabled: true
        }
    };

    form = new FormGroup({});
    fields: FormlyFieldConfig[] = this.formFields.fields;

    ngOnInit() {
        this.options.formState.disabled = this.data.formState;
    }

    submit() {
        this.dialogRef.close(this.data.formData);
    }
}
