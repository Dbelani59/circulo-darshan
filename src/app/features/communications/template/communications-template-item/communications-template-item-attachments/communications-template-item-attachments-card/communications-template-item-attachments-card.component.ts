import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-communications-template-item-attachments-card',
    templateUrl:
        './communications-template-item-attachments-card.component.html',
    styleUrls: [
        './communications-template-item-attachments-card.component.scss',
        '../../../../../../styles/_module-cards.scss'
    ]
})
export class CommunicationsTemplateItemAttachmentsCardComponent
    implements OnInit {
    @Input() data;

    constructor() {}

    ngOnInit() {}
}
