import { Component, OnInit } from '@angular/core';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { CommunicationsTemplateStoreService } from 'app/features/communications/services/comunicationStoreService';

@Component({
    selector: 'app-communications-template-item-message',
    templateUrl: './communications-template-item-message.component.html',
    styleUrls: [
        './communications-template-item-message.component.scss',
        '../../../../../styles/_module-cards.scss'
    ]
})
export class CommunicationsTemplateItemMessageComponent implements OnInit {
    constructor(
        private translate: TranslateService,
        public communicationsTemplateStoreService: CommunicationsTemplateStoreService
    ) {}
    options: FormlyFormOptions = {
        formState: {
            disabled: true
        }
    };

    fields: FormlyFieldConfig[] = [
        {
            key: 'subject',
            type: 'input', // input type
            className: 'col4',
            defaultValue: '',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.template.subject.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.template.subject.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.template.subject.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        }
    ];

    form = new FormGroup({});

    ngOnInit() {
        this.options.formState.disabled = this.communicationsTemplateStoreService._formStateDisabled;
    }
}
