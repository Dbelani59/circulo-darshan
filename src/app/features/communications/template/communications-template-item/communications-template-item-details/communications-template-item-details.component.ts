import { Component, OnInit } from '@angular/core';
import { CommunicationsTemplateItemDetailsFormService } from './communications-template-item-details-form.service';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { CommunicationsTemplateStoreService } from 'app/features/communications/services/comunicationStoreService';

@Component({
    selector: 'app-communications-template-item-details',
    templateUrl: './communications-template-item-details.component.html',
    styleUrls: [
        './communications-template-item-details.component.scss',
        '../../../../../styles/_module-cards.scss'
    ],
    providers: [CommunicationsTemplateItemDetailsFormService]
})
export class CommunicationsTemplateItemDetailsComponent implements OnInit {
    constructor(
        private formFields: CommunicationsTemplateItemDetailsFormService,
        public communicationsTemplateStoreService: CommunicationsTemplateStoreService
    ) {}

    options: FormlyFormOptions = {
        formState: {
            disabled: true
        }
    };

    form = new FormGroup({});
    fields: FormlyFieldConfig[] = this.formFields.fields;

    ngOnInit() {
        this.options.formState.disabled = this.communicationsTemplateStoreService._formStateDisabled;
    }
}
