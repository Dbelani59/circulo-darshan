import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SglLookupsService } from 'app/shared/lookups/sgl-lookups.service';
import { FormlyFieldConfig } from '@ngx-formly/core';

@Injectable()
export class CommunicationsTemplateItemDetailsFormService {
    constructor(
        private translate: TranslateService,
        private sglLookupService: SglLookupsService
    ) {}

    fields: FormlyFieldConfig[] = [
        {
            key: 'sendType',
            type: 'select',
            className: 'col1',
            templateOptions: {
                required: true,
                options: [
                    { label: 'LETTER', value: 'LET' },
                    { label: 'MOBILE', value: 'MOB' },
                    { label: 'EMAIL', value: 'EMA' },
                    { label: 'APP', value: 'APP' }
                ],
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.template.sendType.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.template.sendType.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.template.sendType.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'name',
            type: 'input', // input type
            className: 'col2',
            defaultValue: '',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.template.name.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.template.name.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.template.name.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },

        {
            key: 'description',
            type: 'input', // input type
            className: 'col4',
            defaultValue: '',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.template.description.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.template.description.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.template.description.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        }
    ];
}
