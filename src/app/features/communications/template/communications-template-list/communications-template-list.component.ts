import { Component, OnInit, AfterViewInit } from '@angular/core';
import { GenericStoreConstructor } from 'app/shared/httpService/generic-store-constructor.interface';
import { CommunicationsTemplateStoreService } from '../../services/comunicationStoreService';
import { UserSessionService } from 'app/core/login/services/user-session.service';
import { Router } from '@angular/router';
import { FormState } from 'app/shared/httpService/generic-store.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-communications-template-list',
    templateUrl: './communications-template-list.component.html',
    styleUrls: ['./communications-template-list.component.scss']
})
export class CommunicationsTemplateListComponent
    implements OnInit, AfterViewInit {
    dataObservable: Observable<[]>;
    private _dataSubstription;

    displayedColumns = ['sendType', 'name', 'description'];

    columnHeader = {
        // Contact
        sendType: 'comTem.sendType.label',
        name: 'comTem.name.label',
        description: 'comTem.description.label'
    };

    constructor(
        private communicationTemplateStoreService: CommunicationsTemplateStoreService,
        private userSessionService: UserSessionService,
        private router: Router
    ) {
        this.dataObservable = this.communicationTemplateStoreService.dataListObservable;
        this._dataSubstription = this.dataObservable.subscribe((res) => {
            console.log(communicationTemplateStoreService.instanceId);
            console.log(res);
        });

        this.refreshData({});
    }

    refreshData(filter: {}) {
        this.communicationTemplateStoreService.loadAll(filter);
    }

    rowClick(row) {
        // console.log(row);
        this.communicationTemplateStoreService.clearSelection();
        this.communicationTemplateStoreService.selectRecords(row);
        this.router.navigate(['/office/communications/template/' + row.id]);
    }

    navBarClick(item: any[]) {
        // Clear existing contacts
        this.communicationTemplateStoreService.clearSelection();
        // ADD EDIT REFRESH WILL ONLY EMILT ONE ELEMENT
        item.forEach((element) => {
            if (element.event === 'DELETE') {
                //  console.log(element.element);
                this.communicationTemplateStoreService.deleteCurrentRecord();
            }
            if (element.event === 'ADD') {
                this.communicationTemplateStoreService.setFormSate(
                    FormState.ADD
                );
                this.router.navigate(['/office/communications/template']);
            }
            if (element.event === 'EDIT') {
                this.communicationTemplateStoreService.selectRecords(
                    element.element
                );
                this.communicationTemplateStoreService.setFormSate(
                    FormState.VIEW
                );
                this.router.navigate(['/office/communications/template']);
            }
            if (element.event === 'REFRESH') {
                this.communicationTemplateStoreService.loadAll({});
            }
        });
    }

    ngOnInit() {}

    ngAfterViewInit() {
        this.refreshData({});
    }
}
