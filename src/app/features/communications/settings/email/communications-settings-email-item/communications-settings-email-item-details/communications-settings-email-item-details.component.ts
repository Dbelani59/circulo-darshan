import { Component, OnInit } from '@angular/core';
import { CommunicationsSettingsEmailStoreService } from 'app/features/communications/services/comunicationStoreService';
import { CommunicationsSettingsEmailItemDetailsFormService } from './communications-settings-email-item-details-form.service';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-communications-settings-email-item-details',
    templateUrl: './communications-settings-email-item-details.component.html',
    styleUrls: [
        './communications-settings-email-item-details.component.scss',
        '../../../../../../styles/_module-cards.scss'
    ],
    providers: [CommunicationsSettingsEmailItemDetailsFormService]
})
export class CommunicationsSettingsEmailItemDetailsComponent implements OnInit {
    constructor(
        public communicationsSettingsEmailStoreService: CommunicationsSettingsEmailStoreService,
        private formFields: CommunicationsSettingsEmailItemDetailsFormService
    ) {}

    options: FormlyFormOptions = {
        formState: {
            disabled: true
        }
    };

    form = new FormGroup({});
    fields: FormlyFieldConfig[] = this.formFields.fields;

    ngOnInit() {
        this.options.formState.disabled = this.communicationsSettingsEmailStoreService._formStateDisabled;
    }
}
