import { Injectable } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { SglLookupsService } from 'app/shared/lookups/sgl-lookups.service';

@Injectable()
export class CommunicationsSettingsEmailItemDetailsFormService {
    constructor(
        private translate: TranslateService,
        private sglLookupService: SglLookupsService
    ) {}

    fields: FormlyFieldConfig[] = [
        {
            key: 'smtpHost',
            type: 'input', // input type
            className: 'col2',
            defaultValue: '',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.settings.email.smtpHost.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.settings.email.smtpHost.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.settings.email.smtpHost.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },

        {
            key: 'port',
            type: 'input', // input type
            className: 'col2',
            defaultValue: '',
            templateOptions: {
                type: 'number',
                required: true,
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.settings.email.port.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.settings.email.port.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.settings.email.port.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'secure',
            type: 'checkbox', // input type
            className: 'col2',
            defaultValue: 'true',
            templateOptions: {
                required: false,
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.settings.email.secure.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.settings.email.secure.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.settings.email.secure.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'authUser',
            type: 'input', // input type
            className: 'col2',
            defaultValue: '',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.settings.email.authUser.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.settings.email.authUser.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.settings.email.authUser.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },

        {
            key: 'authPassword',
            type: 'input', // input type
            className: 'col2',
            defaultValue: '',
            templateOptions: {
                type: 'password',
                required: true,
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.settings.email.authUser.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.settings.email.authUser.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.settings.email.authUser.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'tls',
            type: 'select',
            className: 'col1',
            templateOptions: {
                required: true,
                options: [
                    { label: 'SSLv3', value: 'SSLv3' },
                    { label: 'TLS', value: 'TLS' },
                    { label: 'STARTTLS', value: 'STARTTLS' }
                ],
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.settings.email.tls.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.settings.email.tls.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.settings.email.tls.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        }
    ];
}
