import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-communications-settings-email-item-card',
    templateUrl: './communications-settings-email-item-card.component.html',
    styleUrls: [
        './communications-settings-email-item-card.component.scss',
        '../../../../../../styles/_module-cards.scss'
    ]
})
export class CommunicationsSettingsEmailItemCardComponent implements OnInit {
    @Input() data;

    constructor() {}

    ngOnInit() {}
}
