import { Component, OnInit } from '@angular/core';
import { GenericStoreConstructor } from 'app/shared/httpService/generic-store-constructor.interface';
import { Observable } from 'rxjs';
import { UserSessionService } from 'app/core/login/services/user-session.service';
import { Router } from '@angular/router';
import { CommunicationsSettingsEmailStoreService } from 'app/features/communications/services/comunicationStoreService';
import { FormState } from 'app/shared/httpService/generic-store.service';

@Component({
    selector: 'app-communications-settings-email-list',
    templateUrl: './communications-settings-email-list.component.html',
    styleUrls: ['./communications-settings-email-list.component.scss']
})
export class CommunicationsSettingsEmailListComponent implements OnInit {
    public dataObservable: Observable<[]>;
    private _dataSubstription;

    displayedColumns = ['name', 'description', 'smtpHost', 'authUser'];

    columnHeader = {
        // Contact

        name: 'communications.settings.email.name.label',
        description: 'communications.settings.email.description.label',
        smtpHost: 'communications.settings.email.smtpHost.label',
        authUser: 'communications.settings.email.authUser.label'
    };

    constructor(
        private communicationsSettingsEmailStoreService: CommunicationsSettingsEmailStoreService,
        private userSessionService: UserSessionService,
        private router: Router
    ) {
        this.dataObservable = this.communicationsSettingsEmailStoreService.dataListObservable;
        this._dataSubstription = this.dataObservable.subscribe((res) => {});

        this.refreshData({});
    }

    refreshData(filter: {}) {
        this.communicationsSettingsEmailStoreService.loadAll(filter);
    }

    rowClick(row) {
        // console.log(row);
        this.communicationsSettingsEmailStoreService.clearSelection();
        this.communicationsSettingsEmailStoreService.selectRecords(row);
        this.router.navigate([
            '/office/communications/settings/email/' + row.id
        ]);
    }

    navBarClick(item: any[]) {
        // Clear existing contacts
        this.communicationsSettingsEmailStoreService.clearSelection();
        // ADD EDIT REFRESH WILL ONLY EMILT ONE ELEMENT
        item.forEach((element) => {
            if (element.event === 'DELETE') {
                //  console.log(element.element);
                this.communicationsSettingsEmailStoreService.deleteCurrentRecord();
            }
            if (element.event === 'ADD') {
                this.communicationsSettingsEmailStoreService.setFormSate(
                    FormState.ADD
                );
                this.router.navigate(['/office/communications/template']);
            }
            if (element.event === 'EDIT') {
                this.communicationsSettingsEmailStoreService.selectRecords(
                    element.element
                );
                this.communicationsSettingsEmailStoreService.setFormSate(
                    FormState.VIEW
                );
                this.router.navigate(['/office/communications/template']);
            }
            if (element.event === 'REFRESH') {
                this.communicationsSettingsEmailStoreService.loadAll({});
            }
        });
    }

    ngOnInit() {}
}
