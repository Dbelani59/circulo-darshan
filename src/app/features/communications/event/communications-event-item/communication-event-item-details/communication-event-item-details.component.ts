import { Component, OnInit } from '@angular/core';
import { CommunicationsEventItemDetailsFormService } from './communications-event-item-details-form.service';
import { CommunicationsEventStoreService } from 'app/features/communications/services/comunicationStoreService';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-communication-event-item-details',
    templateUrl: './communication-event-item-details.component.html',
    styleUrls: ['./communication-event-item-details.component.scss'],
    providers: [CommunicationsEventItemDetailsFormService]
})
export class CommunicationEventItemDetailsComponent implements OnInit {
    constructor(
        private formFields: CommunicationsEventItemDetailsFormService,
        public communicationsEventStoreService: CommunicationsEventStoreService
    ) {}

    options: FormlyFormOptions = {
        formState: {
            disabled: true
        }
    };

    form = new FormGroup({});
    fields: FormlyFieldConfig[] = this.formFields.fields;

    ngOnInit() {
        this.options.formState.disabled = this.communicationsEventStoreService._formStateDisabled;
    }
}
