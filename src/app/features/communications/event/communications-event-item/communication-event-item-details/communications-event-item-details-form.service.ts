import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SglLookupsService } from 'app/shared/lookups/sgl-lookups.service';
import { FormlyFieldConfig } from '@ngx-formly/core';
import {
    CommunicationsEventStoreService,
    CommunicationsSettingsEventTypes
} from 'app/features/communications/services/comunicationStoreService';

@Injectable()
export class CommunicationsEventItemDetailsFormService {
    constructor(
        private translate: TranslateService,
        private sglLookupService: SglLookupsService,
        private communicationsEventStoreService: CommunicationsEventStoreService,
        private communicationsSettingsEventTypes: CommunicationsSettingsEventTypes
    ) {}

    fields: FormlyFieldConfig[] = [
        {
            key: 'status',
            type: 'select',
            className: 'col1',
            templateOptions: {
                required: true,
                options: [
                    { label: 'HOLD', value: 'HOLD' },
                    { label: 'READYTOSEND', value: 'READYTOSEND' }
                ],
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.event.status.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.event.status.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.event.status.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        // {
        //     key: 'name',
        //     type: 'input', // input type
        //     className: 'col2',
        //     defaultValue: '',
        //     templateOptions: {
        //         type: 'text',
        //         required: true,
        //         attributes: {}
        //     },
        //     expressionProperties: {
        //         'templateOptions.label': this.translate.stream(
        //             'communications.event.name.label'
        //         ),
        //         'templateOptions.placeholder': this.translate.stream(
        //             'communications.event.name.placeholder'
        //         ),
        //         'templateOptions.description': this.translate.stream(
        //             'communications.event.name.description'
        //         ),
        //         'templateOptions.disabled': 'formState.disabled'
        //     }
        // },

        {
            key: 'description',
            type: 'input', // input type
            className: 'col4',
            defaultValue: '',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.event.description.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.event.description.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.event.description.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'comSysEtyId',
            type: 'select', // input type
            className: 'col2',
            templateOptions: {
                required: true,
                options: this.communicationsSettingsEventTypes
                    .dataListObservable,

                valueProp: 'id',
                labelProp: 'name',
                attributes: {}
            },

            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.event.type.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.event.type.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.event.type.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            //      className: 'flex-1',
            key: 'delayedSendDate',
            type: 'datepicker', // input type
            className: 'col1',
            templateOptions: {
                required: false,
                attributes: {}
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'communications.event.delayedSendDate.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'communications.event.delayedSendDate.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'communications.event.delayedSendDate.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        }
    ];
}
