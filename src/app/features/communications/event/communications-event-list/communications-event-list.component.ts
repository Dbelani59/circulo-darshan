import { Component, OnInit } from '@angular/core';
import { CommunicationsEventStoreService } from '../../services/comunicationStoreService';
import { UserSessionService } from 'app/core/login/services/user-session.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { GenericStoreConstructor } from 'app/shared/httpService/generic-store-constructor.interface';
import { FormState } from 'app/shared/httpService/generic-store.service';

@Component({
    selector: 'app-communications-event-list',
    templateUrl: './communications-event-list.component.html',
    styleUrls: ['./communications-event-list.component.scss']
})
export class CommunicationsEventListComponent implements OnInit {
    dataObservable: Observable<[]>;
    private _dataSubstription;

    displayedColumns = ['status', 'raisedDate', 'description'];

    columnHeader = {
        // Contact
        status: 'communications.event.list.status.label',
        raisedDate: 'communications.event.list.raisedDate.label',
        description: 'comTem.description.label'
    };

    constructor(
        private communicationsEventStoreService: CommunicationsEventStoreService,
        private userSessionService: UserSessionService,
        private router: Router
    ) {
        this.dataObservable = this.communicationsEventStoreService.dataListObservable;
        this._dataSubstription = this.dataObservable.subscribe((res) => {});
        this.refreshData({});
    }

    refreshData(filter: {}) {
        this.communicationsEventStoreService.loadAll(filter);
    }

    rowClick(row) {
        // console.log(row);
        this.communicationsEventStoreService.clearSelection();
        this.communicationsEventStoreService.selectRecords(row);
        this.router.navigate(['/office/communications/event/' + row.id]);
    }

    navBarClick(item: any[]) {
        // Clear existing contacts
        this.communicationsEventStoreService.clearSelection();
        // ADD EDIT REFRESH WILL ONLY EMILT ONE ELEMENT
        item.forEach((element) => {
            if (element.event === 'DELETE') {
                //  console.log(element.element);
                this.communicationsEventStoreService.deleteCurrentRecord();
            }
            if (element.event === 'ADD') {
                this.communicationsEventStoreService.setFormSate(FormState.ADD);
                this.router.navigate(['/office/communications/event']);
            }
            if (element.event === 'EDIT') {
                this.communicationsEventStoreService.selectRecords(
                    element.element
                );
                this.communicationsEventStoreService.setFormSate(
                    FormState.VIEW
                );
                this.router.navigate(['/office/communications/template']);
            }
            if (element.event === 'REFRESH') {
                this.communicationsEventStoreService.loadAll({});
            }
        });
    }

    ngOnInit() {}
}
