// https://stackoverflow.com/questions/44129817/typescript-generic-service/44130739

import { GenericStoreService } from '../../../shared/httpService/generic-store.service';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from '../../../core/login/services/user-session.service';
import { Injectable } from '@angular/core';
import { GenericHttpService } from '../../../shared/httpService/generic-http.service';
import { MatSnackBar } from '@angular/material';
import { GenericStoreConstructor } from 'app/shared/httpService/generic-store-constructor.interface';

@Injectable({
    providedIn: 'root'
})
export class CommunicationsTemplateStoreService extends GenericStoreService {
    constructor(
        http: GenericHttpService,
        matC: MatSnackBar,
        userSessionService: UserSessionService
    ) {
        const _urls: GenericStoreConstructor = {
            urlWithCompany: true,
            genericUrl: '/communications/template',
            dbStructure: {
                comEveToos: {},
                comEveAtts: {}
            }
        };
        super(_urls, http, matC, userSessionService);
    }
}

@Injectable({
    providedIn: 'root'
})
export class CommunicationsEventStoreService extends GenericStoreService {
    constructor(
        http: GenericHttpService,
        matC: MatSnackBar,
        userSessionService: UserSessionService
    ) {
        const _urls: GenericStoreConstructor = {
            urlWithCompany: true,
            genericUrl: '/communications/event',
            dbStructure: {
                comEveToos: {},
                comEveAtts: {}
            }
        };

        super(_urls, http, matC, userSessionService);
    }
}

@Injectable({
    providedIn: 'root'
})
export class CommunicationsSettingsEmailStoreService extends GenericStoreService {
    constructor(
        http: GenericHttpService,
        matC: MatSnackBar,
        userSessionService: UserSessionService
    ) {
        const _urls: GenericStoreConstructor = {
            urlWithCompany: true,
            genericUrl: '/communications/settings/email',
            dbStructure: {
                comSysEmaFro: {}
            }
        };
        super(_urls, http, matC, userSessionService);
    }
}

@Injectable({
    providedIn: 'root'
})
export class CommunicationsSettingsMobileStoreService extends GenericStoreService {
    constructor(
        http: GenericHttpService,
        matC: MatSnackBar,
        userSessionService: UserSessionService
    ) {
        const _urls: GenericStoreConstructor = {
            urlWithCompany: true,
            genericUrl: '/communications/settings/mobile',
            dbStructure: {
                comSysEmaFro: {}
            }
        };
        super(_urls, http, matC, userSessionService);
    }
}

@Injectable({
    providedIn: 'root'
})
export class CommunicationsSettingsLetterStoreService extends GenericStoreService {
    constructor(
        http: GenericHttpService,
        matC: MatSnackBar,
        userSessionService: UserSessionService
    ) {
        const _urls: GenericStoreConstructor = {
            urlWithCompany: true,
            genericUrl: '/communications/settings/letter',
            dbStructure: {
                comSysEmaFro: {}
            }
        };
        super(_urls, http, matC, userSessionService);
    }
}

@Injectable({
    providedIn: 'root'
})
export class CommunicationsSettingsEventTypes extends GenericStoreService {
    constructor(
        http: GenericHttpService,
        matC: MatSnackBar,
        userSessionService: UserSessionService
    ) {
        const _urls: GenericStoreConstructor = {
            loadOnStart: true,
            urlWithCompany: true,
            genericUrl: '/communications/settings/eventtypes',
            dbStructure: {}
        };
        super(_urls, http, matC, userSessionService);
    }
}
