import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared';
import { RouterModule } from '@angular/router';

import { CommunicationsEventItemComponent } from './event/communications-event-item/communications-event-item.component';
import { CommunicationsEventListComponent } from './event/communications-event-list/communications-event-list.component';
import { CommunicationsSettingsEmailItemComponent } from './settings/email/communications-settings-email-item/communications-settings-email-item.component';
import { CommunicationsSettingsEmailListComponent } from './settings/email/communications-settings-email-list/communications-settings-email-list.component';
import { CommunicationsSettingsLetterItemComponent } from './settings/letter/communications-settings-letter-item/communications-settings-letter-item.component';
import { CommunicationsSettingsLetterListComponent } from './settings/letter/communications-settings-letter-list/communications-settings-letter-list.component';
import { CommunicationsSettingsMobileItemComponent } from './settings/mobile/communications-settings-mobile-item/communications-settings-mobile-item.component';
import { CommunicationsSettingsMobileListComponent } from './settings/mobile/communications-settings-mobile-list/communications-settings-mobile-list.component';
import { CommunicationsTemplateItemComponent } from './template/communications-template-item/communications-template-item.component';
import { CommunicationsTemplateListComponent } from './template/communications-template-list/communications-template-list.component';
import { CommunicationEventItemDetailsComponent } from './event/communications-event-item/communication-event-item-details/communication-event-item-details.component';
import { CommunicationsTemplateItemDetailsComponent } from './template/communications-template-item/communications-template-item-details/communications-template-item-details.component';
import { CommunicationsEventItemMessageComponent } from './event/communications-event-item/communications-event-item-message/communications-event-item-message.component';
import { CommunicationsTemplateItemMessageComponent } from './template/communications-template-item/communications-template-item-message/communications-template-item-message.component';
import { CommunicationsTemplateItemAttachmentsCardComponent } from './template/communications-template-item/communications-template-item-attachments/communications-template-item-attachments-card/communications-template-item-attachments-card.component';
import { CommunicationsSettingsEmailItemDetailsComponent } from './settings/email/communications-settings-email-item/communications-settings-email-item-details/communications-settings-email-item-details.component';
import { CommunicationsSettingsEmailItemCardComponent } from './settings/email/communications-settings-email-item/communications-settings-email-item-card/communications-settings-email-item-card.component';
import { CommunicationsTemplateItemAttachmentsEditComponent } from './template/communications-template-item/communications-template-item-attachments/communications-template-item-attachments-edit/communications-template-item-attachments-edit.component';

export const routes = [
    {
        path: '',
        redirectTo: 'templates'
    },

    {
        path: 'templates',
        component: CommunicationsTemplateListComponent
    },
    {
        path: 'template/:comTemId',
        component: CommunicationsTemplateItemComponent
    },
    {
        path: 'events',
        component: CommunicationsEventListComponent
    },
    {
        path: 'event/:comEveId',
        component: CommunicationsEventItemComponent
    },
    {
        path: 'settings/mobiles',
        component: CommunicationsSettingsMobileListComponent
    },
    {
        path: 'settings/mobile/:id',
        component: CommunicationsSettingsMobileItemComponent
    },
    {
        path: 'settings/emails',
        component: CommunicationsSettingsEmailListComponent
    },
    {
        path: 'settings/email/:id',
        component: CommunicationsSettingsEmailItemComponent
    },
    {
        path: 'settings/letter',
        component: CommunicationsSettingsLetterListComponent
    },
    {
        path: 'settings/letter/:id',
        component: CommunicationsSettingsLetterItemComponent
    }
];

@NgModule({
    declarations: [
        CommunicationsEventItemComponent,
        CommunicationsEventListComponent,
        CommunicationsSettingsEmailItemComponent,
        CommunicationsSettingsEmailListComponent,
        CommunicationsSettingsLetterItemComponent,
        CommunicationsSettingsLetterListComponent,
        CommunicationsSettingsMobileItemComponent,
        CommunicationsSettingsMobileListComponent,
        CommunicationsTemplateItemComponent,
        CommunicationsTemplateListComponent,
        CommunicationEventItemDetailsComponent,
        CommunicationsTemplateItemDetailsComponent,
        CommunicationsEventItemMessageComponent,
        CommunicationsTemplateItemMessageComponent,
        CommunicationsTemplateItemAttachmentsCardComponent,
        CommunicationsSettingsEmailItemDetailsComponent,
        CommunicationsSettingsEmailItemCardComponent,
        CommunicationsTemplateItemAttachmentsEditComponent
    ],
    imports: [CommonModule, SharedModule, RouterModule.forChild(routes)],
    exports: [],
    entryComponents: [CommunicationsTemplateItemAttachmentsEditComponent]
})
export class CommunicationsModule {}
