import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs';
import { GenericStoreConstructor } from '../../../shared/httpService/generic-store-constructor.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { UserSessionService } from '../../../core/login/services/user-session.service';
import { FormState } from '../../../shared/httpService/generic-store.service';
import { LocationStoreService } from '../services/locationStoreService';

@Component({
    selector: 'app-location-list',
    templateUrl: './location-list.component.html',
    styleUrls: ['./location-list.component.scss']
})
export class LocationListComponent implements OnDestroy, AfterViewInit {
    displayedColumns = ['cardCode', 'cardName', 'validFrom'];

    columnHeader = {
        // Contact
        cardCode: 'conBus.cardCode.label',
        cardName: 'conBus.cardName.label',
        validFrom: 'conBus.validFrom.label',
        validTo: 'conBus.validTo.label',
        docStatus: 'conBus.docStatus.label'
    };

    dataObservable: Observable<[]>;
    private _dataSubstription;

    constructor(
        private locationStoreService: LocationStoreService,
        private route: ActivatedRoute,
        private router: Router
    ) {
        this.dataObservable = this.locationStoreService.dataListObservable;
        this._dataSubstription = this.dataObservable.subscribe((res) => {
            console.log(locationStoreService.instanceId);
            console.log(res);
        });

        this.refreshData({});
    }

    refreshData(filter: {}) {
        this.locationStoreService.loadAll(filter);
    }

    rowClick(row) {
        // console.log(row);
        this.locationStoreService.clearSelection();
        this.locationStoreService.selectRecords(row);
        this.router.navigate([
            '/office/location/location-list/location/' + row.id
        ]);
    }

    navBarClick(item: any[]) {
        // Clear existing contacts
        this.locationStoreService.clearSelection();

        // Item
        // [
        //  { event: 'Delete',   }
        //  {  }
        // ]

        // ADD EDIT REFRESH WILL ONLY EMILT ONE ELEMENT
        item.forEach((element) => {
            if (element.event === 'DELETE') {
                //  console.log(element.element);
                this.locationStoreService.deleteCurrentRecord();
            }
            if (element.event === 'ADD') {
                this.locationStoreService.setFormSate(FormState.ADD);
                this.router.navigate([
                    '/office/location/location-list/location'
                ]);
            }
            if (element.event === 'EDIT') {
                this.locationStoreService.selectRecords(element.element);
                this.locationStoreService.setFormSate(FormState.VIEW);
                this.router.navigate([
                    '/office/location/location-list/location'
                ]);
            }
            if (element.event === 'REFRESH') {
                this.locationStoreService.loadAll({});
            }
        });
    }

    ngAfterViewInit() {
        this.refreshData({});
    }

    ngOnDestroy() {
        this._dataSubstription.unsubscribe();
    }
}
