import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocationListComponent } from './location-list/location-list.component';
import { RouterModule, CanActivate } from '@angular/router';
import { LocationComponent } from './location-group/location-group.component';
import { SiteComponent } from './location-item/location-item.component';

// Modules
import { SharedModule } from '../../shared';
import { StorageModule } from '../documents/storage.module';
import { ContactModule } from '../contact/contact.module';

// Services

import { DeviceModule } from '../device/device.module';

export const routes = [
    {
        path: 'location-list/location/:conBusTypLocId',
        component: LocationComponent
    },
    {
        path: '',
        redirectTo: 'location-list'
    },

    {
        path: 'location-list',
        component: LocationListComponent
    },

    {
        path: 'location-list/location',
        component: LocationComponent
    }
];

@NgModule({
    declarations: [LocationListComponent, LocationComponent, SiteComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        StorageModule,
        ContactModule,
        DeviceModule
    ],
    providers: [],
    entryComponents: []
})
export class LocationModule {}
