import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ContactMethService } from 'app/features/contact/contact-meth/contact-meth.service';
import {
    MatDialog,
    MatDialogRef,
    MatDialogConfig,
    MatTabChangeEvent
} from '@angular/material';
import { ContactMethCardEditComponent } from 'app/features/contact/contact-meth/contact-meth-card-edit/contact-meth-card-edit.component';
import { elementEventFullName } from '@angular/compiler/src/view_compiler/view_compiler';
import { LocationStoreService } from '../services/locationStoreService';
import { first } from 'rxjs/operators';

@Component({
    selector: 'app-location-group',
    templateUrl: './location-group.component.html',
    styleUrls: [
        './location-group.component.scss',
        '../../../styles/_module-cards.scss'
    ]
})
export class LocationComponent implements OnInit {
    constructor(
        public locationStoreService: LocationStoreService,
        public translate: TranslateService,
        private contactMethService: ContactMethService,
        private dialog: MatDialog
    ) {
        console.log(locationStoreService._dataStoreRecordCount);

        // this.contactMethService.dataListObservable.subscribe((res: any) => {
        //     locationStoreService.currentRecord.conBusCons = res;
        // });

        // Load Once
        this.locationStoreService.dataListObservable
            .pipe(first())
            .subscribe((res: any) => {
                this.contactMethService.load(res);
            });
    }

    isLoading = false;
    showSpinner = false;
    multiSelect = false;
    tabIndex = 0;
    deviceAddDialogRef: MatDialogRef<ContactMethCardEditComponent>;
    ngOnInit() {}

    itemClick(itemClick) {
        const serviceResult = this.locationStoreService.itemClick(itemClick);
        if (serviceResult === false) {
            if (this.tabIndex === 1) {
                if (itemClick === 'ITEM_ADD') {
                    this.openContactAddModal();
                }
            }
        }
    }

    openContactAddModal() {
        this.deviceAddDialogRef = this.dialog.open(
            ContactMethCardEditComponent,
            {
                data: {
                    formState: this.locationStoreService._formStateDisabled,
                    formData: {}
                }
            }
        );

        this.deviceAddDialogRef.afterClosed().subscribe((conBusCon) => {
            if (conBusCon) {
                conBusCon.id = this.locationStoreService.getNewUUID();
                this.locationStoreService.currentRecord.conBusCons.push(
                    conBusCon
                );
            }
        });
    }

    toggleMultiSelect() {
        if (this.multiSelect === true) {
            // Need to remove any currently selected
            const c = this.locationStoreService.currentRecord.conBusCons;
            for (let index = 0; index < c.length; index++) {
                c[index].selected = false;
            }
        }
        this.multiSelect = !this.multiSelect;
        console.log('Set mutli to: ' + this.multiSelect);
    }

    onClickContactMethod(data) {
        if (this.multiSelect) {
            if (data.selected) {
                data.selected = !data.selected;
            } else {
                data.selected = true;
            }
        } else {
            const _ = require('lodash');
            const _data = _.cloneDeep(data); // Set main datastore

            this.deviceAddDialogRef = this.dialog.open(
                ContactMethCardEditComponent,
                {
                    data: {
                        formState: this.locationStoreService._formStateDisabled,
                        formData: _data
                    }
                }
            );

            this.deviceAddDialogRef.afterClosed().subscribe((conBusCon) => {
                if (conBusCon) {
                    const c = this.locationStoreService.currentRecord
                        .conBusCons;
                    for (let index = 0; index < c.length; index++) {
                        const element = c[index];
                        if (element.id === conBusCon.id) {
                            c[index] = conBusCon;
                        }
                    }

                    console.log(
                        this.locationStoreService.currentRecord.conBusCons
                    );
                    // data = conBusCon;
                }
            });
        }
    }

    tabChanged(tabChangeEvent: MatTabChangeEvent): void {
        console.log('tabChangeEvent => ', tabChangeEvent);
        console.log('index => ', tabChangeEvent.index);
        this.tabIndex = tabChangeEvent.index;
    }

    testlog() {
        console.log('sdfsdf');
    }
}
