import { Component, OnInit } from '@angular/core';
import { ContactWorkerService } from '../../../../obsolete/services/contact-worker.service';
import { SglLookupsService } from '../../../shared/lookups/sgl-lookups.service';
import { TranslateService } from '@ngx-translate/core';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { GenericStoreService } from '../../../shared/httpService/generic-store.service';
import { LocationStoreService } from '../services/locationStoreService';

@Component({
    selector: 'app-location-item',
    templateUrl: './location-item.component.html',
    styleUrls: ['../../../styles/_module-cards.scss']
})
export class SiteComponent implements OnInit {
    constructor(
        public locationStoreService: LocationStoreService,
        //     private sglLookupService: SglLookupsService,
        private translate: TranslateService
    ) {
        //  console.log(contactWorkerService.dataStore);
        this.locationStoreService.formStateDisabledObservable.subscribe(
            (res) => {
                if (res === true) {
                    this.options.formState.disabled = true;
                } else {
                    this.options.formState.disabled = false;
                }
            }
        );

        this.locationStoreService.helpStatus.subscribe((res) => {
            //   console.log(res);
            this.showHelp = res;
        });
        console.log(this.locationStoreService.currentRecord);
    }

    form = new FormGroup({});
    showHelp = false;

    options: FormlyFormOptions = {
        formState: {
            disabled: true
        }
    };

    fields: FormlyFieldConfig[] = [
        {
            key: 'cardCode',
            type: 'input', // input type

            className: 'col-6',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {
                    style: 'width: 200px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'conBus.cardCode.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'conBus.cardCode.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'conBus.cardCode.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypLoc.locationName',
            type: 'input', // input type

            defaultValue: '',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {
                    style: 'width: 800px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'conBus.conBusTypLoc.locationName.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'conBus.conBusTypLoc.locationName.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'conBus.conBusTypLoc.locationName.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conAreaId',
            type: 'input', // input type
            className: 'col-6',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {
                    style: 'width: 400px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'conBus.conAreaId.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'conBus.conAreaId.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'conBus.conAreaId.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },

        {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
                {
                    //      className: 'flex-1',
                    key: 'validFrom',
                    type: 'datepicker', // input type
                    templateOptions: {
                        required: false,
                        attributes: {
                            style: 'width: 150px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.validFrom.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.validFrom.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.validFrom.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    //      className: 'flex-1',
                    key: 'validTo',
                    type: 'datepicker', // input type
                    templateOptions: {
                        required: false,
                        attributes: {
                            style: 'width: 150px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.validTo.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.validTo.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.validTo.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        }
    ];

    ngOnInit() {}
}
