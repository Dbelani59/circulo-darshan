import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
    template: `
        <div id="modal-form">
            <form [formGroup]="form" (ngSubmit)="submit(form)">
                <h1 mat-dialog-title>Device Registration</h1>
                <mat-dialog-content>
                    <mat-form-field>
                        <input matInput formControlName="filename" placeholder="Enter Code" />
                    </mat-form-field>
                </mat-dialog-content>
                <mat-dialog-actions>
                    <button mat-button type="submit">Add</button>
                    <button mat-button type="button" (click)="dialogRef.close()">Cancel</button>
                </mat-dialog-actions>
            </form>
        </div>
    `,
    styleUrls: ['../../../../styles/_module-cards.scss']
})
export class DeviceAddDialogComponent implements OnInit {
    form: FormGroup;

    constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<DeviceAddDialogComponent>) {}

    ngOnInit() {
        this.form = this.formBuilder.group({
            filename: ''
        });
    }

    submit(form) {
        this.dialogRef.close(`${form.value.filename}`);
    }
}
