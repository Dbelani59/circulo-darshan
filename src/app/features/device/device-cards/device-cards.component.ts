import { Component, OnInit, Input } from '@angular/core';
import { UserSessionService } from '../../../core/login/services/user-session.service';

import { GenericStoreConstructor } from '../../../shared/httpService/generic-store-constructor.interface';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { AlertComponent } from 'app/shared';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DeviceAddDialogComponent } from './device-add-dialog/device-add-dialog.component';
import { HttpClient } from '@angular/common/http';
import { DeviceStoreService } from '../services/deviceStoreService';

@Component({
    selector: 'app-device-cards',
    templateUrl: './device-cards.component.html',
    styleUrls: [
        './device-cards.component.scss',
        '../../../styles/_module-cards.scss'
    ]
})
export class DeviceCardsComponent implements OnInit {
    constructor(
        private userSessionService: UserSessionService,
        public deviceStoreService: DeviceStoreService,
        private dialog: MatDialog,
        private http: HttpClient
    ) {}
    @Input() conBusId;

    deviceAddDialogRef: MatDialogRef<DeviceAddDialogComponent>;

    rowClick(row) {
        // console.log(row);
        this.deviceStoreService.clearSelection();
        this.deviceStoreService.selectRecords(row);
        // this.router.navigate(['/office/location/location/' + row.id]);
    }

    ngOnInit() {
        if (this.conBusId) {
            this.deviceStoreService.loadAll({});
        } else {
            console.error('input conBusId not provided');
        }
    }

    onAddClick() {
        this.deviceStoreService.currentRecord = {
            conBusId: this.conBusId
        };

        this.deviceStoreService.saveCurrentRecordObserable.subscribe((res) => {
            let sub;
            sub = res;
            const formatDate = new Date(
                sub.registrationCodeExpiryDate
            ).toLocaleString();
            const dialogRef = this.dialog.open(AlertComponent, {
                data: {
                    icon: 'check-circle',
                    iconColor: 'success',
                    title: 'Device Created',
                    // tslint:disable-next-line: max-line-length
                    text:
                        `Device ID: ` +
                        sub.deviceId +
                        `<br>` +
                        `Registration Code: ` +
                        sub.registrationCode +
                        `<br>` +
                        `Expiry Date: ` +
                        formatDate,
                    button: 'OK'
                }
            });
        });
    }

    openAddFileDialog() {
        this.deviceAddDialogRef = this.dialog.open(DeviceAddDialogComponent);

        this.deviceAddDialogRef.afterClosed().subscribe((name) => {
            const body = {
                registrationCode: name
            };
            this.http
                .post(
                    this.userSessionService.BaseAPIURL +
                        '/contacts/' +
                        this.conBusId +
                        '/devices',
                    body
                )
                .subscribe((res: any) => {
                    this.deviceStoreService.noUpdateAddToDataStore(res);
                });
            console.log(name);
        });
    }
}
