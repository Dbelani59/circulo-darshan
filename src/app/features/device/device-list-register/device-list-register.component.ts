import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { UserSessionService } from 'app/core/login/services/user-session.service';
import { GenericStoreConstructor } from 'app/shared/httpService/generic-store-constructor.interface';

import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DeviceRegisterStoreService } from '../services/deviceStoreService';

@Component({
    selector: 'app-device-list-register',
    templateUrl: './device-list-register.component.html',
    styleUrls: ['./device-list-register.component.scss']
})
export class DeviceListRegisterComponent
    implements OnInit, OnDestroy, AfterViewInit {
    dataObservable: Observable<[]>;
    private _dataSubstription;

    displayedColumns = [
        'id',
        'registrationCode',
        'dateConnected',
        'dateLastConnected',
        'info'
    ];

    columnHeader = {
        // Contact
        id: 'devTab.id.label',
        registrationCode: 'devTab.registrationCode.label',
        dateConnected: 'devTab.dateConnected.label',
        dateLastConnected: 'devTab.dateLastConnected.label',
        info: 'devTab.info.label'
    };

    constructor(
        private userSessionService: UserSessionService,
        private deviceRegisterStoreService: DeviceRegisterStoreService,
        private router: Router
    ) {
        this.dataObservable = this.deviceRegisterStoreService.dataListObservable;
        this._dataSubstription = this.dataObservable.subscribe((res) => {
            console.log(deviceRegisterStoreService.instanceId);
            console.log(res);
        });

        this.deviceRegisterStoreService.loadAll({});
    }

    ngOnInit() {}

    rowClick(row) {
        // console.log(row);
        this.deviceRegisterStoreService.clearSelection();
        this.deviceRegisterStoreService.selectRecords(row);
        this.router.navigate(['/office/location/location/' + row.id]);
    }

    navBarClick(item: any[]) {
        // Clear existing contacts
        this.deviceRegisterStoreService.clearSelection();
        // ADD EDIT REFRESH WILL ONLY EMILT ONE ELEMENT
        item.forEach((element) => {
            if (element.event === 'DELETE') {
                //  console.log(element.element);
                this.deviceRegisterStoreService.deleteCurrentRecord();
            }
            // if (element.event === 'ADD') {
            //     this.locationStoreService.setFormSate(FormState.ADD);
            //     this.router.navigate(['/office/location/location']);
            // }
            // if (element.event === 'EDIT') {
            //     this.locationStoreService.selectRecords(element.element);
            //     this.locationStoreService.setFormSate(FormState.VIEW);
            //     this.router.navigate(['/office/location/location']);
            // }
            if (element.event === 'REFRESH') {
                this.deviceRegisterStoreService.loadAll({});
            }
        });
    }
    ngAfterViewInit() {
        this.deviceRegisterStoreService.loadAll({});
    }

    ngOnDestroy() {
        this._dataSubstription.unsubscribe();
    }
}
