import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { GenericStoreConstructor } from '../../../shared/httpService/generic-store-constructor.interface';
import { FormState } from '../../../shared/httpService/generic-store.service';
import { ActivatedRoute, Router } from '@angular/router';

import { DeviceStoreService } from '../services/deviceStoreService';

@Component({
    selector: 'app-device-list',
    templateUrl: './device-list.component.html',
    styleUrls: ['./device-list.component.scss']
})
export class DeviceListComponent implements OnInit {
    displayedColumns = [
        'deviceId',
        'conBus.cardCode',
        'conBus.cardName',
        'dateConnected',
        'dateLastConnected',
        'latitude',
        'longitude'
    ];

    columnHeader = {
        // Contact

        deviceId: 'devTab.deviceId.label',
        dateConnected: 'devTab.dateConnected.label',
        dateLastConnected: 'devTab.dateLastConnected.label',
        latitude: 'devTab.latitude.label',
        longitude: 'devTab.longitude.label',
        'conBus.cardCode': 'conBus.cardCode.label',
        'conBus.cardName': 'conBus.cardName.label'
    };

    dataObservable: Observable<[]>;

    constructor(
        private deviceStoreService: DeviceStoreService,
        private route: ActivatedRoute,
        private router: Router
    ) {
        this.dataObservable = this.deviceStoreService.dataListObservable;

        this.refreshData({});
    }

    refreshData(filter: {}) {
        this.deviceStoreService.loadAll(filter);
    }

    rowClick(row) {
        // console.log(row);
        this.deviceStoreService.clearSelection();
        this.deviceStoreService.selectRecords(row);
        this.router.navigate(['/office/location/location/' + row.id]);
    }

    navBarClick(item: any[]) {
        // Clear existing contacts
        this.deviceStoreService.clearSelection();
        // ADD EDIT REFRESH WILL ONLY EMILT ONE ELEMENT
        item.forEach((element) => {
            if (element.event === 'DELETE') {
                //  console.log(element.element);
                this.deviceStoreService.deleteCurrentRecord();
            }
            if (element.event === 'ADD') {
                this.deviceStoreService.setFormSate(FormState.ADD);
                this.router.navigate(['/office/location/location']);
            }
            if (element.event === 'EDIT') {
                this.deviceStoreService.selectRecords(element.element);
                this.deviceStoreService.setFormSate(FormState.VIEW);
                this.router.navigate(['/office/location/location']);
            }
            if (element.event === 'REFRESH') {
                this.deviceStoreService.loadAll({});
            }
        });
    }

    ngOnInit() {}
}
