import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-device-card',
    templateUrl: './device-card.component.html',
    styleUrls: ['./device-card.component.scss', '../../../styles/_module-cards.scss']
})
export class DeviceCardComponent implements OnInit {
    @Input() device;

    constructor() {}

    ngOnInit() {}
}
