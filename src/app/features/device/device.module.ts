import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeviceListComponent } from './device-list/device-list.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared';

import { DeviceCardsComponent } from './device-cards/device-cards.component';
import { DeviceCardComponent } from './device-card/device-card.component';
import { DeviceItemComponent } from './device-item/device-item.component';
import { DeviceListRegisterComponent } from './device-list-register/device-list-register.component';
import { DeviceAddDialogComponent } from './device-cards/device-add-dialog/device-add-dialog.component';
import { DeviceStoreService } from './services/deviceStoreService';

export const routes = [
    {
        path: '',
        redirectTo: 'device-list'
    },

    {
        path: 'device-list',
        component: DeviceListComponent
    },
    {
        path: 'awaiting',
        component: DeviceListRegisterComponent
    }
    // {
    //     path: 'location/:conBusTypLocId',
    //     component: LocationComponent
    // },
    // {
    //     path: 'location',
    //     component: LocationComponent
    // }
];

@NgModule({
    declarations: [
        DeviceListComponent,
        DeviceCardsComponent,
        DeviceCardComponent,
        DeviceItemComponent,
        DeviceListRegisterComponent,
        DeviceAddDialogComponent
    ],
    imports: [CommonModule, RouterModule.forChild(routes), SharedModule],
    providers: [DeviceStoreService],
    exports: [DeviceCardsComponent, DeviceAddDialogComponent],
    entryComponents: [DeviceAddDialogComponent]
})
export class DeviceModule {}
