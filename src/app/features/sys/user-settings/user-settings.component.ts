import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { text } from 'd3';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styles: []
})
export class UserSettingsComponent implements OnInit {

  form = new FormGroup({});
  model: any = { lang: 'fr' };

  options: FormlyFormOptions = {};

  fields: FormlyFieldConfig[] = [
    {
      key: 'username',
      type: 'input',
      templateOptions: {
        type: 'text',
        label: 'Username',
        placeholder: 'Username',
        required: true
      },
      expressionProperties: {
        'templateOptions.label': this.translate.stream('HOME.TITLE'),
        'templateOptions.placeholder': this.translate.stream('HOME.TITLE'),
      },
    },
    {
      key: 'lang',
      type: 'select',
      templateOptions: {
        label: 'HOME.SELECT',
        required: true,
        change: (field) => this.changeLang(field.formControl.value),
        options: [
          { label: 'fr', value: 'fr' },
          { label: 'en', value: 'en' },
        ],
      },
    },
  ];

  constructor(
   private translate: TranslateService
  ) { }

  ngOnInit() {
  }

  changeLang(lang) {
    this.translate.use(lang);
  }




}
