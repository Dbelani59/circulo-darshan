import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { SharedModule } from '../../shared';
import { ContactSettingsComponent } from './contact-settings/contact-settings.component';
import { RelationshipSettingsComponent } from './contact-settings/relationship-settings/relationship-settings.component';
import { EmployeeSettingsComponent } from './employee-settings/employee-settings.component';

@NgModule({
    declarations: [UserSettingsComponent, ContactSettingsComponent, RelationshipSettingsComponent, EmployeeSettingsComponent],
    imports: [CommonModule, SharedModule]
})
export class SysModule {}
