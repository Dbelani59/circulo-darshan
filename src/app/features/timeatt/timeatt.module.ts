import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeesComponent } from './employees/employees.component';

import { ContactModule } from '../contact/contact.module';
import { RouterModule } from '@angular/router';
import { SitesComponent } from './sites/sites.component';
import { FaceRegistrationComponent } from './face-registration/face-registration.component';
import { SharedModule } from 'app/shared';

export const routes = [
    {
        path: '',
        redirectTo: 'timeatt/employees'
    },
    {
        path: 'face',
        component: FaceRegistrationComponent
        // resolve: {
        //   tableData : ContactListResolver
        // },
        //  canActivate: [AuthGuard]
    },

    {
        path: 'timeatt/employees',
        component: EmployeesComponent
        // resolve: {
        //   tableData : ContactListResolver
        // },
        //  canActivate: [AuthGuard]
    },
    {
        path: 'employees',
        component: EmployeesComponent
        // resolve: {
        //   tableData : ContactListResolver
        // },
        //  canActivate: [AuthGuard]
    },
    {
        path: 'sites',
        component: SitesComponent
        // resolve: {
        //   tableData : ContactListResolver
        // },
        //  canActivate: [AuthGuard]
    }
];

@NgModule({
    declarations: [
        EmployeesComponent,
        SitesComponent,
        FaceRegistrationComponent
    ],
    imports: [
        CommonModule,
        ContactModule,
        SharedModule,
        RouterModule.forChild(routes)
    ]
})
export class TimeattModule {}
