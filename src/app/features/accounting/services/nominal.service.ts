import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from '../../../core/login/services/user-session.service';

@Injectable({
    providedIn: 'root'
})
export class NominalService {
    constructor(
        private http: HttpClient,
        private userSession: UserSessionService
    ) {}

    baseAPIURL = this.userSession.BaseAPIURL;

    getNominals() {
        return this.http.get(this.baseAPIURL + '/acc/nom/acc');
    }
}
