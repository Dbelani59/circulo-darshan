import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// tslint:disable-next-line: import-blacklist
import 'rxjs/Rx';
import { UserSessionService } from '../../../core/login/services/user-session.service';

@Injectable()
export class DococrService {
    constructor(
        private http: HttpClient,
        private userSessionService: UserSessionService
    ) {}

    baseAPIURL = this.userSessionService.BaseAPIURL;

    storeServers(servers) {
        return this.http.put(
            this.baseAPIURL + '/acc/doc/ocr/' + servers.id,
            servers
        );
    }

    process_potential_suppliers(doc_ocr_id) {
        return this.http.post(
            this.baseAPIURL + '/acc/doc/ocr/' + doc_ocr_id + '/con',
            ''
        );
    }

    createMarketingDocument(accDocOcrId) {
        return this.http.post(
            this.baseAPIURL +
                '/acc/doc/ocr/' +
                accDocOcrId +
                '/convertmarketing',
            ''
        );
    }

    getMostLikeyContact(accDocOcrId) {
        return this.http.get(
            this.baseAPIURL + '/acc/doc/ocr/' + accDocOcrId + '/con/mostlikely'
        );
    }

    delete_doc_ocr(doc_ocr_id) {
        return this.http.put(
            this.baseAPIURL + '/acc/doc/ocr/' + doc_ocr_id,
            '{"docStatus": "D"}'
        );
    }

    query_doc_ocr(doc_ocr_id) {
        return this.http.put(
            this.baseAPIURL + '/acc/doc/ocr/' + doc_ocr_id,
            '{"docStatus": "Q"}'
        );
    }

    getNext() {
        return this.http.get(this.baseAPIURL + '/acc/doc/ocr/next');
    }

    getPossibleContants(doc_ocr_id: string) {
        return this.http.get(
            this.baseAPIURL + '/acc/doc/ocr/' + doc_ocr_id + '/con'
        );
    }

    getAll() {
        this.http
            .get(this.baseAPIURL + '/acc/doc/ocr/next')
            .subscribe((response) => {
                const data = response;
                return data;
            });
    }
}
