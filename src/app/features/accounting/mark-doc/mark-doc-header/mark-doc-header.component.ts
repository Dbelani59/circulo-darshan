import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';

@Component({
    selector: 'app-mark-doc-header',
    templateUrl: './mark-doc-header.component.html',
    styleUrls: ['./mark-doc-header.component.scss']
})
export class MarkDocHeaderComponent implements OnInit {

    constructor(
        private translate: TranslateService
    ) { }

    options: FormlyFormOptions = {
        formState: {
            disabled: true,
        },
    };

    form = new FormGroup({});



    fields: FormlyFieldConfig[] = [
        {
            key: 'conBusId',
            type: 'input', // input type
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {
                    style: 'width: 600px; '
                },
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream('conBus.conBusTypBus.cusPaymentGrpId.label'),
                'templateOptions.placeholder': this.translate.stream('conBus.conBusTypBus.cusPaymentGrpId.placeholder'),
                'templateOptions.description': this.translate.stream('conBus.conBusTypBus.cusPaymentGrpId.description'),
                'templateOptions.disabled': 'formState.disabled'

            }
        },
        {
            template: '<div><strong>INVOICE ADDRESS</strong></div>',
        },
        {
            fieldGroupClassName: 'addressLines',
            fieldGroup:
                [

                    {
                        key: 'invAddLine1',
                        type: 'input', // input type
                        templateOptions: {
                            type: 'text',
                            required: false
                        },
                        expressionProperties: {
                            // 'templateOptions.label': this.translate.stream('conBus.conBusTypBus.cusBalanceLimit.label'),
                            'templateOptions.placeholder': this.translate.stream('conBus.conBusTypBus.cusBalanceLimit.placeholder'),
                            'templateOptions.description': this.translate.stream('conBus.conBusTypBus.cusBalanceLimit.description'),
                            'templateOptions.disabled': 'formState.disabled'
                        }
                    },




                ]
        }
    ];




ngOnInit() {
}


}
