import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from '../../../../core/login/services/user-session.service';

@Injectable({
    providedIn: 'root'
})
export class MarkDocHttpService {
    constructor(
        private http: HttpClient,
        private userSessionService: UserSessionService
    ) {}

    create(contact) {
        return this.http.post(
            this.userSessionService.BaseAPIURL + '/con',
            contact
        );
    }

    delete(contact) {
        return this.http.delete(
            this.userSessionService.BaseAPIURL + '/con/' + contact.id
        );
    }

    update(contact) {
        return this.http.put(
            this.userSessionService.BaseAPIURL + '/con/' + contact.id,
            contact
        );
    }

    // get(id) {

    //   const headerDict = {
    //     request: JSON.stringify({
    //       filter: {
    //         relationshipType: relationShipType },
    //     })
    //   };

    //   const requestOptions = {
    //     headers: new HttpHeaders(headerDict),
    //   };

    //   return this.http.get(this.userSessionService.BaseAPIURL + '/con', requestOptions)
    //     .map(
    //       (response) => {
    //         const data = response;
    //      //   console.log(data);
    //         return data;

    //       }
    //     );
    // }

    // getALl() {

    //   const headerDict = {
    //     request: JSON.stringify({
    //       filter: {
    //         relationshipType: relationShipType },
    //     })
    //   };

    //   const requestOptions = {
    //     headers: new HttpHeaders(headerDict),
    //   };

    //   return this.http.get(this.userSessionService.BaseAPIURL + '/con', requestOptions)
    //     .map(
    //       (response) => {
    //         const data = response;
    //      //   console.log(data);
    //         return data;

    //       }
    //     );
    // }
}
