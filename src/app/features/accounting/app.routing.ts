// not used see accounting.module.ts

import { NgModule } from '@angular/core';
 import { Routes, RouterModule } from '@angular/router';
 import { OcrComponent } from './pages/ocr/ocr.component';

  const routes: Routes = [
   { path: '', pathMatch: 'full', redirectTo: 'ocr' },
   { path: 'ocr', component: OcrComponent }
 ];
  @NgModule({
   imports: [RouterModule.forRoot(routes)],
   exports: [RouterModule]
 })
 export class AppRoutingModule { }
  export const routingComponents = [OcrComponent];
