import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DococrService } from '../../../services/dococr.service';
import { AlertComponent } from '../../../../../shared';
import { MatDialog } from '@angular/material';
import { ContactHttpService } from '../../../../../../obsolete/services/contacthttp.service';

@Component({
    selector: 'app-doc-ocr',
    templateUrl: './doc-ocr.component.html',
    styles: [],
    providers: [ContactHttpService]
})
export class DocOcrComponent implements OnInit {
    constructor(
        fb: FormBuilder,
        private dococrService: DococrService,
        private contactServic: ContactHttpService,
        public dialog: MatDialog
    ) {
        this.formStacked = fb.group({
            revTaxAmount: new FormControl('revTaxAmount'),
            revTotalAmount: new FormControl('revTotalAmount'),
            revDate: new FormControl('revDate'),
            revCardCode: new FormControl('revCardCode')
        });
    }

    @Input() servers;
    @Input() contactType;
    @Input() cardName;
    @Output() messageEvent = new EventEmitter<string>();
    @ViewChild('rev_tax_amount', { static: true }) taxField: ElementRef;

    formStacked: FormGroup;
    PS = 'P';
    IC = 'I';

    public alertVAT;
    public alertCredit;
    // contactType = 'P';
    vatperc;
    contacts;
    contactName;

    // tslint:disable-next-line: use-life-cycle-interface
    ngOnChanges() {
        this.IC = 'I';
        this.runAlerts();
        // this.UpdateDocumentTypeBasedOnContact();
        this.taxField.nativeElement.focus();
        //   console.log('Contact Type' + this.contactType);
    }

    ngOnInit() {
        this.IC = 'I';
        this.runAlerts();
    }

    sendDelete() {
        this.dococrService.delete_doc_ocr(this.servers.id).subscribe((res) => res);
        this.messageEvent.emit('delete');
    }

    sendQuery() {
        this.dococrService.query_doc_ocr(this.servers.id).subscribe((res) => res);
        this.messageEvent.emit('query');
    }

    onPSChange(value) {
        this.PS = value;
    }

    onICChange(value) {
        this.IC = value;
    }

    updateContactName() {
        //    this.contactServic.getContact();
    }

    runAlerts() {
        this.alertVAT = '';
        this.alertCredit = '';
        let searchtext;
        searchtext = this.servers.taggunText.toUpperCase();
        if (searchtext.includes('CREDIT') === true) {
            this.alertCredit = 'Warning: Potential credit';
        } else {
            this.alertCredit = '';
        }

        try {
            const vatperc = this.calcVAT();
            //   console.log(vatperc);

            if (vatperc < 0.19 || vatperc > 0.21) {
                this.alertVAT = 'Warning: VAT not 20%';
            } else {
                this.alertVAT = '';
            }
        } catch {
            this.alertVAT = '';
        }
    }

    calcVAT() {
        let netAMT;
        netAMT = this.servers.revTotalAmount / 1.2;
        const vatperc = this.servers.revTaxAmount / netAMT;
        this.vatperc = vatperc;
        return vatperc;
    }

    autoCalcVAT() {
        let netAMT;
        netAMT = this.servers.revTotalAmount / 1.2;
        this.servers.revTaxAmount = this.servers.revTotalAmount - netAMT;
    }

    sendComplete() {
        if (this.servers.revCardCode != null && this.servers.revCardCode !== '') {
            this.servers.taggunStatus = 'REVIEWED';
            // this.servers.docStatus = 'P';
            this.servers.revDocType = this.PS + this.IC;
            this.dococrService.storeServers(this.servers).subscribe(
                (servers) => {
                    const ser = servers;
                    this.dococrService.createMarketingDocument(this.servers.id).subscribe((res) => {
                        //   console.log(res);
                        this.messageEvent.emit('Complete');
                    });
                },
                (error) => console.log(error)
            );
        } else {
            this.basicAlert();
        }
    }

    // UpdateDocumentTypeBasedOnContact () {
    //   console.log(this.inputContactType);
    //   if (this.inputContactType === 'SUPPLIER') {
    //     this.contactType = 'P';
    //   } else {
    //     //    console.log("S")
    //     this.contactType = 'S';
    //   }

    // }

    // RefreshData() {
    //   // Process Potential Suppliers
    //   this.dococrService.getPossibleContants(this.servers.id).subscribe(res => {
    //     // console.log(res)
    //     // Add Contacts to arrary for drop down
    //     this.contacts = res;

    //     console.log(this.servers.revCardCode);
    //     for (const srv of this.contacts) {
    //       console.log(srv);
    //       if (srv.conBus.cardCode === this.servers.revCardCode) {

    //         if (srv.contactType === 'SUPPLIER') {
    //           this.contactType = 'P';
    //         } else {
    //           //    console.log("S")
    //           this.contactType = 'S';
    //         }

    //       }

    //     }
    //   });
    // }

    // You can pass the following options to the alert component:
    // {
    //   icon: 'check', // FontAwesome icon name
    //   iconColor: 'success' | 'failure', // icon color
    //   title: "Here's a message!", // Title of the modal
    //   text: 'The content, // Text of the modal
    //   options: false, // True will display yes or no buttons
    //   input: false, // True will show a text input
    //   button: 'Good', // Texto of the modal button
    //   time: undefined // Time you want the modal to live (ms)
    // }

    basicAlert(): void {
        const dialogRef = this.dialog.open(AlertComponent, {
            data: {
                title: 'YOU HAVE NOT ENTERED A CONTACT CODE',
                button: 'OK'
            }
        });
    }
}
