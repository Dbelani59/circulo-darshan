import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DococrService } from '../../../services/dococr.service';
import { ContactHttpService } from '../../../../../../obsolete/services/contacthttp.service';
import { NominalService } from '../../../services/nominal.service';

@Component({
    selector: 'app-con-bus-quickadd',
    templateUrl: './con-bus-quickadd.component.html',
    styles: [],
    providers: [ContactHttpService, NominalService]
})
export class ConBusQuickaddComponent implements OnInit {
    @Input() ocrCardName;
    @Output() messageEvent = new EventEmitter<object>();

    cardName = '';
    vat = '';
    post_code = '';
    other = '';

    contactType = 'SUPPLIER';
    contact;
    formStacked: FormGroup;

    selectedNominal;
    nominals;
    nominalList = [];

    constructor(
        fb: FormBuilder,
        private dococrService: DococrService,
        private contactHttpService: ContactHttpService,
        private nominalService: NominalService
    ) {
        this.formStacked = fb.group({
            cardName: new FormControl(''),
            vat: new FormControl(''),
            post_code: new FormControl(''),
            other: new FormControl(''),
            nominal: new FormControl(''),
            newsletter: new FormControl(true)
        });
    }

    onContactTypeChange(value) {
        this.contactType = value;
    }
    ngOnInit() {
        this.cardName = this.ocrCardName;
        this.populateDropDown();
    }

    OnChanges() {
        this.cardName = this.ocrCardName;
    }

    Selected(value) {
        this.selectedNominal = value;
        this.selectedNominal = this.selectedNominal.substring(0, 4);

        // console.log(value);
    }

    populateDropDown() {
        this.nominalService.getNominals().subscribe((res) => {
            this.nominals = res;
            this.nominalList = [];
            for (const srv of this.nominals) {
                this.nominalList.push(srv.nomCode + '-' + srv.nomCodeName);
            }
        });
    }

    onAddNew() {
        const server = { cardName: this.cardName, contactType: this.contactType, defaultOcrNominal: this.selectedNominal };
        this.contactHttpService.addContactQuickAdd(server).subscribe((res) => {
            this.contact = res;
            //  console.log(res);
            if (this.cardName !== '') {
                this.contactHttpService
                    .add_con_bus_ocr_help({ conBusId: this.contact.id, searchType: 'NAME', searchString: this.cardName })
                    .subscribe(() => {
                        this.cardName = '';
                    });
            }
            if (this.vat !== '') {
                this.contactHttpService
                    .add_con_bus_ocr_help({ conBusId: this.contact.id, searchType: 'VAT', searchString: this.vat })
                    .subscribe(() => {
                        this.vat = '';
                    });
            }
            if (this.post_code !== '') {
                this.contactHttpService
                    .add_con_bus_ocr_help({ conBusId: this.contact.id, searchType: 'POSTCODE', searchString: this.post_code })
                    .subscribe(() => {
                        this.post_code = '';
                    });
            }
            if (this.other !== '') {
                this.contactHttpService
                    .add_con_bus_ocr_help({ conBusId: this.contact.id, searchType: 'OTHER', searchString: this.other })
                    .subscribe(() => {
                        this.other = '';
                    });
            }
            this.messageEvent.emit({ cardCode: this.contact.cardCode, contactType: this.contactType });
        });
    }
}
