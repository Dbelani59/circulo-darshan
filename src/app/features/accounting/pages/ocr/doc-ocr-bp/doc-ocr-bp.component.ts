import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { DococrService } from '../../../services/dococr.service';
import { ContactHttpService } from '../../../../../../obsolete/services/contacthttp.service';
import { resolveSrv } from 'dns';
import { relType } from '../../../../../../obsolete/services/contactstore.service';

@Component({
    selector: 'app-doc-ocr-bp',
    templateUrl: './doc-ocr-bp.component.html',
    styles: [],
    providers: [ContactHttpService]
})
export class DocOcrBpComponent implements OnInit {
    constructor(fb: FormBuilder, private dococrService: DococrService, private contactHttpService: ContactHttpService) {
        this.formStacked = fb.group({
            cardName: new FormControl(''),
            email: new FormControl(''),
            password: new FormControl(''),
            newsletter: new FormControl(true),
            contactdd: new FormControl('')
        });
    }
    allContacts;
    contacts;
    contactList = [];
    formStacked: FormGroup;
    selectediem = true;
    @Input() servers;
    @Input() revCardCode;
    @Input() refreshview;
    @Output() messageEvent = new EventEmitter<object>();

    selectedCardCode;
    dropdown;
    dropdownlist = [];

    lastrunid = '';

    // tslint:disable-next-line: use-life-cycle-interface
    ngOnChanges() {
        this.RefreshData();
        this.getMostLikelyContact();
    }

    ngOnInit() {
        this.RefreshData();
        this.getMostLikelyContact();
    }

    RefreshData() {
        // Populate Contract DRop Down
        this.populateDropDownOffAllContacts();

        // Process Potential Suppliers
        this.populateListofPotentialContacts();
    }

    populateListofPotentialContacts() {
        // Process Potential Suppliers
        this.dococrService.process_potential_suppliers(this.servers.id).subscribe(() => {
            this.dococrService.getPossibleContants(this.servers.id).subscribe((res) => {
                this.contacts = res;
                this.contactList = [];
                if (this.contacts) {
                    for (const srv of this.contacts) {
                        this.contactList.push(srv.conBus.cardCode + '-' + srv.conBus.cardName);
                    }
                }
                this.selectediem = true;
            });
        });
    }

    onBPSelect(card_code_name) {
        let cardCode;
        cardCode = card_code_name.substring(0, card_code_name.indexOf('-'));

        for (const srv of this.allContacts) {
            if (srv.cardCode === cardCode) {
                this.contactHttpService.getDefaultRelationship(srv.id).subscribe((res) => {
                    let def;
                    def = res;
                    this.messageEvent.emit({ cardCode: cardCode, contactType: def.contactType, cardName: srv.cardName });
                });
            }
        }
    }

    Selected(value) {
        this.onBPSelect(value);
    }

    getMostLikelyContact() {
        if (this.servers.id !== this.lastrunid) {
            this.dococrService.process_potential_suppliers(this.servers.id).subscribe((resa) => {
                this.dococrService.getMostLikeyContact(this.servers.id).subscribe((res) => {
                    let _accDocOcrCon;
                    console.log('Getting most likely contact');
                    console.log(res);
                    _accDocOcrCon = res;
                    this.servers.revCardCode = _accDocOcrCon.conBus.cardCode;

                    this.contactHttpService.getDefaultRelationship(_accDocOcrCon.conBus.id).subscribe((res2) => {
                        let def;
                        def = res2;
                        console.log(res);
                        this.messageEvent.emit({
                            cardCode: _accDocOcrCon.conBus.cardCode,
                            contactType: def.contactType,
                            cardName: _accDocOcrCon.conBus.cardName
                        });
                    });
                });

                this.lastrunid = this.servers.id;
            });
        }
    }

    // Selected(value) {
    //   console.log(value);

    //   this.selectedCardCode = value;

    //   this.selectedCardCode = this.selectedCardCode.substring(0, this.selectedCardCode.indexOf('-'));
    //   console.log(this.selectedCardCode);

    //   let def;

    //   // Get contact_type
    //   for (const srv of this.dropdown) {
    //     if (srv.cardCode === this.selectedCardCode) {
    //       console.log(srv.id);
    //       this.ContactHttpService.getDefaultRelationship(srv.id).subscribe(res => {
    //         def = res;
    //         console.log(res);
    //         this.messageEvent.emit({ cardCode: this.selectedCardCode, contactType: def.contactType });
    //       },
    //         error => {
    //           console.log(error);
    //         });
    //     }
    //   }
    // }

    pettycash() {
        this.Selected('PET001-');
    }

    populateDropDownOffAllContacts() {
        this.contactHttpService.getContacts(relType.ALL).subscribe((res) => {
            //   console.log(res)
            this.dropdown = res;
            this.allContacts = res;
            this.dropdownlist = [];
            for (const srv of this.dropdown) {
                this.dropdownlist.push(srv.cardCode + '-' + srv.cardName);
            }
        });
    }

    delay(ms: number) {
        return new Promise((resolve) => setTimeout(resolve, ms));
    }
}
