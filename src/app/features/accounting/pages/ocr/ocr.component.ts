import { Component, OnInit } from '@angular/core';
import { DococrService } from '../../services/dococr.service';




@Component({
  selector: 'app-ocr',
  templateUrl: './ocr.component.html',
  styles: []
})
export class OcrComponent implements OnInit {


  constructor(
      private dococrService: DococrService,
  ) {
  }




  public accDocOcrId;
  public servers;
  public contactType ;
  public quickAddCardCode;
  public cardCode;
  public cardName;
  public isLoaded = false;
  public refreshview = '';

  ngOnInit() {
    this.onGetNew();

  }

  onGetNew() {



    this.dococrService.getNext()
      .subscribe(
        (servers) => {
       //   console.log('hello');
       //   console.log(servers);
          this.servers = servers;
          this.servers.revDate = this.servers.taggunDate;
          this.servers.revTotalAmount = this.servers.taggunTotalAmount;
          this.servers.revTaxAmount = this.servers.taggunTaxAmount;
    //      console.log(this.servers.taggunTotalAmount);
     //     console.log(this.servers.revCardCode);
          this.refreshview = this.servers.id;
          this.accDocOcrId = this.servers.id;
          this.isLoaded = true;
          this.quickAddCardCode = this.servers.cardCode;
          this.rerunContactCheck();
       //   console.log(this.servers)
        },
        (error) => console.log(error)
      );
  }



rerunContactCheck() {
//   this.dococrService.process_potential_suppliers(this.servers.id).subscribe(
//     res => {
//  //     console.log(res);
//   },  (error) => console.log(error)
//   );
}



  receiveMessage($event) {
  //  console.log($event);
    this.onGetNew();
  }

  recieveAddedCardCode($event) {
 //   console.log('Reveive');
  //  console.log($event);
    this.servers.revCardCode = $event.cardCode;
    this.quickAddCardCode = $event.cardCode;
    if ($event.contactType === 'SUPPLIER') {
    this.contactType = 'P'; } else {
  //    console.log("S")
      this.contactType = 'S';
    }

  }

  recieveSelectedCardCode($event) {
 //   console.log('Reveive');
 //   console.log($event);
    this.servers.revCardCode = $event.cardCode;
    this.quickAddCardCode = $event.cardCode;
    this.cardName = $event.cardName;
    if ($event.contactType === 'SUPPLIER') {
    this.contactType = 'P';
  //  console.log('P');
  } else {
  //    console.log('S');
      this.contactType = 'S';
    }

  }


}

