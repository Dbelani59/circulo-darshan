import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import {HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AccountingOcrResolver implements Resolve<any> {
  resolve(): Promise<any> {
    return new Promise((resolve, reject) => {
      const breadcrumbs = [
        { url: '/accounting/ocr', label: 'Taggum OCR' }
      ];

      return resolve({
        // breadcrumbs: breadcrumbs
      });
    });
  }
}

@Injectable()
export class AccountingOcrListResolver implements Resolve<any> {
  resolve(): Promise<any> {
    return new Promise((resolve, reject) => {
      const breadcrumbs = [
        { url: '/accounting/ocrlist', label: 'Taggum OCR' }
      ];

      return resolve({
        // breadcrumbs: breadcrumbs
      });
    });
  }
}
