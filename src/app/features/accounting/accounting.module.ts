import { NgModule } from '@angular/core';
import { RouterModule, CanActivate } from '@angular/router';

import { NouisliderModule } from 'ng2-nouislider';

import { SharedModule } from '../../shared';

import { HttpClient } from '@angular/common/http';

// import { HttpModule } from '@angular/http';
import { CoreModule } from '../../core/core.module';
import { AccountingOcrResolver, AccountingOcrListResolver } from './accounting.resolver';

import { OcrComponent } from './pages/ocr/ocr.component';
// import { OcrlistComponent } from './pages/ocrlist/ocrlist.component';
import { DococrService } from './services/dococr.service';

import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';

// import { ConBusComponent } from './pages/con-bus/con-bus.component';
import { ConBusQuickaddComponent } from './pages/ocr/con-bus-quickadd/con-bus-quickadd.component';
import { DocOcrComponent } from './pages/ocr/doc-ocr/doc-ocr.component';
import { DocOcrBpComponent } from './pages/ocr/doc-ocr-bp/doc-ocr-bp.component';

import { AuthGuardService as AuthGuard } from '../../shared/auth/auth-guard.service';
import { AuthService } from '../../shared/auth/auth.service';
import { JwtHelperService, JwtModuleOptions, JwtModule } from '@auth0/angular-jwt';
import { DoclistComponent } from './doclist/doclist.component';
import { CustomersComponent } from './salesledger/customers/customers.component';
import { SuppliersComponent } from './purchaseledger/suppliers/suppliers.component';
import { ContactModule } from '../contact/contact.module';

import { MarkDocHeaderComponent } from './mark-doc/mark-doc-header/mark-doc-header.component';
import { MarkDocBodyComponent } from './mark-doc/body/mark-doc-body/mark-doc-body.component';
import { MarkDocFooterComponent } from './mark-doc/mark-doc-footer/mark-doc-footer.component';
import { MarkDocBodyDocsComponent } from './mark-doc/mark-doc-body/mark-doc-body-docs/mark-doc-body-docs.component';
import { MarkDocBodyItmsComponent } from './mark-doc/mark-doc-body/mark-doc-body-itms/mark-doc-body-itms.component';
import { MarkDocComponent } from './mark-doc/mark-doc.component';

export const accountingRoutes = [
    {
        path: '',
        redirectTo: 'landing'
    },
    {
        path: 'landing',
        component: OcrComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'ocr',
        component: OcrComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'ocrlist',
        component: DoclistComponent
    },
    {
        path: 'ocradd',
        component: DoclistComponent
    },
    {
        path: 'customers',
        component: CustomersComponent
    },
    {
        path: 'suppliers',
        component: SuppliersComponent
    },
    {
        path: 'salesledger/quote',
        component: MarkDocComponent
    }
];

export function tokenGetter() {
    return sessionStorage.getItem('username');
}

@NgModule({
    declarations: [
        OcrComponent,
        //  OcrlistComponent,
        //   ConBusComponent,
        ConBusQuickaddComponent,
        DocOcrComponent,
        DocOcrBpComponent,

        DoclistComponent,

        CustomersComponent,

        SuppliersComponent,

        MarkDocComponent,

        MarkDocHeaderComponent,

        MarkDocBodyComponent,

        MarkDocFooterComponent,

        MarkDocBodyDocsComponent,

        MarkDocBodyItmsComponent
    ],
    imports: [
        RouterModule.forChild(accountingRoutes),
        SharedModule,
        NouisliderModule,
        MatPaginatorModule,
        MatSortModule,
        MatTableModule,
        CoreModule,
        ContactModule,
        //   HttpClient,

        JwtModule.forRoot({
            config: {
                tokenGetter: tokenGetter,
                whitelistedDomains: ['example.com'],
                blacklistedRoutes: ['example.com/examplebadroute/']
            }
        })
    ],
    providers: [AccountingOcrResolver, AccountingOcrListResolver, DococrService, JwtHelperService, AuthGuard, AuthService]
})
export class AccountingModule {}
