export {
  AccountingOcrResolver,
  AccountingOcrListResolver
 } from './accounting.resolver';

export { OcrComponent } from './pages/ocr/ocr.component';
export { AccountingModule } from './accounting.module';
