import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessComponent } from './business/business.component';
import { CustomerComponent } from './customer/customer.component';
import { SupplierComponent } from './supplier/supplier.component';
import { SharedModule } from 'app/shared';

@NgModule({
    declarations: [BusinessComponent, CustomerComponent, SupplierComponent],
    imports: [CommonModule, SharedModule]
})
export class BusinessModule {}
