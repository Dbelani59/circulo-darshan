import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { ContactWorkerService } from '../../../../obsolete/services/contact-worker.service';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { SglLookupsService } from '../../../shared/lookups/sgl-lookups.service';

@Component({
    selector: 'app-business',
    templateUrl: './business.component.html',
    styleUrls: ['./business.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class BusinessComponent implements OnInit {
    constructor(
        public contactWorkerService: ContactWorkerService,
        private sglLookupService: SglLookupsService,
        private translate: TranslateService
    ) {
        // (contactWorkerService.dataStore);
        this.contactWorkerService.formEditState.subscribe((res) => {
            if (res === true) {
                this.options.formState.disabled = false;
            } else {
                this.options.formState.disabled = true;
            }
        });

        this.contactWorkerService.helpStatus.subscribe((res) => {
            // console.log(res);
            this.showHelp = res;
        });

        this.sglLookupService.currencys.subscribe((res) => {
            this.currencys = res;
            console.log(this.currencys);
        });
    }

    options: FormlyFormOptions = {
        formState: {
            disabled: true
        }
    };

    form = new FormGroup({});
    showHelp = false;
    // currencys = [{ 'id': '1', 'codeName': 'hello' }, { 'is': '2', 'codeName': 'sdfsd' }];
    currencys;

    orderFields: FormlyFieldConfig[] = [
        {
            key: 'cardCode',
            type: 'input', // input type
            className: 'col-6',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {
                    style: 'width: 200px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream('conBus.cardCode.label'),
                'templateOptions.placeholder': this.translate.stream('conBus.cardCode.placeholder'),
                'templateOptions.description': this.translate.stream('conBus.cardCode.description'),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypBus.companyName',
            type: 'input', // input type
            defaultValue: '',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {
                    style: 'width: 800px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream('conBus.conBusTypBus.companyName.label'),
                'templateOptions.placeholder': this.translate.stream('conBus.conBusTypBus.companyName.placeholder'),
                'templateOptions.description': this.translate.stream('conBus.conBusTypBus.companyName.description'),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conAreaId',
            type: 'input', // input type
            className: 'col-6',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {
                    style: 'width: 400px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream('conBus.conAreaId.label'),
                'templateOptions.placeholder': this.translate.stream('conBus.conAreaId.placeholder'),
                'templateOptions.description': this.translate.stream('conBus.conAreaId.description'),
                'templateOptions.disabled': 'formState.disabled'
            }
        },

        {
            key: 'conBusTypBus.companyRegNo',
            type: 'input', // input type
            templateOptions: {
                type: 'text',
                required: false,
                attributes: {
                    style: 'width: 200px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream('conBus.conBusTypBus.companyRegNo.label'),
                'templateOptions.placeholder': this.translate.stream('conBus.conBusTypBus.companyRegNo.placeholder'),
                'templateOptions.description': this.translate.stream('conBus.conBusTypBus.companyRegNo.description'),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypBus.companyIncorpDate',
            type: 'datepicker', // input type
            templateOptions: {
                required: false,
                attributes: {
                    style: 'width: 150px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream('conBus.conBusTypBus.companyIncorpDate.label'),
                'templateOptions.placeholder': this.translate.stream('conBus.conBusTypBus.companyIncorpDate.placeholder'),
                'templateOptions.description': this.translate.stream('conBus.conBusTypBus.companyIncorpDate.description'),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypBus.conIndId',
            type: 'select', // input type
            templateOptions: {
                required: false,
                options: this.sglLookupService.currencys,
                // options: this.currencys,
                valueProp: 'id',
                labelProp: 'codeName',
                attributes: {
                    style: 'width: 400px;'
                }
            },

            expressionProperties: {
                'templateOptions.label': this.translate.stream('conBus.conBusTypBus.conIndId.label'),
                'templateOptions.placeholder': this.translate.stream('conBus.conBusTypBus.conIndId.placeholder'),
                'templateOptions.description': this.translate.stream('conBus.conBusTypBus.conIndId.description'),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypBus.salesTaxRegNo',
            type: 'input', // input type
            templateOptions: {
                type: 'text',
                required: false,
                attributes: {
                    style: 'width: 200px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream('conBus.conBusTypBus.salesTaxRegNo.label'),
                'templateOptions.placeholder': this.translate.stream('conBus.conBusTypBus.salesTaxRegNo.placeholder'),
                'templateOptions.description': this.translate.stream('conBus.conBusTypBus.salesTaxRegNo.description'),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypBus.currency',
            type: 'select', // input type - select // autocomplete
            templateOptions: {
                valueProp: 'id',
                labelProp: 'codeName',
                required: false,
                options: this.sglLookupService.currencys,
                attributes: {
                    style: 'width: 250px;'
                },
                filter: (term) => this.sglLookupService.filterCurrency(term)
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream('conBus.conBusTypBus.currency.label'),
                'templateOptions.placeholder': this.translate.stream('conBus.conBusTypBus.currency.placeholder'),
                'templateOptions.description': this.translate.stream('conBus.conBusTypBus.currency.description'),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
                {
                    //      className: 'flex-1',
                    key: 'validFrom',
                    type: 'datepicker', // input type
                    templateOptions: {
                        required: false,
                        attributes: {
                            style: 'width: 150px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream('conBus.validFrom.label'),
                        'templateOptions.placeholder': this.translate.stream('conBus.validFrom.placeholder'),
                        'templateOptions.description': this.translate.stream('conBus.validFrom.description'),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    //      className: 'flex-1',
                    key: 'validTo',
                    type: 'datepicker', // input type
                    templateOptions: {
                        required: false,
                        attributes: {
                            style: 'width: 150px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream('conBus.validTo.label'),
                        'templateOptions.placeholder': this.translate.stream('conBus.validTo.placeholder'),
                        'templateOptions.description': this.translate.stream('conBus.validTo.description'),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        }
    ];

    ngOnInit() {}
}
