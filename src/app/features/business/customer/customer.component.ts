import { Component, OnInit } from '@angular/core';

import { ContactWorkerService } from '../../../../obsolete/services/contact-worker.service';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-customer',
    templateUrl: './customer.component.html',
    styles: []
})
export class CustomerComponent implements OnInit {
    constructor(public contactWorkerService: ContactWorkerService, private translate: TranslateService) {
        this.contactWorkerService.formEditState.subscribe((res) => {
            if (res === true) {
                this.options.formState.disabled = false;
            } else {
                this.options.formState.disabled = true;
            }
        });
    }

    options: FormlyFormOptions = {
        formState: {
            disabled: true
        }
    };

    form = new FormGroup({});

    // cus_payment_grp_id, cus_balance_limit, cus_payment_method_id, cus_payment_terms_id, cus_rebate_perc,

    fields: FormlyFieldConfig[] = [
        {
            key: 'conBusTypBus.cusPaymentGrpId',
            type: 'input', // input type
            templateOptions: {
                type: 'text',
                required: true
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream('conBus.conBusTypBus.cusPaymentGrpId.label'),
                'templateOptions.placeholder': this.translate.stream('conBus.conBusTypBus.cusPaymentGrpId.placeholder'),
                'templateOptions.description': this.translate.stream('conBus.conBusTypBus.cusPaymentGrpId.description'),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypBus.cusBalanceLimit',
            type: 'input', // input type
            templateOptions: {
                type: 'text',
                required: false
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream('conBus.conBusTypBus.cusBalanceLimit.label'),
                'templateOptions.placeholder': this.translate.stream('conBus.conBusTypBus.cusBalanceLimit.placeholder'),
                'templateOptions.description': this.translate.stream('conBus.conBusTypBus.cusBalanceLimit.description'),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypBus.cusPaymentMethodId',
            type: 'input', // input type
            templateOptions: {
                type: 'text',
                required: false
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream('conBus.conBusTypBus.cusPaymentMethodId.label'),
                'templateOptions.placeholder': this.translate.stream('conBus.conBusTypBus.cusPaymentMethodId.placeholder'),
                'templateOptions.description': this.translate.stream('conBus.conBusTypBus.cusPaymentMethodId.description'),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypBus.cusPaymentTermsId',
            type: 'input', // input type
            templateOptions: {
                type: 'text',
                required: false
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream('conBus.conBusTypBus.cusPaymentTermsId.label'),
                'templateOptions.placeholder': this.translate.stream('conBus.conBusTypBus.cusPaymentTermsId.placeholder'),
                'templateOptions.description': this.translate.stream('conBus.conBusTypBus.cusPaymentTermsId.description'),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypBus.cusRebatePerc',
            type: 'input', // input type
            templateOptions: {
                type: 'text',
                required: false
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream('conBus.conBusTypBus.cusRebatePerc.label'),
                'templateOptions.placeholder': this.translate.stream('conBus.conBusTypBus.cusRebatePerc.placeholder'),
                'templateOptions.description': this.translate.stream('conBus.conBusTypBus.cusRebatePerc.description'),
                'templateOptions.disabled': 'formState.disabled'
            }
        }
    ];

    onSubmit(f) {}

    ngOnInit() {}
}
