import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    OnChanges
} from '@angular/core';
import { ContactPersonFormService } from './contact-person-form.service';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-contact-person',
    templateUrl: './contact-person.component.html',
    styleUrls: ['./contact-person.component.scss'],
    providers: [ContactPersonFormService]
})
export class ContactPersonComponent implements OnInit, OnChanges {
    constructor(private formFields: ContactPersonFormService) {}

    @Input() value: any;
    @Output() valueChange = new EventEmitter<any>();

    @Input() formStateDisabled: Boolean;

    options: FormlyFormOptions = {
        formState: {
            disabled: true
        }
    };

    form = new FormGroup({});
    fields: FormlyFieldConfig[] = this.formFields.fields;

    ngOnChanges(changes) {
        if (changes.formStateDisabled.currentValue === false) {
            this.options.formState.disabled = false;
        } else {
            this.options.formState.disabled = true;
        }
    }

    ngOnInit() {}
}
