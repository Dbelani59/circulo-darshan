import { Injectable } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { SglLookupsService } from 'app/shared/lookups/sgl-lookups.service';

@Injectable()
export class ContactPersonFormService {
    constructor(
        private translate: TranslateService,
        private sglLookupService: SglLookupsService
    ) {}

    fields: FormlyFieldConfig[] = [
        {
            key: 'conBusTypPer.title',
            type: 'select',
            // defaultValue: 'M',
            templateOptions: {
                options: [
                    { label: 'Mr.', value: 'Mr.' },
                    { label: 'Mrs.', value: 'Mrs.' },
                    { label: 'Mx.', value: 'Mx' },
                    { label: 'Ms.', value: 'Ms.' },
                    { label: 'Miss.', value: 'Miss.' },
                    { label: 'Master.', value: 'Master' }
                ],
                attributes: {
                    style: 'width: 200px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream('title.label'),
                'templateOptions.placeholder': this.translate.stream(
                    'title.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'title.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
                {
                    key: 'conBusTypPer.firstname',
                    type: 'input', // input type
                    //   className: 'col-6',
                    templateOptions: {
                        type: 'text',
                        required: true,
                        attributes: {
                            style: 'width: 400px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.conBusTypPer.firstname.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.conBusTypPer.firstname.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.cardCode.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'conBusTypPer.middlename',
                    type: 'input', // input type
                    //     className: 'col-6',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            style: 'width: 400px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.conBusTypPer.middlename.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.conBusTypPer.middlename.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.cardCode.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            key: 'conBusTypPer.lastname',
            type: 'input', // input type
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {
                    style: 'width: 600px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'conBus.conBusTypPer.lastname.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'conBus.conBusTypPer.lastname.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'conBus.cardCode.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypPer.jobtitle',
            type: 'input', // input type
            templateOptions: {
                type: 'text',
                required: false,
                attributes: {
                    style: 'width: 600px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'conBus.conBusTypPer.jobtitle.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'conBus.conBusTypPer.jobtitle.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'conBus.cardCode.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypPer.company',
            type: 'input', // input type
            templateOptions: {
                type: 'text',
                required: false,
                attributes: {
                    style: 'width: 600px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'conBus.conBusTypPer.company.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'conBus.conBusTypPer.company.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'conBus.cardCode.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypPer.profession',
            type: 'input', // input type
            templateOptions: {
                type: 'text',
                required: false,
                attributes: {
                    style: 'width: 600px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'conBus.conBusTypPer.profession.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'conBus.conBusTypPer.profession.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'conBus.cardCode.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        }
    ];
}
