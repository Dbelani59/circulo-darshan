import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    ViewEncapsulation,
    Inject
} from '@angular/core';
import { ContactWorkerService } from '../../../../../obsolete/services/contact-worker.service';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ContactMethCardEditFormService } from './contact-meth-card-edit-form.service';

@Component({
    selector: 'app-contact-meth-card-edit',
    templateUrl: './contact-meth-card-edit.component.html',
    styleUrls: ['./contact-meth-card-edit.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [ContactMethCardEditFormService]
})
export class ContactMethCardEditComponent implements OnInit {
    constructor(
        private formFields: ContactMethCardEditFormService,
        public dialogRef: MatDialogRef<ContactMethCardEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {}

    options: FormlyFormOptions = {
        formState: {
            disabled: true
        }
    };

    form = new FormGroup({});
    fields: FormlyFieldConfig[] = this.formFields.fields;

    ngOnInit() {
        this.options.formState.disabled = this.data.formState;
    }

    submit() {
        this.dialogRef.close(this.data.formData);
    }
}
