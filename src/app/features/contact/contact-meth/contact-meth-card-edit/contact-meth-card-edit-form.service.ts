import { Injectable } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { SglLookupsService } from 'app/shared/lookups/sgl-lookups.service';

@Injectable()
export class ContactMethCardEditFormService {
    constructor(
        private translate: TranslateService,
        private sglLookupService: SglLookupsService
    ) {}

    fields: FormlyFieldConfig[] = [
        {
            key: 'default',
            type: 'checkbox',
            templateOptions: {
                label: 'Default Contact details'
            }
        },

        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'docType', // app
                    type: 'select', // app
                    className: 'col1',
                    templateOptions: {
                        required: true, // app // To ensure abstraction
                        options: [
                            { label: 'WORK', value: 'W' }, // model
                            { label: 'HOME', value: 'H' },
                            { label: 'OTHER', value: 'O' }
                        ],
                        attributes: {
                            //    style: 'width: 200px; margin-right:15px;' // app
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.conBusCon.docType.label'
                        ), // default from controller ot app
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.conBusCon.docType.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.conBusCon.docType.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'header',
                    type: 'input',
                    className: 'col2',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.conBusCon.header.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.conBusCon.header.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.conVusCon.header.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            template: '<div><strong>ADDRESS</strong></div>'
        },
        {
            fieldGroupClassName: 'addressLines',
            fieldGroup: [
                {
                    key: 'line1',
                    type: 'input', // input type
                    //     className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            // style: 'width: 400px; margin-bottom: 0px'
                        },
                        hideLabel: true
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.conBusCon.line1.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.conBusCon.line1.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.conVusCon.line1.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'line2',
                    type: 'input', // input type

                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //         style: 'width: 400px;'
                        },
                        hideLabel: true
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.conBusCon.line2.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.conBusCon.line2.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.conBusCon.line2.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'line3',
                    type: 'input', // input type

                    //   className: 'col-6',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //           style: 'width: 400px;'
                        },
                        hideLabel: true
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.conBusCon.line3.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.conBusCon.line3.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.conBusCon.line3.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
                // {
                //     key: 'line4',
                //     type: 'input', // input type

                //     //    className: 'col-6',
                //     templateOptions: {
                //         type: 'text',
                //         required: false,
                //         attributes: {
                //             //        style: 'width: 400px;'
                //         },
                //         hideLabel: true
                //     },
                //     expressionProperties: {
                //         'templateOptions.label': this.translate.stream(
                //             'conBus.conBusCon.line4.label'
                //         ),
                //         'templateOptions.placeholder': this.translate.stream(
                //             'conBus.conBusCon.line4.placeholder'
                //         ),
                //         'templateOptions.description': this.translate.stream(
                //             'conBus.conBusCon.line4.description'
                //         ),
                //         'templateOptions.disabled': 'formState.disabled'
                //     }
                // },
                // {
                //     key: 'line5',
                //     type: 'input', // input type

                //     //       className: 'col-6',
                //     templateOptions: {
                //         type: 'text',
                //         required: false,
                //         attributes: {
                //             style: 'margin-bottom: 15px;'
                //         },
                //         hideLabel: true
                //     },
                //     expressionProperties: {
                //         'templateOptions.label': this.translate.stream(
                //             'conBus.conBusCon.line5.label'
                //         ),
                //         'templateOptions.placeholder': this.translate.stream(
                //             'conBus.conBusCon.line5.placeholder'
                //         ),
                //         'templateOptions.description': this.translate.stream(
                //             'conBus.conBusCon.line5.description'
                //         ),
                //         'templateOptions.disabled': 'formState.disabled'
                //     }
                // }
            ]
        },

        {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
                {
                    key: 'zipCode',
                    type: 'input', // input type

                    className: 'col1',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //  style: 'width: 100px; margin-right:15px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.conBusCon.zipCode.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.conBusCon.zipCode.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.conBusCon.zipCode.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'country',
                    type: 'select', // input type

                    className: 'col2',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        options: this.sglLookupService.currencys,
                        // options: this.currencys,
                        valueProp: 'id',
                        labelProp: 'codeName',
                        attributes: {
                            //      style: 'width: 400px;'
                        }
                    },

                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.conBusCon.country.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.conBusCon.country.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.conBusCon.country.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
                {
                    key: 'mobile',
                    type: 'input', // input type

                    className: 'col2',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 400px; margin-right:15px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.conBusCon.mobile.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.conBusCon.mobile.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.conBusCon.mobile.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'email',
                    type: 'input', // input type

                    className: 'col2',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //         style: 'width: 400px; '
                        }
                    },
                    validators: {
                        ip: {
                            expression: (c) =>
                                !c.value ||
                                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
                                    c.value
                                ),
                            message: (error, field: FormlyFieldConfig) =>
                                `"${field.formControl.value}" is not a valid email Address`
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.conBusCon.email.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.conBusCon.email.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.conBusCon.email.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
                {
                    key: 'landline',
                    type: 'input', // input type

                    className: 'col2',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //          style: 'width: 400px; margin-right:15px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.conBusCon.landline.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.conBusCon.landline.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.conBusCon.landline.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'fax',
                    type: 'input', // input type

                    className: 'col2',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //            style: 'width: 400px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.conBusCon.fax.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.conBusCon.fax.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.conBusCon.fax.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            key: 'notes',
            type: 'textarea', // input type

            className: 'notes',
            templateOptions: {
                required: false,
                attributes: {
                    style: 'width: 100%; min-height: 100px'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'conBus.conBusCon.notes.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'conBus.conBusCon.notes.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'conBus.conBusCon.notes.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        }
    ];
}
