import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { ContactWorkerService } from '../../../../../obsolete/services/contact-worker.service';
import { SglLookupsService } from '../../../../shared/lookups/sgl-lookups.service';
import { TranslateService } from '@ngx-translate/core';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-contact-meth-card-edit-purpose',
    templateUrl: './contact-meth-card-edit-purpose.component.html',
    styleUrls: ['./contact-meth-card-edit-purpose.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ContactMethCardEditPurposeComponent implements OnInit {
    @Output() itemClick = new EventEmitter<Boolean>();

    constructor(
        public contactWorkerService: ContactWorkerService,
        private translate: TranslateService,
        private sglLookupService: SglLookupsService
    ) {
        this.contactWorkerService.formEditState.subscribe((res) => {
            if (res === true) {
                this.options.formState.disabled = false;
            } else {
                this.options.formState.disabled = true;
            }
        });
    }

    options: FormlyFormOptions = {
        formState: {
            disabled: true
        }
    };

    form = new FormGroup({});

    model;

    fields: FormlyFieldConfig[] = [
        {
            fieldGroupClassName: 'display-flex2',
            fieldGroup: [
                {
                    key: 'include',
                    type: 'select', // input type

                    templateOptions: {
                        required: false,
                        multiple: true,
                        options: this.sglLookupService.contactPurposes,
                        valueProp: 'id',
                        labelProp: 'details',
                        change: (field, $event) => this.onUpdate(),
                        attributes: {
                            style: 'min-width: 300px; width: 45%; height: 400px; margin-right: 20px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream('general.include'),
                        // 'templateOptions.placeholder': this.translate.stream('conBus.conBusCon.zipCode.placeholder'),
                        // 'templateOptions.description': this.translate.stream('conBus.conBusCon.zipCode.description'),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'exclude',
                    type: 'select', // input type

                    templateOptions: {
                        required: false,
                        multiple: true,
                        options: this.sglLookupService.contactPurposes,
                        valueProp: 'id',
                        labelProp: 'details',
                        change: (field, $event) => this.onUpdate(),
                        attributes: {
                            style: 'min-width: 300px; width: 45%; height: 400px;'
                        }
                    },

                    expressionProperties: {
                        'templateOptions.label': this.translate.stream('general.exclude'),
                        // 'templateOptions.placeholder': this.translate.stream('conBus.conBusCon.country.placeholder'),
                        // 'templateOptions.description': this.translate.stream('conBus.conBusCon.country.description'),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        }
    ];

    onUpdate() {
        // console.log('mdel Inital');
        // console.log(this.model);

        // Check none removed
        const sourceData = this.contactWorkerService.dataStore.conBusCons[0].conBusConPurs;
        //  console.log('sourceData Inital');
        // console.log(sourceData);

        sourceData.forEach((t, i) => {
            let checked: Boolean = false;

            // Check Internal for Match
            this.model.include.forEach((item, inc) => {
                if (item === t.sysConPurId) {
                    checked = true;
                    if (t.docType === 'E') {
                        t.docType = 'I'; // Update to Internal if changed
                    }
                }
            });

            // Check Exernal for Match
            this.model.exclude.forEach((item, inc) => {
                if (item === t.sysConPurId) {
                    checked = true;
                    if (t.docType === 'I') {
                        t.docType = 'E'; // Update to External if changed
                    }
                }
            });

            // if not match then remove
            //  console.log(checked);
            if (checked === false) {
                //    console.log(i);
                sourceData.splice(i, 1);
            }
        });

        // Add Internal
        // Check Internal for Match
        this.model.include.forEach((item, inc) => {
            let checked: Boolean = false;

            sourceData.forEach((t, i) => {
                if (item === t.sysConPurId) {
                    checked = true;
                }
            });

            if (checked === false) {
                sourceData.push({
                    sysConPurId: item,
                    docType: 'I'
                });
            }
        });

        this.model.exclude.forEach((item, inc) => {
            let checked: Boolean = false;

            sourceData.forEach((t, i) => {
                if (item === t.sysConPurId) {
                    checked = true;
                }
            });

            if (checked === false) {
                sourceData.push({
                    sysConPurId: item,
                    docType: 'E'
                });
            }
        });

        // console.log('sourceData end');
        // console.log(sourceData);

        this.contactWorkerService.dataStore.conBusCons[0].conBusConPurs = sourceData;
    }

    ngOnInit() {
        const internal: string[] = [];
        const external: string[] = [];

        const sourceData = this.contactWorkerService.dataStore.conBusCons[0].conBusConPurs;

        sourceData.forEach((t, i) => {
            if (t.docType === 'I') {
                internal.push(t.sysConPurId);
            }
            if (t.docType === 'E') {
                external.push(t.sysConPurId);
            }
        });

        this.model = {
            include: internal,
            exclude: external
        };

        // console.log(this.model);
    }
}
