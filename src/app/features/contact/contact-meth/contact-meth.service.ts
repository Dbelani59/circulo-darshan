import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ContactMethService {
    constructor() {}

    private _dataSubject = new Subject<[]>(); // Main Data Store
    public _dataStore: any[] = [];

    // Observables
    public get dataListObservable() {
        return this._dataSubject.asObservable();
    }

    public load(dataRecords) {
        this._dataStore = dataRecords;

        this._dataSubject.next(Object.assign(this._dataStore));
    }

    public add(dataRecord) {
        this._dataStore.push(dataRecord);

        this._dataSubject.next(Object.assign(this._dataStore));
    }

    public update(dataRecord) {
        // Update record in the store
        this._dataStore.forEach((t, i) => {
            if (t.id === dataRecord.id) {
                this._dataStore[i] = dataRecord;
            }
        });

        this._dataSubject.next(Object.assign(this._dataStore));
    }

    public delete(dataRecord) {
        // Update record in the store
        this._dataStore.forEach((t, i) => {
            if (t.id === dataRecord.id) {
                this._dataStore[i].docStatus = 'D';
            }
        });

        this._dataSubject.next(Object.assign(this._dataStore));
    }
}
