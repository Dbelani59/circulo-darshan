import { Component, OnInit } from '@angular/core';
import { ContactWorkerService } from '../../../../../obsolete/services/contact-worker.service';

import { MatDialogRef, MatDialog } from '@angular/material';
import { ContactMethCardEditComponent } from '../contact-meth-card-edit/contact-meth-card-edit.component';
import { ContactMethService } from '../contact-meth.service';
import { LocationStoreService } from 'app/features/location/services/locationStoreService';

@Component({
    selector: 'app-contact-meth-cards',
    templateUrl: './contact-meth-cards.component.html',
    styleUrls: [
        './contact-meth-cards.component.scss',
        '../../../../styles/_module-cards.scss'
    ]
})
export class ContactMethCardsComponent implements OnInit {
    constructor(
        public contactWorkerService: ContactWorkerService,
        private locationStoreService: LocationStoreService,
        public contactMethService: ContactMethService,
        private dialog: MatDialog
    ) {}

    deviceAddDialogRef: MatDialogRef<ContactMethCardEditComponent>;

    contactData;
    _showContactMeth = false;
    rowNum = 0;
    ngOnInit() {}

    // onAdd() {
    //     this.contactWorkerService.dataStore.conBusCons.push({});
    //     this.rowNum = this.contactWorkerService.dataStore.conBusCons.length - 1;

    //     this._showContactMeth = true;
    // }

    // onClick(row) {
    //     this.rowNum = row;
    //     this._showContactMeth = true;
    // }

    // itemClick(clicked) {
    //     this._showContactMeth = false;
    // }

    openAddFileDialog() {
        this.deviceAddDialogRef = this.dialog.open(
            ContactMethCardEditComponent
        );

        this.deviceAddDialogRef.afterClosed().subscribe((name) => {
            const body = {
                registrationCode: name
            };
            // this.http.post(this.userSessionService.BaseAPIURL + '/contacts/' + this.conBusId + '/devices', body)
            // .subscribe((res: any) => {
            //     this.deviceStoreService.noUpdateAddToDataStore(res);
            // });
            console.log(name);
        });
    }
}
