import { Component, OnInit, Input } from '@angular/core';
import { ContactWorkerService } from '../../../../../obsolete/services/contact-worker.service';

@Component({
    selector: 'app-contact-meth-card',
    templateUrl: './contact-meth-card.component.html',
    styleUrls: [
        './contact-meth-card.component.scss',
        '../../../../styles/_module-cards.scss'
    ]
})
export class ContactMethCardComponent implements OnInit {
    @Input() contactData;

    constructor() {}

    ngOnInit() {}

    onDelete() {}
}
