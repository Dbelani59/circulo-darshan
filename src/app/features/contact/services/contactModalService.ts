// https://stackoverflow.com/questions/44129817/typescript-generic-service/44130739

import { UserSessionService } from '../../../core/login/services/user-session.service';
import { Injectable } from '@angular/core';
import { GenericModalService } from 'app/shared/httpService/generic-modal.service';
import { MatDialog } from '@angular/material';
import { ContactMethCardEditComponent } from '../contact-meth/contact-meth-card-edit/contact-meth-card-edit.component';
import { ContactRtwModalComponent } from '../contact-rtw/contact-rtw-modal/contact-rtw-modal.component';
import { ContactBankItemModalComponent } from '../contact-bank/contact-bank-item-modal/contact-bank-item-modal.component';
import { ContactContItemModalComponent } from '../contact-cont/contact-cont-item-modal/contact-cont-item-modal.component';

@Injectable({
    providedIn: 'root'
})
export class ContactMethModalService extends GenericModalService {
    constructor(dialog: MatDialog) {
        // const _urls: GenericStoreConstructor = {
        //     urlWithCompany: true,
        //     genericUrl: '/payroll/employees',
        //     dbStructure: {
        //         conBusTypLoc: {},
        //         conBusCons: []
        //     }
        // };
        super(dialog, ContactMethCardEditComponent);
    }
}

@Injectable({
    providedIn: 'root'
})
export class ContactRtwModalService extends GenericModalService {
    constructor(dialog: MatDialog) {
        // const _urls: GenericStoreConstructor = {
        //     urlWithCompany: true,
        //     genericUrl: '/payroll/employees',
        //     dbStructure: {
        //         conBusTypLoc: {},
        //         conBusCons: []
        //     }
        // };
        super(dialog, ContactRtwModalComponent);
    }
}

@Injectable({
    providedIn: 'root'
})
export class ContactBankItemModal_Service extends GenericModalService {
    constructor(dialog: MatDialog) {
        // const _urls: GenericStoreConstructor = {
        //     urlWithCompany: true,
        //     genericUrl: '/payroll/employees',
        //     dbStructure: {
        //         conBusTypLoc: {},
        //         conBusCons: []
        //     }
        // };
        super(dialog, ContactBankItemModalComponent);
    }
}

@Injectable({
    providedIn: 'root'
})
export class ContactContractItemModel_Service extends GenericModalService {
    constructor(dialog: MatDialog) {
        // const _urls: GenericStoreConstructor = {
        //     urlWithCompany: true,
        //     genericUrl: '/payroll/employees',
        //     dbStructure: {
        //         conBusTypLoc: {},
        //         conBusCons: []
        //     }
        // };
        super(dialog, ContactContItemModalComponent);
    }
}
