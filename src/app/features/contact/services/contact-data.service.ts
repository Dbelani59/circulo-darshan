import { Injectable } from '@angular/core';
import { UserSessionService } from 'app/core/login/services/user-session.service';
import { GenericHttpService } from 'app/shared/httpService/generic-http.service';
import { ActivatedRoute } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class ContactDataService {
    private all_details = [];
    private url;
    private final_data;
    id;
    constructor(
        userSessionService: UserSessionService,
        private genericHttpService: GenericHttpService,
        private route: ActivatedRoute
    ) {
        this.route.paramMap.subscribe((params) => {
            this.id = params.get('conBusId');
            console.log(this.id);
        });
    }

    insertDetails(data) {
        // if (this.bank_details) {
        //     this.final_data = this.bank_details;
        // }
        this.url =
            'https://api.dev.cmtech.app/api/v1/3455015a-9163-465f-a681-0e9d7c7e5b8e/payroll/employees';
        this.genericHttpService.put(this.url, this.id, data).subscribe(
            (dataResp: any) => {
                let notFound = true;
                const localData: any = dataResp.data[0];
                this.all_details.push(localData + data);
            },
            (error) => console.log(error)
        );
        this.all_details.push(data);
        // console.log(this.sysCompId);
    }

    getDetails() {
        this.url =
            'https://api.dev.cmtech.app/api/v1/3455015a-9163-465f-a681-0e9d7c7e5b8e/payroll/employees';
        // const httpOptions = {
        //     headers: new HttpHeaders({
        //         'Content-Type': 'application/json',
        //         'x-access-token':
        //             'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzeXNVc3JTZXNJZCI6IjQ2MmE4NTNiLTUzMzAtNDczZi04NjM1LTEzZDZmNDgxNTk5MSIsImlhdCI6MTU5OTgwMjE0OSwiZXhwIjoxNjAwNDA2OTQ5fQ.6zTSYHSnovNrBgooAo1fjSOi-OvN27ZfYsGxUzfA1f8'
        //     })
        // };
        // this.myData = this.http.get<any>(this.url, httpOptions).toPromise();
        // console.log(this.myData);
        this.genericHttpService.get(this.url, this.id).subscribe(
            (dataResp: any) => {
                let notFound = true;
                const localData: any = dataResp.data[0];
                this.all_details.push(localData);
            },
            (error) => console.log('Error: ' + error)
        );
        return this.all_details;
    }
}
