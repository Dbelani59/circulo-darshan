import { Injectable } from '@angular/core';
import { UserSessionService } from '../../../core/login/services/user-session.service';
import { GenericHttpService } from '../../../shared/httpService/generic-http.service';

@Injectable({
    providedIn: 'root'
})
export class ContactContService {
    contractDetails = [];
    sessionId;
    url;
    constructor(
        userSession: UserSessionService,
        private genericHttpService: GenericHttpService
    ) {}

    insertContractDetails(data) {
        this.url =
            'https://api.dev.cmtech.app/api/v1/3455015a-9163-465f-a681-0e9d7c7e5b8e/payroll/employees';

        // this.genericHttpService
        //     .put(this.url, data[0].id, JSON.stringify(data))
        //     .subscribe(
        //         (dataResp: any) => {
        //             let notFound = true;
        //             const localData: any = dataResp.data[0];
        //             this.contractDetails.push(localData);
        //         },
        //         (error) => console.log('Error: ' + error)
        //     );
        this.contractDetails.push(data);
        // console.log(this.contractDetails);
    }

    getContractDetails(id) {
        this.url =
            'https://api.dev.cmtech.app/api/v1/3455015a-9163-465f-a681-0e9d7c7e5b8e/payroll/employees';
        this.genericHttpService.get(this.url, id).subscribe(
            (dataResp: any) => {
                let notFound = true;
                const localData: any = dataResp.data[0];
                this.contractDetails.push(localData.conBusTypPer);
            },
            (error) => console.log('Error: ' + error)
        );
        console.log(this.contractDetails);
        return this.contractDetails;
    }
}
