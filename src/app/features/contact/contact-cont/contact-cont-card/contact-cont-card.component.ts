import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-contact-cont-card',
    templateUrl: './contact-cont-card.component.html',
    styleUrls: [
        './contact-cont-card.component.scss',
        '../../../../styles/_module-cards.scss'
    ]
})
export class ContactContCardComponent implements OnInit {
    @Input() contactCont: any;
    constructor() {}

    ngOnInit() {}
    
}
