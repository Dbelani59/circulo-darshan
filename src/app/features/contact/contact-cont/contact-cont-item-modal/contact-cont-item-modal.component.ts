import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    ViewEncapsulation,
    Inject
} from '@angular/core';
import { ContactWorkerService } from '../../../../../obsolete/services/contact-worker.service';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ContactContItemModalFromService } from './contact-cont-item-modal-from.service';
import { ContactContService } from '../contact-cont.service';

@Component({
    selector: 'app-contact-cont-item-modal',
    templateUrl: './contact-cont-item-modal.component.html',
    styleUrls: ['./contact-cont-item-modal.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [ContactContItemModalFromService]
})
export class ContactContItemModalComponent implements OnInit {
    mydata;
    constructor(
        private formFields: ContactContItemModalFromService,
        public dialogRef: MatDialogRef<ContactContItemModalComponent>,
        private contractService: ContactContService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {}

    options: FormlyFormOptions = {
        formState: {
            disabled: true
        }
    };

    form = new FormGroup({});
    fields: FormlyFieldConfig[] = this.formFields.fields;

    ngOnInit() {
        this.options.formState.disabled = this.data.formState;
    }

    submit() {
        console.log(this.data.formData);
        this.contractService.insertContractDetails(this.data.formData);
        this.dialogRef.close(this.data.formData);
    }
}
