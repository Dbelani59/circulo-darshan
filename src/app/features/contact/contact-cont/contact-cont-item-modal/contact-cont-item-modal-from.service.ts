import { Injectable } from '@angular/core';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { SglLookupsService } from 'app/shared/lookups/sgl-lookups.service';
import { type } from 'os';
import { FormGroup } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class ContactContItemModalFromService {
    myFormData = [];
    days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
    postFix = ['St', 'Et', 'Bt', 'Pr', 'Hrs', 'Pay'];
    full_names = ['Pay Rate', 'Start Time', 'Finish Time', 'Break', 'Hours'];

    constructor(
        private translate: TranslateService,
        private sglLookupService: SglLookupsService
    ) {
        // for (let i = 0; i < this.full_names.length; i++) {
        //     this.myFormData.push({
        //         template:
        //             '<div><strong class="form-control col1">' +
        //             this.full_names[i] +
        //             '</strong></div>'
        //     });
        // }
        for (let i = 0; i < this.days.length; i++) {
            for (let j = 0; j < this.postFix.length; j++) {
                if (j < 3) {
                    this.myFormData.push({
                        key: this.days[i] + this.postFix[j],
                        type: 'input',
                        className: 'col1',
                        templateOptions: {
                            type: 'text',
                            required: false,
                            attributes: {
                                //   style: 'width: 600px; '
                            }
                        },
                        expressionProperties: {
                            'templateOptions.disabled': 'formState.disabled'
                        }
                    });
                } else {
                    this.myFormData.push({
                        key: this.days[i] + this.postFix[j],
                        type: 'input',
                        className: 'col1',
                        templateOptions: {
                            type: 'number',
                            required: false,
                            attributes: {
                                //   style: 'width: 600px; '
                            }
                        },
                        expressionProperties: {
                            'templateOptions.disabled': 'formState.disabled'
                        }
                    });
                }
            }
        }
        console.log(this.myFormData);
    }
    form = new FormGroup({});
    model: any = {};
    options: FormlyFormOptions = {};

    fields: FormlyFieldConfig[] = [
        {
            type: 'tabs',
            fieldGroup: [
                {
                    templateOptions: { label: 'Step 1' },
                    fieldGroup: [
                        {
                            fieldGroupClassName: 'display-flex', // controller or app
                            fieldGroup: [
                                // controller or app
                                {
                                    key: 'cmsAgrSitShiId',
                                    type: 'input',
                                    className: 'col2',
                                    templateOptions: {
                                        type: 'text',
                                        required: false,
                                        label: 'cmsAgrSitShiId',
                                        attributes: {
                                            //   style: 'width: 600px; '
                                        }
                                    },
                                    expressionProperties: {
                                        'templateOptions.disabled':
                                            'formState.disabled'
                                    }
                                },
                                {
                                    key: 'payBasis', // app
                                    type: 'select', // app
                                    className: 'col2',
                                    templateOptions: {
                                        required: true, // app // To ensure abstraction
                                        label: 'payBasis',
                                        options: [
                                            { label: 'SHIFT', value: 'S' }, // model
                                            { label: 'HOURLY', value: 'H' },
                                            { label: 'DAILY', value: 'D' },
                                            { label: 'WEEKLY', value: 'W' },
                                            { label: 'MONTHLY', value: 'M' }
                                        ],
                                        attributes: {
                                            //    style: 'width: 200px; margin-right:15px;' // app
                                        }
                                    },
                                    expressionProperties: {
                                        'templateOptions.disabled':
                                            'formState.disabled'
                                    }
                                }
                            ]
                        },
                        {
                            fieldGroupClassName: 'display-flex', // controller or app
                            fieldGroup: [
                                // controller or app
                                {
                                    key: 'startDate',
                                    type: 'datepicker',
                                    className: 'col2',
                                    templateOptions: {
                                        type: 'text',
                                        required: false,
                                        label: 'Start Date'
                                    },
                                    expressionProperties: {
                                        'templateOptions.disabled':
                                            'formState.disabled'
                                    }
                                },
                                {
                                    key: 'cmsSysPosId',
                                    type: 'input',
                                    className: 'col2',
                                    templateOptions: {
                                        type: 'text',
                                        required: false,
                                        label: 'cmsSysPosId',
                                        attributes: {
                                            //   style: 'width: 600px; '
                                        }
                                    },
                                    expressionProperties: {
                                        'templateOptions.disabled':
                                            'formState.disabled'
                                    }
                                }
                            ]
                        },
                        {
                            fieldGroupClassName: 'display-flex', // controller or app
                            fieldGroup: [
                                // controller or app
                                {
                                    key: 'endDate',
                                    type: 'datepicker',
                                    className: 'col2',
                                    templateOptions: {
                                        type: 'text',
                                        required: false,
                                        attributes: {
                                            //   style: 'width: 600px; '
                                        },
                                        label: 'End Date'
                                    },
                                    expressionProperties: {
                                        'templateOptions.disabled':
                                            'formState.disabled'
                                    }
                                },
                                {
                                    key: 'jobTitle',
                                    type: 'input',
                                    className: 'col2',
                                    templateOptions: {
                                        type: 'text',
                                        required: false,
                                        label: 'jobTitle',
                                        attributes: {
                                            //   style: 'width: 600px; '
                                        }
                                    },
                                    expressionProperties: {
                                        'templateOptions.disabled':
                                            'formState.disabled'
                                    }
                                }
                            ]
                        },
                        {
                            template:
                                '<div class="my_container_row"><div>Mon</div><div>Tue</div><div>Wed</div><div>Thu</div><div>Fri</div><div>Sat</div><div>Sun</div></div>'
                        },
                        {
                            fieldGroupClassName: 'my_container_col',
                            fieldGroup: this.myFormData
                        }
                    ]
                },

                {
                    templateOptions: { label: 'Step 2' },
                    fieldGroup: [
                        {
                            key: 'firstname',
                            type: 'input',
                            templateOptions: {
                                label: 'First name',
                                required: true
                            }
                        },
                        {
                            key: 'age',
                            type: 'input',
                            templateOptions: {
                                type: 'number',
                                label: 'Age',
                                required: true
                            }
                        }
                    ]
                }
            ]
        },
        {
            template: '<div><strong>Id</strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'id',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            template: '<div><strong>Payroll Employee Id:</strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'payEmpId',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            template: '<div><strong>conBusId</strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'conBusId',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'totBt',
                    type: 'input',
                    className: 'col1',
                    templateOptions: {
                        type: 'number',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'totBt'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'avePr',
                    type: 'input',
                    className: 'col1',
                    templateOptions: {
                        type: 'number',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'avePr'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'totHrs',
                    type: 'input',
                    className: 'col1',
                    templateOptions: {
                        type: 'number',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'totHrs'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'totPay',
                    type: 'input',
                    className: 'col1',
                    templateOptions: {
                        type: 'number',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'totPay'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },

        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'cmsShiPatId',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'cmsShiPatId'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'holidayEntitlementDays',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'number',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'holidayEntitlementDays'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'payHolId',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'payHolId'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'paySicId',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'paySicId'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'payPenId',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'payPenId'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'payLifId',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'payLifId'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'payHeaId',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'payHeaId'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'hourRate',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'number',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'hourRate'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'dayRate',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'number',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'dayRate'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'weekRate',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'number',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'weekRate'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'monthRate',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'number',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'monthRate'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'aveHourlyRate',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'number',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'aveHourlyRate'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        }
    ];
}
