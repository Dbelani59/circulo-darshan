import { Component, OnInit, Input } from '@angular/core';
import { ContactBankService } from '../contact-bank.service';

@Component({
    selector: 'app-contact-bank-card',
    templateUrl: './contact-bank-card.component.html',
    styleUrls: ['./contact-bank-card.component.scss']
})
export class ContactBankCardComponent implements OnInit {
    @Input() contactData;

    constructor(private contactBank: ContactBankService) {}
    empData;
    ngOnInit() {
        console.log(this.contactData);
    }
}
