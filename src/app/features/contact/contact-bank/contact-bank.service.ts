import { Injectable } from '@angular/core';
import { UserSessionService } from '../../../core/login/services/user-session.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GenericHttpService } from '../../../shared/httpService/generic-http.service';

@Injectable({
    providedIn: 'root'
})
export class ContactBankService {
    bank_details = [];
    response = [];
    url;
    id;
    headers;
    sysCompId;
    myvar;
    final_data;
    constructor(
        userSessionService: UserSessionService,
        private http: HttpClient,
        private genericHttpService: GenericHttpService
    ) {
        this.sysCompId = userSessionService;
    }

    insertDetails(data, id) {
        console.log(data);
        if (this.bank_details) {
            this.final_data = this.bank_details;
            // this.final_data.push(data);
            data = [{ conBusBans: data }];
            this.final_data.push(data);
        }
        this.url =
            'https://api.dev.cmtech.app/api/v1/3455015a-9163-465f-a681-0e9d7c7e5b8e/payroll/employees';

        this.genericHttpService.put(this.url, id, data).subscribe(
            (dataResp: any) => {
                let notFound = true;
                const localData: any = dataResp.data[0];
                this.bank_details.push(localData + data);
            },
            (error) => console.log(error)
        );
        this.bank_details.push(data);
        console.log('contact-bank-service');
        console.log(this.bank_details);
        // console.log(this.sysCompId);
    }

    getDetails(id) {
        this.url =
            'https://api.dev.cmtech.app/api/v1/3455015a-9163-465f-a681-0e9d7c7e5b8e/payroll/employees';
        // const httpOptions = {
        //     headers: new HttpHeaders({
        //         'Content-Type': 'application/json',
        //         'x-access-token':
        //             'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzeXNVc3JTZXNJZCI6IjQ2MmE4NTNiLTUzMzAtNDczZi04NjM1LTEzZDZmNDgxNTk5MSIsImlhdCI6MTU5OTgwMjE0OSwiZXhwIjoxNjAwNDA2OTQ5fQ.6zTSYHSnovNrBgooAo1fjSOi-OvN27ZfYsGxUzfA1f8'
        //     })
        // };
        // this.myData = this.http.get<any>(this.url, httpOptions).toPromise();
        // console.log(this.myData);
        this.genericHttpService.get(this.url, id).subscribe(
            (dataResp: any) => {
                let notFound = true;
                const localData: any = dataResp.data[0];
                this.bank_details.push(localData.conBusBans);
            },
            (error) => console.log('Error: ' + error)
        );
        return this.bank_details;
    }
}
