import { Injectable } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { SglLookupsService } from 'app/shared/lookups/sgl-lookups.service';

@Injectable()
export class ContactBankItemModelService {
    constructor(
        private translate: TranslateService,
        private sglLookupService: SglLookupsService
    ) {}
    today = new Date();
    fields: FormlyFieldConfig[] = [
        {
            template: '<div><strong>Id</strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'id',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            template: '<div><strong>conBusId</strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'conBusId',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'validFrom',
                    type: 'datepicker',
                    className: 'col1',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        label: 'Valid From'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'validTo',
                    type: 'datepicker',
                    className: 'col1',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        },
                        label: 'Valid To'
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            template: '<div><strong>Document Status</strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'docStatus', // app
                    type: 'select', // app
                    className: 'col1',
                    templateOptions: {
                        required: true, // app // To ensure abstraction
                        options: [
                            { label: 'WORK', value: 'W' }, // model
                            { label: 'HOME', value: 'H' },
                            { label: 'OTHER', value: 'O' }
                        ],
                        attributes: {
                            //    style: 'width: 200px; margin-right:15px;' // app
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            template: '<div><strong>Bank Name: </strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'bankName',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            template: '<div><strong>Branch Name: </strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'branchName',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            template: '<div><strong>Sort Name: </strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'sortCode',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            template: '<div><strong>Account No: </strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'accountNo',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            template: '<div><strong>Account Name: </strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'accountName',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            template: '<div><strong>Roll No: </strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'rollNo',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            template: '<div><strong>Payment Reference: </strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'paymentReference',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            template: '<div><strong>iBan: </strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'iban',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            template: '<div><strong>Swift: </strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'swift',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            template: '<div><strong>bacs reference: </strong></div>'
        },
        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'bacsReference',
                    type: 'input',
                    className: 'col3',
                    templateOptions: {
                        type: 'text',
                        required: false,
                        attributes: {
                            //   style: 'width: 600px; '
                        }
                    },
                    expressionProperties: {
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        }
    ];
}
