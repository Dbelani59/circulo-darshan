import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    ViewEncapsulation,
    Inject
} from '@angular/core';
import { ContactWorkerService } from '../../../../../obsolete/services/contact-worker.service';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ContactBankItemModelService } from './contact-bank-item-model-service.service';
import { ContactBankService } from '../contact-bank.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-contact-bank-item-modal',
    templateUrl: './contact-bank-item-modal.component.html',
    styleUrls: ['./contact-bank-item-modal.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [ContactBankItemModelService]
})
export class ContactBankItemModalComponent implements OnInit {
    mydata;
    id;
    conBusBans = [];
    constructor(
        private formFields: ContactBankItemModelService,
        public dialogRef: MatDialogRef<ContactBankItemModalComponent>,
        private contactService: ContactBankService,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private route: ActivatedRoute
    ) {
        this.route.paramMap.subscribe((params) => {
            this.id = params.get('conBusId');
            console.log('contact-bank-item-modal');
            console.log(this.id);
        });
    }

    options: FormlyFormOptions = {
        formState: {
            disabled: true
        }
    };

    form = new FormGroup({});
    fields: FormlyFieldConfig[] = this.formFields.fields;

    ngOnInit() {
        this.options.formState.disabled = this.data.formState;
    }

    ngOnChanges(changes) {
        if (changes.formStateDisabled.currentValue === false) {
            this.options.formState.disabled = false;
        } else {
            this.options.formState.disabled = true;
        }
    }

    submit() {
        //console.log(this.data.formData);
        console.log(this.id);
        this.conBusBans = [{ conBusBans: this.data.formData }];
        console.log(this.conBusBans);
        this.contactService.insertDetails(this.conBusBans, this.id);
        // this.dialogRef.close([this.data.formData]);
    }
}
