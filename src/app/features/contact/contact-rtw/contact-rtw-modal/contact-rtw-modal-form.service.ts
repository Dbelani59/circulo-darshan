import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SglLookupsService } from 'app/shared/lookups/sgl-lookups.service';
import { FormlyFieldConfig } from '@ngx-formly/core';

@Injectable()
export class ContactRtwModalFormService {
    constructor(
        private translate: TranslateService,
        private sglLookupService: SglLookupsService
    ) {}

    fields: FormlyFieldConfig[] = [
        {
            key: 'default',
            type: 'checkbox',
            templateOptions: {
                label: 'Default Contact details'
            }
        },

        {
            fieldGroupClassName: 'display-flex', // controller or app
            fieldGroup: [
                // controller or app
                {
                    key: 'docType', // app
                    type: 'select', // app
                    className: 'col1',
                    templateOptions: {
                        required: true, // app // To ensure abstraction
                        options: [
                            { label: 'List A', value: 'listA' }, // model
                            { label: 'List B - Group 1', value: 'listBG1' },
                            { label: 'List B - Group 2', value: 'listBG2' }
                        ],
                        attributes: {
                            //    style: 'width: 200px; margin-right:15px;' // app
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.conBusCon.docType.label'
                        ), // default from controller ot app
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.conBusCon.docType.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.conBusCon.docType.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'listA',
                    type: 'radio',
                    className: 'col2',
                    hideExpression: (model) => model.docType === 'listA',
                    templateOptions: {
                        required: false,
                        attributes: {},
                        options: [
                            {
                                value: '1',
                                label:
                                    'A passport showing the holder, or a person named in the passport as the child of the holder, is a British citizen or a citizen of the UK and Colonies having the right of abode in the UK.'
                            },
                            {
                                value: '2',
                                label:
                                    'A passport or national identity card showing the holder, or a person named in the passport as the child of the holder, is a national of a European Economic Area country or Switzerland.'
                            },
                            {
                                value: '3',
                                label:
                                    'A Registration Certificate or Document Certifying Permanent Residence issued by the UKVI to a national of a European Economic Area country or Switzerland.'
                            },
                            {
                                value: '4',
                                label:
                                    'A Permanent Residence Card issued by the UKVI to the family member of a national of a European Economic Area country or Switzerland.'
                            },
                            {
                                value: '5',
                                label:
                                    'A current Biometric Immigration Document (Biometric Residence Permit) issued by the UKVI to the holder indicating that the person named is allowed to stay indefinitely in the UK, or has no time limit on their stay in the UK.'
                            },
                            {
                                value: '6',
                                label:
                                    'A current passport endorsed to show that the holder is exempt from immigration control, is allowed to stay indefinitely in the UK, has the right of abode in the UK, or has no time limit on their stay in the UK.'
                            },
                            {
                                value: '7',
                                label:
                                    'A current Immigration Status Document issued by the UKVI to the holder with an endorsement indicating that the named person is allowed to stay indefinitely in the UK or has no time limit on their stay in the UK, together with an official document giving the person’s permanent National Insurance number and their name issued by a Government agency or a previous employer.'
                            },
                            {
                                value: '8',
                                label:
                                    'A birth (short or long) or adoption certificate issued in the UK which includes the name(s) of at least one of the holder’s parents or adoptive parents, together with an official document giving the person’s permanent National Insurance number and their name issued by a Government agency or a previous employer.'
                            },
                            {
                                value: '9',
                                label:
                                    'A birth (short or long) or adoption certificate issued in the Channel Islands, the Isle of Man or Ireland, together with an official document giving the person’s permanent National Insurance number and their name issued by a Government agency or a previous employer.'
                            },
                            {
                                value: '10',
                                label:
                                    'A certificate of registration or naturalisation as a British citizen, together with an official document giving the person’s permanent National Insurance number and their name issued by a Government agency or a previous employer.'
                            }
                        ]
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBus.conBusCon.header.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBus.conBusCon.header.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBus.conVusCon.header.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        }
    ];
}
