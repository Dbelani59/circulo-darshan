import { Component, OnInit, Inject } from '@angular/core';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { ContactRtwModalFormService } from './contact-rtw-modal-form.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-contact-rtw-modal',
    templateUrl: './contact-rtw-modal.component.html',
    styleUrls: ['./contact-rtw-modal.component.scss'],
    providers: [ContactRtwModalFormService]
})
export class ContactRtwModalComponent implements OnInit {
    constructor(
        private formFields: ContactRtwModalFormService,
        public dialogRef: MatDialogRef<ContactRtwModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {}

    options: FormlyFormOptions = {
        formState: {
            disabled: true
        }
    };

    form = new FormGroup({});
    fields: FormlyFieldConfig[] = this.formFields.fields;

    ngOnInit() {
        this.options.formState.disabled = this.data.formState;
    }

    submit() {
        this.dialogRef.close(this.data.formData);
    }
}
