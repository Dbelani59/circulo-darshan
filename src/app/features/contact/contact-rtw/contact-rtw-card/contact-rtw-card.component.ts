import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-contact-rtw-card',
    templateUrl: './contact-rtw-card.component.html',
    styleUrls: [
        './contact-rtw-card.component.scss',
        '../../../../styles/_module-cards.scss'
    ]
})
export class ContactRtwCardComponent implements OnInit {
    @Input() value: any;
    @Input() selected: boolean;
    constructor() {}

    conBusRtw;

    ngOnInit() {}
}
