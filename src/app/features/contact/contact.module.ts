import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, CanActivate } from '@angular/router';

import { SharedModule } from '../../shared';

import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { MatRippleModule, MatTabsModule } from '@angular/material';

import { StorageModule } from '../documents/storage.module';

import { ContactMethCardComponent } from './contact-meth/contact-meth-card/contact-meth-card.component';
import { ContactMethCardEditComponent } from './contact-meth/contact-meth-card-edit/contact-meth-card-edit.component';
// tslint:disable-next-line: max-line-length
import { ContactMethCardEditPurposeComponent } from './contact-meth/contact-meth-card-edit-purpose/contact-meth-card-edit-purpose.component';
import { ContactMethCardsComponent } from './contact-meth/contact-meth-cards/contact-meth-cards.component';
import { ContactPersonComponent } from './contact-person/contact-person.component';
import { ContactRtwCardComponent } from './contact-rtw/contact-rtw-card/contact-rtw-card.component';
import { ContactRtwModalComponent } from './contact-rtw/contact-rtw-modal/contact-rtw-modal.component';

import { ContactContCardComponent } from './contact-cont/contact-cont-card/contact-cont-card.component';
import { ContactContItemModalComponent } from './contact-cont/contact-cont-item-modal/contact-cont-item-modal.component';
import { ContactDbsCardComponent } from './contact-dbs/contact-dbs-card/contact-dbs-card.component';
import { ContactDbsItemModalComponent } from './contact-dbs/contact-dbs-item-modal/contact-dbs-item-modal.component';
import { ContactBankCardComponent } from './contact-bank/contact-bank-card/contact-bank-card.component';
import { ContactBankItemModalComponent } from './contact-bank/contact-bank-item-modal/contact-bank-item-modal.component';
import { FormlyFieldTabs } from './contact-cont/contact-cont-item-modal/tabs.type';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';

export const accountingRoutes = [
    {
        // No direct routes this is a shared module
    }
];

@NgModule({
    declarations: [
        ContactMethCardComponent,
        ContactMethCardEditComponent,
        ContactMethCardEditPurposeComponent,
        ContactMethCardsComponent,

        ContactPersonComponent,
        ContactRtwCardComponent,
        ContactRtwModalComponent,

        ContactContCardComponent,
        ContactContItemModalComponent,

        ContactDbsCardComponent,
        ContactDbsItemModalComponent,
        ContactBankCardComponent,
        ContactBankItemModalComponent,
        FormlyFieldTabs
    ],
    imports: [
        CommonModule,
        SharedModule,
        ReactiveFormsModule,
        MatRippleModule,
        StorageModule,
    ],
    providers: [],
    exports: [
        ContactMethCardsComponent,
        ContactMethCardComponent,
        ContactMethCardEditComponent,
        ContactPersonComponent,
        ContactRtwCardComponent,
        ContactRtwModalComponent,

        ContactContCardComponent,
        ContactContItemModalComponent,

        ContactDbsCardComponent,
        ContactDbsItemModalComponent,

        ContactBankCardComponent,
        ContactBankItemModalComponent
    ],
    entryComponents: [ContactMethCardEditComponent]
})
export class ContactModule {}
