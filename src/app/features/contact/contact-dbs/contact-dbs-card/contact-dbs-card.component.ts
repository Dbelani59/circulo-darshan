import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-contact-dbs-card',
    templateUrl: './contact-dbs-card.component.html',
    styleUrls: ['./contact-dbs-card.component.scss']
})
export class ContactDbsCardComponent implements OnInit {
    @Input() value: any;
    @Input() selected: boolean;
    constructor() {}

    ngOnInit() {}
}
