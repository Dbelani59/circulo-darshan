import { Component, OnInit } from '@angular/core';
import { CompService } from './service/comp.service';
import { UserSessionService } from '../../../core/login/services/user-session.service';

@Component({
    selector: 'app-comp-list',
    templateUrl: './comp-list.component.html',
    styles: []
})
export class CompListComponent implements OnInit {
    displayedColumns: string[] = ['companyCode', 'companyName'];
    dataSource = undefined;

    constructor(private compService: CompService, public userSessionService: UserSessionService) {}

    ngOnInit() {
        this.getListofCompanies();
    }

    getListofCompanies() {
        this.compService.getCompanyList().subscribe(
            (res) => {
                this.dataSource = res;
                //    console.log(res);
            },
            (error) => {
                console.log(error);
            }
        );
    }

    getRecord(row) {
        //   this.userSessionService.setupSession(row.id);
    }
}
