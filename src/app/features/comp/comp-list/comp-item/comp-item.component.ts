import { Component, OnInit, Input } from '@angular/core';
import { CompService } from '../service/comp.service';

@Component({
    selector: 'app-comp-item',
    templateUrl: './comp-item.component.html',
    styles: [],
    providers: [CompService]
})
export class CompItemComponent implements OnInit {
    @Input() comp;

    cfURL: any;

    constructor(private compService: CompService) {}

    // getCFForLogo() {
    //     this.storageService.getCloudfrontURLdocSto(this.comp.logoDocStoId).subscribe((res) => {
    //         this.cfURL = res;
    //     });
    // }

    ngOnInit() {}
}
