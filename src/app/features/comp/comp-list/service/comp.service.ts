import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserSessionService } from '../../../../core/login/services/user-session.service';

@Injectable({
    providedIn: 'root'
})
export class CompService {
    constructor(
        private http: HttpClient,
        private sessionService: UserSessionService
    ) {}

    getCompanyList(): Observable<[]> {
        return this.http.get<[]>(
            this.sessionService.BaseAPIURLWithoutSysCompId + '/tenant'
        );
    }

    getCompany(sysCompId): Observable<[]> {
        return this.http.get<[]>(
            this.sessionService.BaseAPIURLWithoutSysCompId +
                '/tenant/' +
                sysCompId
        );
    }
}
