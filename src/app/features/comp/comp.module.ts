import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompListComponent } from './comp-list/comp-list.component';
import { CompItemComponent } from './comp-list/comp-item/comp-item.component';
import { RouterModule } from '@angular/router';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared';
import { MatTableModule, MatPaginatorModule, MatSortModule } from '@angular/material';

export const companyRoutes = [
    {
        path: '',
        redirectTo: 'tenant-list'
    },

    {
        path: 'tenant-list',
        component: CompListComponent
        //  canActivate: [AuthGuard]
    },
    {
        path: ':con_bus_id',
        component: CompListComponent
        //  canActivate: [AuthGuard]
    }
];

@NgModule({
    declarations: [CompListComponent, CompItemComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(companyRoutes),
        CoreModule,
        SharedModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule
    ]
})
export class CompModule {}
