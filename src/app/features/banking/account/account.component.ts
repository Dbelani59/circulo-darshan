import { Component, OnInit } from '@angular/core';
import { BankingHttpService } from '../services/banking-http.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  constructor(
    private bankingHttpService: BankingHttpService
  ) { }

  ngOnInit() {


  }



  onPlaidSuccess(event) {
    // console.log('Succes');
    console.log(event);
    const token = event.token;
    this.bankingHttpService.exchangeToken(token).subscribe(res => {
    //  console.log('httpService');
     // console.log(res);
    });


  }

  onPlaidExit(event) {
    console.log(event);
  }
  onPlaidLoad(event) {
    console.log(event);
  }

  onPlaidEvent(event) {
    console.log(event);
  }


  onPlaidClick(event) {
    console.log(event);
  }



}
