import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from '../../../core/login/services/user-session.service';

@Injectable({
    providedIn: 'root'
})
export class BankingHttpService {
    constructor(
        private http: HttpClient,
        private userSessionService: UserSessionService
    ) {}

    exchangeToken(token) {
        return this.http.put(
            this.userSessionService.BaseAPIURL + '/acc/ban/open/token',
            { public_token: token }
        );
    }
}
