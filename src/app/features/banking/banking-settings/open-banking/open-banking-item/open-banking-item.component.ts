import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-open-banking-item',
  templateUrl: './open-banking-item.component.html',
  styleUrls: ['./open-banking-item.component.scss']
})
export class OpenBankingItemComponent implements OnInit {

@Input() contactData;

  constructor() { }

  ngOnInit() {
  }

}
