import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material';
import { OpenBankingLinkComponent } from './open-banking-link/open-banking-link.component';
import { BankPlaidHttpService } from './services/bank-plaid-http.service';

@Component({
  selector: 'app-open-banking',
  templateUrl: './open-banking.component.html',
  styleUrls: ['./open-banking.component.scss']
})
export class OpenBankingComponent implements OnInit {

  constructor(
    public translate: TranslateService,
    private bankPlaidHttp: BankPlaidHttpService,
    public dialog: MatDialog
  ) {
    this.refreshPlaid();
  }

_showAcc = false;
_showAdd = false;
_plaidData = [];

  ngOnInit() {
  }

  onClickExit() {
   this._showAdd = false;
  }

onClickAddAcc() {



  this._showAdd = true;
  this._showAcc = false;
}

openDialog(): void {
  const dialogRef = this.dialog.open(OpenBankingLinkComponent, {
    width: '450px',
    height: '450px'
 //   data: {name: this.name, animal: this.animal}
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
   // this.animal = result;
  });

}


refreshPlaid() {

this.bankPlaidHttp.getAllPlaidAccObs().subscribe(res => this._plaidData = res);

}

}
