import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from '../../../../../core/login/services/user-session.service';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class BankPlaidHttpService {
    constructor(
        private http: HttpClient,
        private userSessionService: UserSessionService
    ) {}

    getAllPlaidAccObs(): Observable<[]> {
        return this.http.get<[]>(
            this.userSessionService.BaseAPIURL + '/acc/ban/open'
        );
    }
}
