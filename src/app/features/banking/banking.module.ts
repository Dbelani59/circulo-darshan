import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './account/account.component';
import { RouterModule } from '@angular/router';
import { NgxPlaidLinkModule } from 'ngx-plaid-link';
import { BankingSettingsComponent } from './banking-settings/banking-settings.component';
import { OpenBankingComponent } from './banking-settings/open-banking/open-banking.component';
import { SharedModule } from '../../shared';
import { CoreModule } from '../../core';

import { OpenBankingItemComponent } from './banking-settings/open-banking/open-banking-item/open-banking-item.component';
import { OpenBankingLinkComponent } from './banking-settings/open-banking/open-banking-link/open-banking-link.component';

export const accountingRoutes = [
    {
        path: '',
        redirectTo: 'account'
    },
    {
        path: 'account',
        component: AccountComponent
    },
    {
        path: 'settings',
        component: BankingSettingsComponent
    }
];

@NgModule({
    declarations: [AccountComponent, BankingSettingsComponent, OpenBankingComponent, OpenBankingItemComponent, OpenBankingLinkComponent],
    imports: [CommonModule, RouterModule.forChild(accountingRoutes), NgxPlaidLinkModule, SharedModule, CoreModule],
    entryComponents: [OpenBankingLinkComponent]
})
export class BankingModule {}
