import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportComponent } from './import/import.component';
import { RouterModule } from '@angular/router';
import { CoreModule } from '../../core';
import { EmployeeListComponent } from './employee/employee-list/employee-list.component';
import { EmployeeItemComponent } from './employee/employee-item/employee-item.component';
import { SharedModule } from 'app/shared';
import { EmployeeItemDetailsComponent } from './employee/employee-item/employee-item-details/employee-item-details.component';
import { ContactModule } from '../contact/contact.module';
import { StorageModule } from '../documents/storage.module';
import { PayrollEmployeeContractCardComponent } from './employee/payroll-employee-contract/payroll-employee-contract-card/payroll-employee-contract-card.component';
import { PayrollEmployeeContractModalComponent } from './employee/payroll-employee-contract/payroll-employee-contract-modal/payroll-employee-contract-modal.component';
import { PayrollEmployeeTaxItemComponent } from './employee/payroll-employee-tax/payroll-employee-tax-item/payroll-employee-tax-item.component';
import { PayrollEmployeeOpeningItemComponent } from './employee/payroll-employee-opening/payroll-employee-opening-item/payroll-employee-opening-item.component';
import { ScannerComponent } from './scanner/scanner.component';
import { QrCodeGeneratorComponent } from './qr-code-generator/qr-code-generator.component';
import { QRCodeModule } from 'angularx-qrcode';

// For Scanner
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../../../environments/environment';

export const routes = [
    {
        path: '',
        redirectTo: 'employeeimport'
    },
    {
        path: 'employees',
        component: EmployeeListComponent
    },
    {
        path: 'employee/:conBusId',
        component: EmployeeItemComponent
    },

    {
        path: 'employeeimport',
        component: ImportComponent
    },

    {
        path: 'scanner',
        component: ScannerComponent
    }
];

@NgModule({
    declarations: [
        ImportComponent,
        EmployeeListComponent,
        EmployeeItemComponent,
        EmployeeItemDetailsComponent,
        PayrollEmployeeContractCardComponent,
        PayrollEmployeeContractModalComponent,
        PayrollEmployeeTaxItemComponent,
        PayrollEmployeeOpeningItemComponent,
        ScannerComponent,
        QrCodeGeneratorComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        CoreModule,
        SharedModule,
        ContactModule,
        StorageModule,
        QRCodeModule,
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.production
        }),
        ZXingScannerModule
    ],
    exports: [ScannerComponent]
})
export class PayrollModule {}
