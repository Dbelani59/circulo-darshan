import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from '../../../core/login/services/user-session.service';
import { BehaviorSubject } from 'rxjs';
import { ContactListResolver } from '../../../../obsolete/resolver/contact.resolver';

@Injectable({
    providedIn: 'root'
})
export class PaySysService {
    constructor(
        private http: HttpClient,
        private userSessionService: UserSessionService
    ) {
        this.LoadEthnicitys();
        this.loadNationalitys();
    }

    private _ethnicityDataStore;
    private _ehnicityBehavourSubject = new BehaviorSubject<[]>([]);

    private _nationalAlityDataStore;
    private _nationalityBehavourSubject = new BehaviorSubject<[]>([]);

    private _genderDataStore = [
        { value: 'M', label: 'Male' },
        { value: 'F', label: 'Female' },
        { value: 'O', label: 'Other' }
    ];
    private _maritalStatusDataStore = [
        { value: 'S', label: 'Single' },
        { value: 'M', label: 'Married' },
        { value: 'D', label: 'Divorced' },
        { value: 'W', label: 'Widowed' },
        { value: 'C', label: 'Civil Partnership' }
    ];

    get ethnicitys() {
        return this._ehnicityBehavourSubject.asObservable();
    }

    get nationality() {
        return this._nationalityBehavourSubject.asObservable();
    }

    get genders() {
        return this._genderDataStore;
    }

    get maritalStatus() {
        return this._maritalStatusDataStore;
    }

    private httpLoadEhnicitys() {
        return this.http.get(
            this.userSessionService.BaseAPIURL + '/pay/sys/eth'
        );
    }

    private httpLoadNationalitys() {
        return this.http.get(
            this.userSessionService.BaseAPIURL + '/pay/sys/nat'
        );
    }

    private LoadEthnicitys() {
        this.httpLoadEhnicitys().subscribe((response) => {
            this._ethnicityDataStore = response;
            this._ehnicityBehavourSubject.next(this._ethnicityDataStore);
        });
    }

    private loadNationalitys() {
        this.httpLoadNationalitys().subscribe((response) => {
            this._nationalAlityDataStore = response;
            this._nationalityBehavourSubject.next(this._nationalAlityDataStore);
        });
    }
}
