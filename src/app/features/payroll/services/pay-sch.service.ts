import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from '../../../core/login/services/user-session.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PaySchService {
    constructor(
        private http: HttpClient,
        private userSessionService: UserSessionService
    ) {
        this.LoadPayrollCycles();
    }

    private _payrollCyclesDataStore;
    private _payrollCyclesBehaviourSubject = new BehaviorSubject<[]>([]);

    get payrollSchedules() {
        return this._payrollCyclesBehaviourSubject.asObservable();
    }

    private httpLoadPayrollCycles() {
        return this.http.get(this.userSessionService.BaseAPIURL + '/pay/sch');
    }

    private LoadPayrollCycles() {
        this.httpLoadPayrollCycles().subscribe((response) => {
            this._payrollCyclesDataStore = response;
            this._payrollCyclesBehaviourSubject.next(
                this._payrollCyclesDataStore
            );
        });
    }
}
