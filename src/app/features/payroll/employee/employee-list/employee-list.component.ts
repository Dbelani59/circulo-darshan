import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserSessionService } from 'app/core/login/services/user-session.service';
import { Observable } from 'rxjs';
import { FormState } from 'app/shared/httpService/generic-store.service';
import { EmployeeStoreService } from '../../services/payrollStoreService';

@Component({
    selector: 'app-employee-list',
    templateUrl: './employee-list.component.html',
    styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit, OnDestroy, AfterViewInit {
    displayedColumns = ['cardCode', 'cardName', 'validFrom'];

    columnHeader = {
        // Contact
        cardCode: 'conBus.cardCode.label',
        cardName: 'conBus.cardName.label',
        validFrom: 'conBus.validFrom.label',
        validTo: 'conBus.validTo.label',
        docStatus: 'conBus.docStatus.label'
    };

    dataObservable: Observable<[]>;
    private _dataSubstription;

    constructor(
        public employeeStoreService: EmployeeStoreService,
        private route: ActivatedRoute,
        private router: Router,
        public userSessionService: UserSessionService
    ) {
        this.dataObservable = this.employeeStoreService.dataListObservable;
        this._dataSubstription = this.dataObservable.subscribe((res) => {
            console.log(employeeStoreService.instanceId);
            console.log(res);
        });

        this.refreshData({});
    }

    refreshData(filter: {}) {
        this.employeeStoreService.loadAll(filter);
    }

    rowClick(row) {
        //console.log(row);
        this.employeeStoreService.clearSelection();
        this.employeeStoreService.selectRecords(row);
        this.router.navigate(['/office/payroll/employee/' + row.id]);
    }

    navBarClick(item: any[]) {
        // Clear existing contacts
        this.employeeStoreService.clearSelection();
        // ADD EDIT REFRESH WILL ONLY EMILT ONE ELEMENT
        item.forEach((element) => {
            if (element.event === 'DELETE') {
                //  console.log(element.element);
                this.employeeStoreService.deleteCurrentRecord();
            }
            if (element.event === 'ADD') {
                this.employeeStoreService.setFormSate(FormState.ADD);
                this.router.navigate([
                    '/office/location/location-list/location'
                ]);
            }
            if (element.event === 'EDIT') {
                this.employeeStoreService.selectRecords(element.element);
                this.employeeStoreService.setFormSate(FormState.VIEW);
                this.router.navigate([
                    '/office/location/location-list/location'
                ]);
            }
            if (element.event === 'REFRESH') {
                this.employeeStoreService.loadAll({});
            }
        });
    }

    ngAfterViewInit() {
        this.refreshData({});
    }

    ngOnDestroy() {
        this._dataSubstription.unsubscribe();
    }

    ngOnInit() {}
}
