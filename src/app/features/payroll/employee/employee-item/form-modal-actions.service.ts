import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ThemeService } from 'ng2-charts';

@Injectable({
    providedIn: 'root'
})
export class FormModalActionsService {
    constructor(private dialog: MatDialog) {}

    multiSelect = false;
    ContactMethCardEditComponent;
    dialogReference;
    storeService;

    configure(component, dialogReference, storeService) {
        this.ContactMethCardEditComponent = component;
        this.dialogReference = dialogReference;
        this.storeService = storeService;
    }

    onClickContactMethod(data) {
        if (this.multiSelect) {
            if (data.selected) {
                data.selected = !data.selected;
            } else {
                data.selected = true;
            }
        } else {
            const _ = require('lodash');
            const _data = _.cloneDeep(data); // Set main datastore

            this.dialogReference = this.dialog.open(
                this.ContactMethCardEditComponent,
                {
                    data: {
                        formState: this.storeService._formStateDisabled,
                        formData: _data
                    }
                }
            );

            this.dialogReference.afterClosed().subscribe((conBusCon) => {
                if (conBusCon) {
                    const c = this.storeService.currentRecord.conBusCons;
                    for (let index = 0; index < c.length; index++) {
                        const element = c[index];
                        if (element.id === conBusCon.id) {
                            c[index] = conBusCon;
                        }
                    }

                    console.log(this.storeService.currentRecord.conBusCons);
                    // data = conBusCon;
                }
            });
        }
    }

    openContactAddModal() {
        this.dialogReference = this.dialog.open(
            this.ContactMethCardEditComponent,
            {
                data: {
                    formState: this.storeService._formStateDisabled,
                    formData: {}
                }
            }
        );

        this.dialogReference.afterClosed().subscribe((conBusCon) => {
            if (conBusCon) {
                conBusCon.id = this.storeService.getNewUUID();
                this.storeService.currentRecord.conBusCons.push(conBusCon);
            }
        });
    }

    toggleMultiSelect() {
        if (this.multiSelect === true) {
            // Need to remove any currently selected
            const c = this.storeService.currentRecord.conBusCons;
            for (let index = 0; index < c.length; index++) {
                c[index].selected = false;
            }
        }
        this.multiSelect = !this.multiSelect;
        console.log('Set mutli to: ' + this.multiSelect);
    }
}
