import { Injectable } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { SglLookupsService } from 'app/shared/lookups/sgl-lookups.service';
import { PaySysService } from 'app/features/payroll/services/pay-sys.service';
import { PaySchService } from 'app/features/payroll/services/pay-sch.service';

@Injectable()
export class EmployeeItemDetailsFormService {
    constructor(
        private translate: TranslateService,
        private sglLookupService: SglLookupsService,
        private paySysService: PaySysService,
        private paySchService: PaySchService
    ) {}

    fields: FormlyFieldConfig[] = [
        {
            key: 'conBusTypPer.payEmp.payNo',
            type: 'input', // input type

            className: 'col-6',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {
                    style: 'width: 200px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'conBusTypPer.payEmp.payNo.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'conBusTypPer.payEmp.payNo.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'conBusTypPer.payEmp.payNo.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
                {
                    key: 'conBusTypPer.payEmp.startDate',
                    type: 'datepicker', // input type
                    className: 'col1',
                    defaultValue: '',
                    templateOptions: {
                        type: 'text',
                        required: true,
                        attributes: {
                            //   style: 'width: 800px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBusTypPer.payEmp.startDate.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBusTypPer.payEmp.startDate.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBusTypPer.payEmp.startDate.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'conBusTypPer.payEmp.tupeDate',
                    type: 'datepicker', // input type
                    className: 'col1',
                    templateOptions: {
                        type: 'text',
                        required: true,
                        attributes: {
                            //       style: 'width: 400px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBusTypPer.payEmp.tupeDate.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBusTypPer.payEmp.tupeDate.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBusTypPer.payEmp.tupeDate.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'conBusTypPer.payEmp.leaveData',
                    type: 'datepicker', // input type
                    className: 'col1',
                    templateOptions: {
                        type: 'text',
                        required: true,
                        attributes: {
                            //       style: 'width: 400px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBusTypPer.payEmp.leaveData.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBusTypPer.payEmp.leaveData.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBusTypPer.payEmp.leaveData.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            key: 'conBusTypPer.payEmp.dateOfBirth',
            type: 'datepicker', // input type
            className: 'col2',
            templateOptions: {
                type: 'text',
                required: true,
                attributes: {
                    //        style: 'width: 400px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'conBusTypPer.payEmp.dateOfBirth.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'conBusTypPer.payEmp.dateOfBirth.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'conBusTypPer.payEmp.dateOfBirth.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypPer.payEmp.gender',
            type: 'select', // input type
            className: 'col2',
            templateOptions: {
                required: true,
                options: this.paySysService.genders,
                attributes: {
                    //         style: 'width: 400px;'
                }
            },

            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'conBusTypPer.payEmp.gender.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'conBusTypPer.payEmp.gender.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'conBusTypPer.payEmp.gender.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypPer.payEmp.maritalStatus',
            type: 'select', // input type
            className: 'col2',
            templateOptions: {
                required: true,
                options: this.paySysService.maritalStatus,
                attributes: {
                    //      style: 'width: 400px;'
                }
            },

            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'conBusTypPer.payEmp.maritalStatus.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'conBusTypPer.payEmp.maritalStatus.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'conBusTypPer.payEmp.maritalStatus.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
                {
                    key: 'conBusTypPer.payEmp.ninumber',
                    type: 'input', // input type

                    className: 'col2',
                    templateOptions: {
                        type: 'text',
                        required: true,
                        attributes: {
                            //      style: 'width: 200px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBusTypPer.payEmp.ninumber.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBusTypPer.payEmp.ninumber.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBusTypPer.payEmp.ninumber.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },

                {
                    key: 'conBusTypPer.payEmp.passportnumber',
                    type: 'input', // input type

                    className: 'col2',
                    templateOptions: {
                        type: 'text',
                        required: true,
                        attributes: {
                            //      style: 'width: 200px;'
                        }
                    },
                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBusTypPer.payEmp.passportnumber.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBusTypPer.payEmp.passportnumber.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBusTypPer.payEmp.passportnumber.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },

        {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
                {
                    key: 'conBusTypPer.payEmp.paySysNatId',
                    type: 'select', // input type
                    className: 'col2',
                    templateOptions: {
                        required: false,
                        options: this.paySysService.nationality,
                        valueProp: 'id',
                        labelProp: 'nationality',
                        attributes: {
                            //      style: 'width: 400px;'
                        }
                    },

                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBusTypPer.payEmp.paySysNatId.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBusTypPer.payEmp.paySysNatId.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBusTypPer.payEmp.paySysNatId.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                },
                {
                    key: 'conBusTypPer.payEmp.paySysEthId',
                    type: 'select', // input type
                    className: 'col2',
                    templateOptions: {
                        required: false,
                        options: this.paySysService.ethnicitys,
                        // options: this.currencys,
                        valueProp: 'id',
                        labelProp: 'ethnicity',
                        attributes: {
                            //  style: 'width: 400px;'
                        }
                    },

                    expressionProperties: {
                        'templateOptions.label': this.translate.stream(
                            'conBusTypPer.payEmp.paySysEthId.label'
                        ),
                        'templateOptions.placeholder': this.translate.stream(
                            'conBusTypPer.payEmp.paySysEthId.placeholder'
                        ),
                        'templateOptions.description': this.translate.stream(
                            'conBusTypPer.payEmp.paySysEthId.description'
                        ),
                        'templateOptions.disabled': 'formState.disabled'
                    }
                }
            ]
        },
        {
            key: 'conBusTypPer.payEmp.paySchId',
            type: 'select', // input type
            templateOptions: {
                required: false,
                options: this.paySchService.payrollSchedules,
                valueProp: 'id',
                labelProp: 'scheduleName',
                attributes: {
                    style: 'width: 400px;'
                }
            },

            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'conBusTypPer.payEmp.paySchId.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'conBusTypPer.payEmp.paySchId.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'conBusTypPer.payEmp.paySchId.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        },
        {
            key: 'conBusTypPer.payEmp.passwordProtectPayslip',
            type: 'checkbox', // input type
            templateOptions: {
                required: false,
                attributes: {
                    style: 'width: 200px;'
                }
            },
            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'conBusTypPer.payEmp.passwordProtectPayslip.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'conBusTypPer.payEmp.passwordProtectPayslip.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'conBusTypPer.payEmp.passwordProtectPayslip.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        }
    ];
}
