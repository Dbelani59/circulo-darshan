import { Component, OnInit } from '@angular/core';
import { EmployeeStoreService } from 'app/features/payroll/services/payrollStoreService';
import { EmployeeItemDetailsFormService } from './employee-item-details-form.service';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-employee-item-details',
    templateUrl: './employee-item-details.component.html',
    styleUrls: ['./employee-item-details.component.scss'],
    providers: [EmployeeItemDetailsFormService]
})
export class EmployeeItemDetailsComponent implements OnInit {
    constructor(
        public employeeStoreService: EmployeeStoreService,
        private formFields: EmployeeItemDetailsFormService
    ) {
        this.employeeStoreService.formStateDisabledObservable.subscribe(
            (res) => {
                console.log(res);
                if (res === true) {
                    this.options.formState.disabled = true;
                } else {
                    this.options.formState.disabled = false;
                }
            }
        );
    }

    options: FormlyFormOptions = {
        formState: {
            disabled: true
        }
    };

    form = new FormGroup({});
    fields: FormlyFieldConfig[] = this.formFields.fields;

    ngOnInit() {}
}
