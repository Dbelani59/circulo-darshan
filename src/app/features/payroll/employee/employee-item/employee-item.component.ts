import { Component, OnInit } from '@angular/core';
import { EmployeeStoreService } from '../../services/payrollStoreService';
import { MatTabChangeEvent } from '@angular/material';
import { ContactBankService } from '../../../contact/contact-bank/contact-bank.service';
import { ContactContService } from '../../../contact/contact-cont/contact-cont.service';
import {
    ContactMethModalService,
    ContactRtwModalService,
    ContactBankItemModal_Service,
    ContactContractItemModel_Service
} from 'app/features/contact/services/contactModalService';
import { ActivatedRoute, Router } from '@angular/router';
import { GetDetailsService } from '../../qr-code-generator/get-details.service';
import { ContactDataService } from '../../../contact/services/contact-data.service';

@Component({
    selector: 'app-employee-item',
    templateUrl: './employee-item.component.html',
    styleUrls: [
        './employee-item.component.scss',
        '../../../../styles/_module-cards.scss'
    ]
})
export class EmployeeItemComponent implements OnInit {
    isLoading = false;
    showSpinner = false;
    tabIndex = 0;
    myDoc;
    contractDetails;
    id;
    all_data_qr;

    constructor(
        public employeeStoreService: EmployeeStoreService,
        public contactMethModalService: ContactMethModalService,
        public contactRtwModalService: ContactRtwModalService,
        public contactBankService: ContactBankService,
        public contactBankItemModalService: ContactBankItemModal_Service,
        public contactContItemModalService: ContactContractItemModel_Service,
        public contService: ContactContService,
        private route: ActivatedRoute,
        private router: Router,
        private qr_service: GetDetailsService,
        private contactData: ContactDataService
    ) {
        this.contactMethModalService.configure(
            this.employeeStoreService,
            'conBusCons'
        );

        this.contactRtwModalService.configure(
            this.employeeStoreService,
            'conBusRtws'
        );

        this.contactBankItemModalService.configure(
            this.employeeStoreService,
            'conBusBans'
        );
        this.contactContItemModalService.configure(
            this.employeeStoreService,
            'payEmpCons'
        );

        this.route.paramMap.subscribe((params) => {
            this.id = params.get('conBusId');
        });
        this.myDoc = this.contactBankService.getDetails(this.id);
        //console.log('myDoc');
        // console.log(this.contactData.getDetails());
        this.contractDetails = this.contService.getContractDetails(this.id);

        this.all_data_qr = this.qr_service.getDetails(this.id);
    }

    onClick_address() {
        this.contactMethModalService.openModalAdd();
    }

    onClick_cont() {
        this.contactContItemModalService.openModalAdd();
    }

    onClick_rtw() {
        this.contactRtwModalService.openModalAdd();
    }

    onClick_bank_details() {
        this.contactBankItemModalService.openModalAdd();
    }

    onClick_scanner() {
        this.router.navigate(['/office/payroll/scanner']);
    }

    // onClick_dbs()
    // {
    //     this.contactDbsModalService.openModalAdd();
    // }

    ngOnInit() {}

    itemClick(itemClick) {
        const serviceResult = this.employeeStoreService.itemClick(itemClick);

        if (serviceResult === false) {
            // If action not handled by service
            if (this.tabIndex === 2) {
                if (itemClick === 'ITEM_ADD') {
                    this.contactMethModalService.openModalAdd();
                }
            }

            if (this.tabIndex === 3) {
                if (itemClick === 'ITEM_ADD') {
                    this.contactRtwModalService.openModalAdd();
                }
            }
        }
    }

    tabChanged(tabChangeEvent: MatTabChangeEvent): void {
        this.tabIndex = tabChangeEvent.index;
    }
}
