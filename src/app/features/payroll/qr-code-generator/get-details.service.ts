import { Injectable } from '@angular/core';
import { GenericHttpService } from 'app/shared/httpService/generic-http.service';

@Injectable({
    providedIn: 'root'
})
export class GetDetailsService {
    myDetails = [];
    url;
    constructor(private genericHttpService: GenericHttpService) {}

    getDetails(id) {
        this.myDetails = [];
        this.url =
            'https://api.dev.cmtech.app/api/v1/3455015a-9163-465f-a681-0e9d7c7e5b8e/payroll/employees';

        this.genericHttpService.get(this.url, id).subscribe(
            (dataResp: any) => {
                let notFound = true;
                const localData: any = dataResp.data[0];
                Object.entries(localData).forEach(([key, value]) =>
                    this.myDetails.push(`${key}: ${value}`)
                );
            },
            (error) => console.log('Error: ' + error)
        );

        return this.myDetails;
    }
}
