import { Component } from '@angular/core';

@Component({
    selector: 'scanner',
    templateUrl: './scanner.component.html',
    styleUrls: ['./scanner.component.scss']
})
export class ScannerComponent {
    qrResultString: string;

    clearResult(): void {
        this.qrResultString = null;
    }

    onCodeResult(resultString: string) {
        this.qrResultString = resultString;
        console.log(this.qrResultString);
    }
}
