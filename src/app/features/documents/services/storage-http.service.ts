import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserSessionService } from '../../../core/login/services/user-session.service';

@Injectable({
    providedIn: 'root'
})
export class StorageHttpService {
    constructor(
        private http: HttpClient,
        private userSession: UserSessionService
    ) {}

    getLibrary(docType) {
        const headerDict = {
            request: JSON.stringify({
                filter: {
                    docType: docType
                }
            })
        };

        const requestOptions = {
            headers: new HttpHeaders(headerDict)
        };

        return this.http.get(
            this.userSession.BaseAPIURL + '/documents/library',
            requestOptions
        );
    }

    addDocumentToConBus(conBusId, docStoId, docLibId) {
        const body = {
            conBusId: conBusId,
            docStoId: docStoId,
            docLibId: docLibId
        };

        return this.http.post(
            this.userSession.BaseAPIURL +
                '/contacts/' +
                conBusId +
                '/documents',
            body
        );
    }
}
