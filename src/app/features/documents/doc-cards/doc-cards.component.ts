import { Component, OnInit, Input } from '@angular/core';
import { AnyRecordWithTtl } from 'dns';
import { Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';
import { GenericStoreConstructor } from '../../../shared/httpService/generic-store-constructor.interface';
import { UserSessionService } from '../../../core/login/services/user-session.service';

import { MatDialog } from '@angular/material';
import { DocBulkUploadComponent } from '../doc-bulk-upload/doc-bulk-upload.component';
import { DocViewComponent } from '../../../shared/doc-view/doc-view.component';
import { DocumentContactStoreService } from '../services/documentStoreService';

@Component({
    selector: 'app-doc-cards',
    templateUrl: './doc-cards.component.html',
    styleUrls: [
        './doc-cards.component.scss',
        '../../../styles/_module-cards.scss'
    ],
    providers: []
})
export class DocCardsComponent implements OnInit {
    _showContactMeth = false;
    _showAdd = false;
    // _dataObservable: any;
    docStoId = '';
    @Input() conBusId;
    // TODO need to add the ability to select the module
    // location
    // customer
    // Marketing Document

    constructor(
        private router: Router,
        private userSessionService: UserSessionService,
        public documentStoreService: DocumentContactStoreService,
        private dialog: MatDialog
    ) {}

    ngOnInit() {
        this.refreshData({});
    }
    refreshData(arg0: {}) {
        this.documentStoreService.loadAll({
            filter: { conBusId: this.conBusId },
            urlParams: { conBusId: this.conBusId }
        });
    }

    onDocClick(value: any) {
        if (value) {
            this.documentStoreService.selectRecords(value);
            this.docStoId = this.documentStoreService.currentRecord.docStoId;

            let dialogRef = this.dialog.open(DocViewComponent, {
                //  width: '75%',
                height: '75%',
                data: { docStoId: this.docStoId }
            });

            dialogRef.afterClosed().subscribe((x) => {
                dialogRef = null;
            });
        }
    }

    onDocDelete(value: any) {
        this.documentStoreService.setCurrentRecordById(value.id);
        this.documentStoreService.deleteCurrentRecord();
    }

    itemClick(clicked) {
        this._showContactMeth = false;
    }

    onAddClick(): void {
        const dialogRef = this.dialog.open(DocBulkUploadComponent, {
            //   width: '250px',
            data: { sourceId: this.conBusId }
        });
    }
}
