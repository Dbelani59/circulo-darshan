import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocCardsComponent } from './doc-cards/doc-cards.component';
import { DocCardComponent } from './doc-card/doc-card.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared';

import { DocBulkUploadComponent } from './doc-bulk-upload/doc-bulk-upload.component';

import { PdfViewerModule } from 'ng2-pdf-viewer';
import { DocViewComponent } from '../../shared/doc-view/doc-view.component';
import { DocumentContactStoreService } from './services/documentStoreService';

// import { PdfViewComponent } from '../shared/pdf-view/pdf-view.component';

export const storageRoutes = [
    // {
    //     path: '',
    //     component: DocsviewComponent
    // },
    // {
    //     path: 'docs',
    //     component: DocsviewComponent
    // },
    // {
    //     path: '/doc/:docStoId',
    //     component: DocsviewComponent
    // }
];

@NgModule({
    declarations: [DocCardsComponent, DocCardComponent, DocBulkUploadComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(storageRoutes),
        SharedModule,
        PdfViewerModule
    ],
    exports: [DocCardsComponent],
    providers: [DocumentContactStoreService],
    entryComponents: [DocBulkUploadComponent, DocViewComponent] // Note re-matDialog https://github.com/angular/components/issues/1491
})
export class StorageModule {}
