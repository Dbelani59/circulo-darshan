import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from 'app/core/login/services/user-session.service';

@Component({
    selector: 'app-doc-card',
    templateUrl: './doc-card.component.html',
    styleUrls: ['./doc-card.component.scss', '../../../styles/_module-cards.scss']
})
export class DocCardComponent implements OnInit {
    @Input() conBusDoc;
    @Output() conBusDocOut: EventEmitter<string> = new EventEmitter<string>();
    @Output() conBusDocDelete: EventEmitter<string> = new EventEmitter<string>();
    _showContactMeth = false;
    _conBusDoc: any;

    constructor(private http: HttpClient, private userSessionService: UserSessionService) {
        this._conBusDoc = this.conBusDoc;
    }

    ngOnInit() {}

    onDeleteClick() {
        // this.http
        //     .delete(this.userSessionService.BaseAPIURL + '/locations/' + this.conBusDoc.conBusId + '/documents/' + this.conBusDoc.id)
        //     .subscribe((res) => {
        //         console.log(res);
        //     });
        this.conBusDocDelete.emit(this.conBusDoc);
        console.log(this.conBusDoc);
    }

    onDocClickA() {
        this.conBusDocOut.emit(this.conBusDoc);
    }
}
