import { Component, OnInit, Input, Output, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { StorageHttpService } from '../services/storage-http.service';
import { GenericStoreService } from '../../../shared/httpService/generic-store.service';
import { BehaviorSubject } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EventEmitter } from 'events';
import { DocumentContactStoreService } from '../services/documentStoreService';

@Component({
    selector: 'app-doc-bulk-upload',
    templateUrl: './doc-bulk-upload.component.html',
    styleUrls: [
        './doc-bulk-upload.component.scss',
        '../../../styles/_module-cards.scss'
    ]
})
export class DocBulkUploadComponent implements OnInit {
    constructor(
        private translate: TranslateService,
        private storageHttpService: StorageHttpService,
        private documentStoreService: DocumentContactStoreService,
        public dialogRef: MatDialogRef<DocBulkUploadComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.storageHttpService
            .getLibrary(this.docType)
            .subscribe((res: any) => {
                this._contactPurposeSubject.next(res.data);
            });
    }

    @Input() docType = 'CL';
    @Output() uploadComplete: boolean;

    options: FormlyFormOptions = {
        formState: {
            disabled: false
        }
    };

    private _contactPurposeSubject = new BehaviorSubject<[]>([]);
    get contactPurposes() {
        return this._contactPurposeSubject.asObservable();
    }

    _library: [];
    form = new FormGroup({});

    fields: FormlyFieldConfig[] = [
        {
            key: 'name',
            type: 'select', // input type
            templateOptions: {
                required: false,
                options: this.contactPurposes,
                valueProp: 'id',
                labelProp: 'name',
                attributes: {
                    style: 'width: 90%; margin-left:5%'
                }
            },

            expressionProperties: {
                'templateOptions.label': this.translate.stream(
                    'docLib.name.label'
                ),
                'templateOptions.placeholder': this.translate.stream(
                    'docLib.name.placeholder'
                ),
                'templateOptions.description': this.translate.stream(
                    'docLib.name.description'
                ),
                'templateOptions.disabled': 'formState.disabled'
            }
        }
    ];

    ngOnInit() {}

    onfileListUploaded(eventData) {
        //   console.log('onfileListUploaded');
        // console.log(this.docType);

        // Add to conBusDoc
        if (this.docType === 'CL') {
            for (const file of eventData) {
                this.storageHttpService
                    .addDocumentToConBus(
                        this.data.sourceId,
                        file.id,
                        this.form.value.name
                    )
                    .subscribe((res: any) => {
                        this.documentStoreService.noUpdateAddToDataStore(
                            res.data[0]
                            //     fieldMatch: 'name',
                            //     addRowName: 'conBusDocs'
                            // }
                        );
                    });
            }
            //       this.options.resetModel();
        }

        this.dialogRef.close();
    }
}
