import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChartsModule as ng2ChartsModule } from 'ng2-charts';

import { SharedModule } from '../shared';

import { MatTableModule } from '@angular/material/table';

import { DashboardPageComponent } from './dashboard.component';
import { DashboardResolver } from './dashboard.resolver';
import { DashComponent } from './dash/dash.component';

export const DashboardRoutes = [
    {
        path: '',
        component: DashComponent
    },

    {
        path: 'Dashboard',
        component: DashboardPageComponent,
        resolve: {
            data: DashboardResolver
        }
    }
];

@NgModule({
    declarations: [DashboardPageComponent, DashComponent],
    imports: [RouterModule.forChild(DashboardRoutes), CommonModule, ng2ChartsModule, SharedModule, MatTableModule],
    providers: [DashboardResolver],
    exports: [DashComponent]
})
export class DashboardModule {}
