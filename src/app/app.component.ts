import {
    Component,
    ViewChild,
    ViewEncapsulation,
    PLATFORM_ID,
    Inject,
    AfterViewInit,
    ChangeDetectorRef
} from '@angular/core';
import {
    MessagesMenuService,
    NotificationsMenuService,
    SideMenuService
} from './core';
import { isPlatformBrowser } from '@angular/common';

import { UserSessionService } from './core/login/services/user-session.service';
import { UserSessionModel } from './core/login/services/user-session.model';

import { JwtHelperService } from '@auth0/angular-jwt';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

import { fadeAnimation } from './core/animations/fade.animation';
import { TrackingService } from './core/login/services/tracking.service';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
// import { UpdateService } from './core/update.service';
import { ConstantPool } from '@angular/compiler';
import { VersionCheckServiceService } from './core/login/services/version-check-service.service';
import { environment } from 'environments/environment';

const jwtHelper = new JwtHelperService();

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./styles/app.scss', './styles/tab.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [fadeAnimation]
})

// export class AppComponent implements AfterContentInit {
export class AppComponent implements AfterViewInit {
    @ViewChild('drawerContainer') drawerContainer;
    @ViewChild('sideMenu') sideMenu;
    @ViewChild('sideNotifications') sideNotifications;

    notifications = [];
    messages = [];
    open_menu = true;
    userSession = new UserSessionModel();

    public loading = true;

    // theme$: Observable<string>;

    constructor(
        private sideMenuService: SideMenuService,
        private notificationsMenuService: NotificationsMenuService,
        private messagesMenuService: MessagesMenuService,
        public userSessionService: UserSessionService,
        private translate: TranslateService,
        private changeDetector: ChangeDetectorRef,
        public router: Router,
        private trackingService: TrackingService, // Tracking Service
        //       private update: UpdateService, // Update Service
        private versionCheckService: VersionCheckServiceService, //  Version Check Service
        @Inject(PLATFORM_ID) private platformId: Object
    ) {
        this.loading = true;

        versionCheckService.initVersionCheck(environment.versionCheckURL);

        notificationsMenuService
            .getNotifications()
            .then((notifications: any) => {
                this.notifications = notifications;
            });
        messagesMenuService.getMessages().then((messages: any) => {
            this.messages = messages;
        });

        // Add Languages
        translate.addLangs(['en-GB', 'fr']);
        translate.setDefaultLang('en-GB');
        this.translate.use('en-GB');

        this.userSessionService.userSessionEmit.subscribe(
            (res: any) => {
                this.userSession = res;
                this.loading = false;
                this.changeDetector.detectChanges(); // Note this cannot be called before the int
                this.updateViewChild();
                const app = localStorage.getItem('appType');
                if (app) {
                    if (app === 'TAB') {
                        this.router.navigate(['/tab']);
                    } else {
                        this.router.navigate(['/office']);
                    }
                } else {
                    this.router.navigate(['/office']);
                }
            },
            (error) => {
                this.userSession = error;
                this.loading = false;
                this.changeDetector.detectChanges();
                this.updateViewChild();
                console.log(error);
            }
        );
    }

    theme$ = new BehaviorSubject('mat-def-theme');

    updateViewChild() {
        this.sideMenuService.sidenav = this.sideMenu;
        this.sideMenuService.drawerContainer = this.drawerContainer;
        this.notificationsMenuService.sidenav = this.sideNotifications;
        if (isPlatformBrowser(this.platformId)) {
            this.open_menu = true;
        }
        //    console.log('core services loaded...');
    }

    ngAfterViewInit(): void {
        this.userSessionService.setApp();
        //   this.router.navigate(['tab']);
    }

    public getRouterOutletState(outlet) {
        return outlet.isActivated ? outlet.activatedRoute : '';
    }
}
