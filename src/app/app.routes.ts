import { Routes } from '@angular/router';
import { Error404PageComponent, Error404PageResolver } from './core';
import { DashComponent } from './dashboard/dash/dash.component';
import { DashboardPageComponent } from './dashboard';

import { AppselectComponent } from './core/appselect/appselect.component';

export const rootRoutes: Routes = [
    { path: '', redirectTo: '/office', pathMatch: 'full' },
    {
        path: 'office',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
    },
    {
        path: 'customer',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
    },
    {
        path: 'employee',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
    },
    {
        path: 'supplier',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
    },

    // { path: 'forms', loadChildren: './forms/forms.module#FormsModule' },
    // { path: ''            , loadChildren: './accounting/accounting.module#AccountingModule' },
    {
        path: 'office/contact',
        loadChildren: './features/contact/contact.module#ContactModule'
    },
    {
        path: 'office/accounting',
        loadChildren: './features/accounting/accounting.module#AccountingModule'
    },
    {
        path: 'office/tenant',
        loadChildren: './features/comp/comp.module#CompModule'
    },
    {
        path: 'office/timeatt',
        loadChildren: './features/timeatt/timeatt.module#TimeattModule'
    },
    {
        path: 'office/communications',
        loadChildren:
            './features/communications/communications.module#CommunicationsModule'
    },
    {
        path: 'office/banking',
        loadChildren: './features/banking/banking.module#BankingModule'
    },
    {
        path: 'office/payroll',
        loadChildren: './features/payroll/payroll.module#PayrollModule'
    },
    {
        path: 'office/devices',
        loadChildren: './features/device/device.module#DeviceModule'
    },
    {
        path: 'office/location',
        loadChildren: './features/location/location.module#LocationModule'
    },

    {
        path: 'office/storage',
        loadChildren: './features/documents/storage.module#StorageModule'
    },
    { path: 'tab', loadChildren: './tab/tab.module#TabModule' },
    {
        path: '404',
        redirectTo: '/office'
        // component: DashComponent
        // resolve: { data: Error404PageResolver }
    },
    {
        // There's a bug that's preventing wild card routes to be lazy loaded (see: https://github.com/angular/angular/issues/13848)
        // That's why the Error page should be eagerly loaded
        //   path: '**',
        //  component: Error404PageComponent,
        //   resolve: { data: Error404PageResolver }

        path: '**',
        redirectTo: '/office'
        // component: DashComponent
        // resolve: { data: Error404PageResolver }
    }
];
