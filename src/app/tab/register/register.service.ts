import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DeviceSessionService } from '../device-session.service';
import { UserSessionService } from 'app/core/login/services/user-session.service';

@Injectable({
    providedIn: 'root'
})
export class RegisterService {
    constructor(
        private http: HttpClient,
        private deviceSessionService: DeviceSessionService,
        private userSessionService: UserSessionService
    ) {}

    registerDevice(body) {
        // console.log(body);

        return this.http.post(
            this.userSessionService.BaseAPIURLWithoutSysCompId + '/device',
            body
        );
    }
}
