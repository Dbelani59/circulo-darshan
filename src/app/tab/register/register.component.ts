import { Component, OnInit, Inject } from '@angular/core';
import { RegisterService } from './register.service';
import { TrackingService } from '../../core/login/services/tracking.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatDialog, ThemePalette } from '@angular/material';
import { AlertComponent } from '../../shared/alert/alert.component';
import { MatIconRegistry } from '@angular/material';
import { Router } from '@angular/router';
import { UserSessionService } from 'app/core/login/services/user-session.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    formStacked: FormGroup;

    constructor(
        fb: FormBuilder,
        private regiserService: RegisterService,
        private trackingService: TrackingService,
        public userSessionService: UserSessionService,
        public dialog: MatDialog,
        private router: Router
    ) {}

    ngOnInit() {}
}
