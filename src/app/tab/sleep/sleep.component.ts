import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserSessionService } from 'app/core/login/services/user-session.service';

@Component({
    selector: 'app-sleep',
    templateUrl: './sleep.component.html',
    styleUrls: ['../styles/tabStyles.scss']
})
export class SleepComponent implements OnInit {
    constructor(
        private router: Router,
        private userSessionService: UserSessionService
    ) {}

    ngOnInit() {}

    onClick(itemClicked: String) {
        // console.log(itemClicked);

        if (itemClicked === 'LIBRARY') {
            this.router.navigate(['tab/library']);
        } else {
            this.router.navigate(['tab/login-method']);
        }
    }
}
