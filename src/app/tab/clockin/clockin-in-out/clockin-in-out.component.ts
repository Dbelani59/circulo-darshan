import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-clockin-in-out',
    templateUrl: './clockin-in-out.component.html',
    styleUrls: ['../../styles/tabStyles.scss']
})
export class ClockinInOutComponent implements OnInit {
    constructor(private router: Router) {}

    ngOnInit() {}

    onClick(itemClicked: string) {
        this.router.navigate(['./tab']);
    }
}
