import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-clockin',
    templateUrl: './clockin.component.html',
    styleUrls: ['./clockin.component.scss', '../styles/tabStyles.scss']
})
export class ClockinComponent implements OnInit {
    constructor(private router: Router) {}

    payNo = '';

    ngOnInit() {}

    onClick(itemClicked: String) {
        // console.log(itemClicked);
        if (itemClicked !== 'Go') {
            this.payNo = this.payNo + itemClicked.toString();
        } else {
            this.router.navigate(['./tab/face-capture-pin']);
        }
    }

    onClickClose() {
        this.router.navigate(['./tab']);
    }
}
