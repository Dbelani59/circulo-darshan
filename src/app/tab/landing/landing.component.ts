import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  constructor(
    private router: Router,

  ) { }

  ngOnInit() {
  }


  onClick(menuClicked: string) {



    if (menuClicked === 'DOCUMENTS') {
    this.router.navigate(['tab/documents']);
  //  console.log(menuClicked);
    }

    if (menuClicked === 'CLOCKING') {
      this.router.navigate(['tab/clocking']);
   //  console.log(menuClicked);
      }


  }


}
