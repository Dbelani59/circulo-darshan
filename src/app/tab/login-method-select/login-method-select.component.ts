import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login-method-select',
    templateUrl: './login-method-select.component.html',
    styleUrls: ['../styles/tabStyles.scss']
})
export class LoginMethodSelectComponent implements OnInit {
    constructor(private router: Router) {}

    ngOnInit() {}

    onClick(itemClicked: String) {
        if (itemClicked === 'USERNAME') {
            this.router.navigate(['/tab/clockin-pin']);
        }
    }
}
