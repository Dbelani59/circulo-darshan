import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { DocumentsService } from '../documents.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserSessionService } from 'app/core/login/services/user-session.service';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-documents',
    templateUrl: './documents.component.html',
    styleUrls: ['./documents.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class DocumentsComponent implements OnInit {
    docStoId = '';
    docUrlPdf = '';
    docUrlGoogle = '';
    pdfDoc = true;

    constructor(
        private documentService: DocumentsService,
        private _route: ActivatedRoute,
        private userSessionService: UserSessionService,
        private http: HttpClient,
        private router: Router
    ) {
        this.docStoId = _route.snapshot.paramMap.get('docStoId');
        this.getURL(this.docStoId);
    }

    ngOnInit() {
        //  this.docStoId = this._route.snapshot.paramMap.get('docStoId');
        // this.getURL(this.docStoId);
        // console.log(this.docStoId);
    }

    onClick(itemCl) {}

    onClickClose() {
        this.router.navigate(['./tab/library/' + this._route.snapshot.paramMap.get('groupName') + '/items']);
    }

    getURL(id) {
        let apiUrl = '';
        console.log(this.docStoId);

        apiUrl = this.userSessionService.BaseAPIURL + '/documents/documents/' + id;

        return this.http.get(apiUrl).subscribe(
            (response) => {
                let data;
                data = response;

                if (data.data[0].contentType === 'application/pdf') {
                    this.pdfDoc = true;
                    this.docUrlPdf = data.data[0].url;
                } else {
                    this.pdfDoc = false;
                    this.docUrlGoogle = data.data[0].url;
                }
                return data;
            },
            (error) => console.log(error)
        );
    }
}
