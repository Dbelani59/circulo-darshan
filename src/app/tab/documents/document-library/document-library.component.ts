import { Component, OnInit } from '@angular/core';
import { DocumentsService } from '../documents.service';
import { Router } from '@angular/router';

import { UserSessionService } from 'app/core/login/services/user-session.service';
import { GenericStoreConstructor } from 'app/shared/httpService/generic-store-constructor.interface';
import { DocumentContactStoreService } from 'app/features/documents/services/documentStoreService';
import { TabDocumentStoreService } from 'app/tab/services/tabStoreService';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-document-library',
    templateUrl: './document-library.component.html',
    styleUrls: ['./document-library.component.scss']
})
export class DocumentLibraryComponent implements OnInit {
    dataObservable: Observable<[]>;
    private _dataSubstription;

    library;
    docs;

    constructor(
        private documentService: DocumentsService,
        private router: Router,

        public tabDocumentStoreService: TabDocumentStoreService
    ) {
        this.dataObservable = this.tabDocumentStoreService.dataListObservable;
        this._dataSubstription = this.dataObservable.subscribe((res) => {
            console.log(res);
        });

        this.refreshData({});
    }

    refreshData(filter: {}) {
        this.tabDocumentStoreService.loadAll(filter);
    }

    ngOnInit() {
        // this.documentService.getContactDocuments().subscribe((res) => {
        //     console.log(res);
        //     this.library = res;
        // });
        // const url =
        //     this.userSessionService.BaseAPIURL +
        //     '/contacts/' +
        //     this.userSessionService.user.conBusId +
        //     '/documents';
        // this.documentContactStoreService.loadAll({});
    }

    // onClickLibrary(item) {
    //     //  console.log('dsfsdf');
    //     //  console.log('dsfsdf');
    //     if (this.viewLibrary === true) {
    //         this.library = item.conBusDocs;
    //         //    console.log(item);
    //         this.viewLibrary = !this.viewLibrary;
    //     } else {
    //         this.router.navigate(['/tab/library/doc/' + item.docSto.id]);
    //     }
    // }

    itemClick() {
        console.log('Not implemented');
    }

    onClickClose() {
        this.router.navigate(['./tab']);
    }

    onClickLibrary(value: any) {
        this.router.navigate(['tab/library/' + value.name + '/items']);
    }
}
