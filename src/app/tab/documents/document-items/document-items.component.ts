import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { DocViewComponent } from 'app/shared/doc-view/doc-view.component';
import { MatDialog } from '@angular/material';
import { DocumentContactStoreService } from 'app/features/documents/services/documentStoreService';
import { TabDocumentStoreService } from 'app/tab/services/tabStoreService';

@Component({
    selector: 'app-document-items',
    templateUrl: './document-items.component.html',
    styleUrls: ['./document-items.component.scss']
})
export class DocumentItemsComponent implements OnInit {
    documentItems: any[] = [];
    groupName = '';
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private tabDocumentStoreService: TabDocumentStoreService,
        private dialog: MatDialog
    ) {
        activatedRoute.params.subscribe((params) => {
            this.groupName = params['groupName'];
            const documents = tabDocumentStoreService._groupRecordsGetValues(
                this.groupName
            );
            console.log(documents);
            documents.forEach((element) => {
                this.documentItems.push(element);
            });
        });
    }

    ngOnInit() {
        console.log(this.tabDocumentStoreService._groupedRecords);
    }

    // onDocClick(value: any) {
    //     this.router.navigate(['/library/' + value.name + '/items']);
    // }

    onClickClose() {
        this.router.navigate(['./tab/library']);
    }

    onDocClick(value: any) {
        if (value) {
            // this.documentStoreService.selectRecords(value);
            // const docStoId = this.documentStoreService.currentRecord.docStoId;

            // let dialogRef = this.dialog.open(DocViewComponent, {
            //     width: '95%',
            //     height: '95%',
            //     data: { docStoId: docStoId }
            // });

            // dialogRef.afterClosed().subscribe((x) => {
            //     dialogRef = null;
            // });

            this.router.navigate([
                'tab/library/' + this.groupName + '/items/' + value.docStoId
            ]);
        }
    }
}
