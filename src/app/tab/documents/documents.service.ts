import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from '../../core/login/services/user-session.service';

@Injectable({
    providedIn: 'root'
})
export class DocumentsService {
    constructor(
        private http: HttpClient,
        private userSessionService: UserSessionService
    ) {}

    getContactDocuments() {
        // The conBusId of a user associated to a device is the location
        return this.http.get(
            this.userSessionService.BaseAPIURL +
                '/contacts/' +
                this.userSessionService.user.conBusId +
                '/documents'
        );
    }
}
