// https://stackoverflow.com/questions/44129817/typescript-generic-service/44130739

import { GenericStoreService } from '../../shared/httpService/generic-store.service';
import { HttpClient } from '@angular/common/http';
import { UserSessionService } from '../../core/login/services/user-session.service';
import { Injectable } from '@angular/core';
import { GenericHttpService } from '../../shared/httpService/generic-http.service';
import { MatSnackBar } from '@angular/material';
import { GenericStoreConstructor } from 'app/shared/httpService/generic-store-constructor.interface';

@Injectable({
    providedIn: 'root'
})
export class TabDocumentStoreService extends GenericStoreService {
    constructor(
        http: GenericHttpService,
        matC: MatSnackBar,
        userSessionService: UserSessionService
    ) {
        const _urls: GenericStoreConstructor = {
            urlWithCompany: true,
            genericUrl:
                '/contacts/' + userSessionService.user.conBusId + '/documents',
            dbStructure: {},
            groupField: 'docLibName'
        };
        super(_urls, http, matC, userSessionService);
    }
}
