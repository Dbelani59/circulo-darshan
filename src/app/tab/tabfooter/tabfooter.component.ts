import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
    selector: 'app-tabfooter',
    templateUrl: './tabfooter.component.html',
    styleUrls: ['./tabfooter.component.scss']
})
export class TabfooterComponent implements OnInit {
    constructor(private _location: Location) {}

    ngOnInit() {}

    routeBack() {
        this._location.back();
    }
}
