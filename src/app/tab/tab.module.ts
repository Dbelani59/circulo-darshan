import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingComponent } from './landing/landing.component';

import { SharedModule } from '../shared';
import { ClockinComponent } from './clockin/clockin.component';

import { MatTabsModule } from '@angular/material';
import { MatIconModule } from '@angular/material/icon';
import { TasksComponent } from './tasks/tasks.component';
import { AppchoiceComponent } from './appchoice/appchoice.component';
import { DocumentsComponent } from './documents/documents/documents.component';
import { RouterModule } from '@angular/router';

import { WebcamModule } from 'ngx-webcam';
import { TabheaderComponent } from './tabheader/tabheader.component';
import { LoginMethodSelectComponent } from './login-method-select/login-method-select.component';
import { SleepComponent } from './sleep/sleep.component';
import { LoginFacialComponent } from './login-facial/login-facial.component';
import { LoginUsernameComponent } from './login-username/login-username.component';
import { RegisterComponent } from './register/register.component';
import { CoreModule } from '../core';
import { ClockinInOutComponent } from './clockin/clockin-in-out/clockin-in-out.component';
import { DocumentLibraryComponent } from './documents/document-library/document-library.component';
import { DocumentItemsComponent } from './documents/document-items/document-items.component';

import { DocViewComponent } from 'app/shared/doc-view/doc-view.component';
import { TabfooterComponent } from './tabfooter/tabfooter.component';
import { FaceCapturePinComponent } from './face-capture-pin/face-capture-pin.component';

export const accountingRoutes = [
    // {
    //   path: '',
    //   redirectTo: 'landing',
    //   pathMatch: 'full'

    // },

    {
        path: 'tab',
        component: SleepComponent
    },

    {
        path: '',
        component: SleepComponent
    },
    {
        path: 'login-method',
        component: LoginMethodSelectComponent
    },
    {
        path: 'login-username',
        component: LoginUsernameComponent
    },
    {
        path: 'face-capture-pin',
        component: FaceCapturePinComponent
    },
    {
        path: 'landing',
        component: SleepComponent
    },

    {
        path: 'library',
        component: DocumentLibraryComponent
    },
    {
        path: 'tab/library/:groupName/items/:docStoId',
        component: DocumentsComponent
    },

    {
        path: 'tab/library/:groupName/items',
        component: DocumentItemsComponent
    },

    {
        path: 'clockin-pin',
        component: ClockinComponent
    },
    {
        path: 'clockin-in-out',
        component: ClockinInOutComponent
    }
];

@NgModule({
    declarations: [
        LandingComponent,
        ClockinComponent,
        TasksComponent,
        AppchoiceComponent,
        DocumentsComponent,

        TabheaderComponent,
        LoginMethodSelectComponent,
        SleepComponent,
        LoginFacialComponent,
        LoginUsernameComponent,
        RegisterComponent,
        ClockinInOutComponent,
        DocumentLibraryComponent,
        DocumentItemsComponent,
        TabfooterComponent,
        FaceCapturePinComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        MatTabsModule,
        MatIconModule,
        WebcamModule,
        RouterModule.forChild(accountingRoutes),
        CoreModule
    ],
    exports: [
        LandingComponent,
        ClockinComponent,
        TasksComponent,
        AppchoiceComponent,
        TabheaderComponent,
        RegisterComponent,
        TabfooterComponent
    ],
    providers: [],
    entryComponents: [DocViewComponent] // Note re-matDialog https://github.com/angular/components/issues/1491
})
export class TabModule {}
