import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-appchoice',
  templateUrl: './appchoice.component.html',
  styleUrls: ['./appchoice.component.scss']
})
export class AppchoiceComponent implements OnInit {

  constructor() { }

  @Output() ItemClick = new EventEmitter<string>();


  onClick(itemClicked) {

   this.ItemClick.emit(itemClicked);
  }

  ngOnInit() {
  }

}
