import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeviceSessionService {




  public BaseAPIURLWithoutSysCompId;
  public BaseAPIURL;


  constructor(

    private http: HttpClient,
  ) {
    this.BaseAPIURLWithoutSysCompId = environment.API_URL;
  }
}
