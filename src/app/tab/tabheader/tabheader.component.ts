import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { Router } from '@angular/router';
import { UserSessionService } from 'app/core/login/services/user-session.service';
import { UserSessionModel } from 'app/core/login/services/user-session.model';
import { IfStmt } from '@angular/compiler';

@Component({
    selector: 'app-tabheader',
    templateUrl: './tabheader.component.html',
    styleUrls: ['./tabheader.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TabheaderComponent implements OnInit {
    companyName = 'CIRCULO';
    siteCodeName = 'Un-registered Device';
    baseUrl = '';
    constructor(@Inject(APP_BASE_HREF) private baseHref: string, private router: Router, public userSessionService: UserSessionService) {
        this.baseUrl = baseHref;
        let deviceInfo;
        deviceInfo = JSON.parse(localStorage.getItem('deviceInfo'));

        if (deviceInfo) {
            this.siteCodeName = deviceInfo.cardCode + ' - ' + deviceInfo.cardName;
        } else {
            this.siteCodeName = '';
        }

        userSessionService.userSessionEmit.subscribe((res: UserSessionModel) => {
            if (res.conBusName) {
                this.siteCodeName = res.conBusCardCode + ' - ' + res.conBusName;
            }

            if (res.conCompName) {
                this.companyName = res.conCompName;
            }
        });
    }

    ngOnInit() {
        if (localStorage.getItem('deviceInfo')) {
            const deviceInfo: any = JSON.parse(localStorage.getItem('deviceInfo'));
            //   console.log(deviceInfo);
            this.companyName = deviceInfo.companyName;
            this.siteCodeName = deviceInfo.cardCode + ' - ' + deviceInfo.cardName;
        } else {
            // this.companyName = 'CIRCULO';
        }
    }

    navigateAppSelect() {
        this.userSessionService.setApp('OFFICE');

        this.router.navigate(['/office']);
    }
}
